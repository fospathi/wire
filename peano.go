package wire

import (
	"gitlab.com/fospathi/wire/compgeom/peano"
)

// NewPeano constructs a new Peano curve which is initially oriented with a
// standard frame.
func NewPeano(spec peano.PlotSpec) Peano {
	return Peano{NewPolyLine(peano.Vertices(spec))}
}

// A Peano is a wireframe figure that depicts a Peano curve occupying a 2D
// square region.
//
// Relative to its orientation frame the curve is in a plane parallel to the
// XY-plane.
type Peano struct {
	*PolyLine
}
