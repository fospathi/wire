package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewPolyLine constructs a new polyline which is initially oriented with a
// standard frame.
func NewPolyLine(points []maf.Vec) *PolyLine {
	return &PolyLine{depictioner.Figure{Orientation: maf.StdFrame}, points}
}

// NewClosedPolyLine constructs a new closed polyline which is initially
// oriented with a standard frame.
func NewClosedPolyLine(points []maf.Vec) *PolyLine {
	if len(points) > 2 {
		points = append(points, points[0])
	}
	return &PolyLine{depictioner.Figure{Orientation: maf.StdFrame}, points}
}

// A PolyLine is a wireframe figure made from a connected sequence of line
// segments.
type PolyLine struct {
	depictioner.Figure

	// Points are the vertices of the polyline. Each vertex is connected by a
	// line segment to the next vertex in the slice.
	Points []maf.Vec
}

// Depict the given portions/(parameter intervals) on the polyline, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and they are
// unitless proportions of the polyline's total length in the range 0..1.
//
// The portions' quality is the literal number of quadratic Beziers to use for
// each line segment.
func (fig *PolyLine) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictPolyLine(fig.Orientation, fig.Points, vp)
}

// DepictPolyLine depicts the given portions/(parameter intervals) on a
// polyline.
func DepictPolyLine(
	fr maf.Frame,
	points []maf.Vec,
	vp []mech.Portion,
) mech.Depiction {
	l := len(points)
	if l == 0 {
		return mech.Depiction{}
	}

	type seg struct {
		arc float64 // Current arc length at the start of the segment.
		l   float64 // Length of the segment.
		seg maf.LineSeg
		t   interval.In
	}

	segs := l - 1
	vSeg := make([]seg, segs)

	polyL := 0.0
	for i := 0; i < segs; i++ {
		p1, p2 := points[i], points[i+1]
		segL := p1.DistTo(p2)
		vSeg[i] = seg{
			arc: polyL,
			l:   segL,
			seg: maf.LineSeg{P1: p1, P2: p2},
		}
		polyL += segL
	}

	for i, seg := range vSeg {
		seg.t = interval.In{
			seg.arc / polyL,
			math.Min(1.0, (seg.arc+seg.l)/polyL),
		}
		vSeg[i] = seg
	}

	pic := mech.Depiction{}
	for _, p := range vp {
		th := interval.In{p.Th[0], p.Th[1]}
		if p.Qual < 1 {
			p.Qual = LineQual
		}
		for _, seg := range vSeg {
			in, ok := seg.t.AsOpenIntersection(p.In)
			if !ok {
				continue
			}
			cap0, cap1 := mech.Circle, mech.Circle
			if in[0] == p.In[0] {
				cap0 = p.Caps[0]
			}
			if in[1] == p.In[1] {
				cap1 = p.Caps[1]
			}
			pic = pic.Append(depictioner.DepictLine(mech.Portion{
				Caps:     [2]mech.Linecap{cap0, cap1},
				Colourer: p.Colours(in[0], in[1]),
				In:       interval.In{0, 1},
				Qual:     p.Qual,
				Th:       [2]float64{th.T(in[0]), th.T(in[1])},
			}, fr, seg.seg))
		}
	}
	return pic
}

// NewMultiPartPolyLine constructs a new MultiPartPolyLine which is initially
// oriented with a standard frame.
func NewMultiPartPolyLine(parts [][]maf.Vec) *MultipartPolyLine {
	return &MultipartPolyLine{
		depictioner.Figure{Orientation: maf.StdFrame},
		parts,
	}
}

// A MultipartPolyLine is a wireframe figure made from distinct polyline parts
// such that there is a gap between the parts.
type MultipartPolyLine struct {
	depictioner.Figure

	// Parts are the polylines' vertices.
	Parts [][]maf.Vec
}

// Depict the given portions/(parameter intervals) on the multipart polyline,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and they are
// unitless proportions of the multipart polyline's total length in the range
// 0..1.
//
// The portions' quality is the literal number of quadratic Beziers to use for
// each line segment.
func (fig *MultipartPolyLine) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictMultiPartPolyLine(fig.Orientation, fig.Parts, vp)
}

// DepictMultiPartPolyLine depicts the given portions/(parameter intervals) on a
// multipart polyline such that there is a gap between the parts.
func DepictMultiPartPolyLine(
	fr maf.Frame,
	parts [][]maf.Vec,
	vp []mech.Portion,
) mech.Depiction {
	n := len(parts)
	if n == 0 {
		return mech.Depiction{}
	}

	type seg struct {
		arc float64 // Current arc length at the start of the segment.
		l   float64 // Length of the segment.
		seg maf.LineSeg
		t   interval.In
	}

	partSegs := make([][]seg, n)

	polyL := 0.0
	for i := 0; i < n; i++ {
		l := len(parts[i])
		if l == 0 {
			continue
		}

		segs := l - 1
		vSeg := make([]seg, segs)

		for j := 0; j < segs; j++ {
			p1, p2 := parts[i][j], parts[i][j+1]
			segL := p1.DistTo(p2)
			vSeg[j] = seg{
				arc: polyL,
				l:   segL,
				seg: maf.LineSeg{P1: p1, P2: p2},
			}
			polyL += segL
		}

		partSegs[i] = vSeg
	}

	for i := 0; i < n; i++ {
		for j, seg := range partSegs[i] {
			seg.t = interval.In{
				seg.arc / polyL,
				math.Min(1.0, (seg.arc+seg.l)/polyL),
			}
			partSegs[i][j] = seg
		}
	}

	pic := mech.Depiction{}
	for _, p := range vp {
		th := interval.In{p.Th[0], p.Th[1]}
		if p.Qual < 1 {
			p.Qual = LineQual
		}
		for _, vSeg := range partSegs {
			for _, seg := range vSeg {
				in, ok := seg.t.AsOpenIntersection(p.In)
				if !ok {
					continue
				}
				cap0, cap1 := mech.Circle, mech.Circle
				if in[0] == p.In[0] {
					cap0 = p.Caps[0]
				}
				if in[1] == p.In[1] {
					cap1 = p.Caps[1]
				}
				pic = pic.Append(depictioner.DepictLine(mech.Portion{
					Caps:     [2]mech.Linecap{cap0, cap1},
					Colourer: p.Colours(in[0], in[1]),
					In:       interval.In{0, 1},
					Qual:     p.Qual,
					Th:       [2]float64{th.T(in[0]), th.T(in[1])},
				}, fr, seg.seg))
			}
		}
	}
	return pic
}
