package colour

import (
	"fmt"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// An OkLCh colour.
//
// The illuminant is D65 white.
//
// https://www.w3.org/TR/css-color-4/#ok-lab.
type OkLCh struct {
	// The lightness L is a percentage between 0 and 100. The chroma C is a
	// positive number that does not exceed 0.5 in practice. The hue H is an
	// angle in degrees from 0 to 360.
	//
	//                                  | (b) / 90 deg / mustard yellow
	//                                  |
	//                                  |
	//  180 deg / greenish cyan - - - - | - - - - (a) / 0 deg / purplish red
	//                                  |
	//                                  |
	//                                  | 270 deg / sky blue
	//
	// https://www.w3.org/TR/css-color-4/#funcdef-oklch
	L, C, H float64
}

func (lch OkLCh) rgb() maf.Vec {
	lab := okLChToOkLab(maf.Vec{X: lch.L / 100, Y: lch.C, Z: lch.H})
	return xyzToLinear.Transform(okLabToXYZ(lab)).Map(LinToGamma)
}

// Colour is an sRGB equivalent of the receiver colour.
func (lch OkLCh) Colour() mech.Colour {
	v := lch.rgb()
	return [3]float32{float32(v.X), float32(v.Y), float32(v.Z)}
}

// GoString returns a Go parsable string representing the receiver colour. Its
// components are rounded to two decimal places.
func (lch OkLCh) GoString() string {
	return fmt.Sprintf("OkLCh{L: %.2f, C: %.2f, H: %.2f}", lch.L, lch.C, lch.H)
}

// RGB returns the sRGB equivalent of the receiver colour.
func (lch OkLCh) RGB() RGB {
	return NewRGB(lch.rgb())
}
