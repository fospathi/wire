package colour

import "gitlab.com/fospathi/universal/mech"

// OkLChGrad is a colour gradient that linearly interpolates between two OkLCh
// colours.
type OkLChGrad struct {
	from OkLCh
	Δh   float64
	toC  float64
	toL  float64
}

// Colour takes a parameter that varies in the range 0..1 and returns a linearly
// interpolated RGB colour with components clamped to the range 0..1.
func (g OkLChGrad) Colour(t float64) mech.Colour {
	return g.OkLCh(t).Colour().Clamped()
}

// Colours is a slice of the receiver colour gradient between the given
// proportions of the interval in the range 0..1.
func (g OkLChGrad) Colours(t1, t2 float64) mech.Colourer {
	from := LCh{
		L: g.from.L + t1*(g.toL-g.from.L),
		C: g.from.C + t1*(g.toC-g.from.C),
		H: g.from.H + t1*g.Δh,
	}
	to := LCh{
		L: g.from.L + t2*(g.toL-g.from.L),
		C: g.from.C + t2*(g.toC-g.from.C),
		H: g.from.H + t2*g.Δh,
	}
	return NewLChGrad(from, to.L, to.C, to.H-from.H)
}

// OkLCh takes a parameter that varies in the range 0..1 and returns a linearly
// interpolated colour.
func (g OkLChGrad) OkLCh(t float64) OkLCh {
	var h float64
	if g.Δh == 0 {
		h = g.from.H
	} else {
		h = mod360(g.from.H + g.Δh*t)
	}
	return OkLCh{
		L: g.from.L + t*(g.toL-g.from.L),
		C: g.from.C + t*(g.toC-g.from.C),
		H: h,
	}
}

// Uniform reports whether the gradient is a single colour.
func (g OkLChGrad) Uniform() bool {
	return g.Δh == 0 && g.from.C == g.toC && g.from.L == g.toL
}

// NewOkLChGrad constructs a new OkLChGrad which covers the range from the given
// colour to the absolute L and C values and the relative hue value.
//
// The delta hue is measured in degrees and is clamped to the range -360..360.
func NewOkLChGrad(from OkLCh, toL float64, toC float64, Δh float64) OkLChGrad {
	if Δh < -360 {
		Δh = -360
	} else if Δh > 360 {
		Δh = 360
	}
	return OkLChGrad{from: from, Δh: Δh, toL: toL, toC: toC}
}
