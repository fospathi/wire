package colour

import "fmt"

// An RGBA colour is an RGB colour with variable opacity.
type RGBA struct {
	// The amount of the primary additive red colour in the range 0..255.
	R uint8 `json:"r"`
	// The amount of the primary additive green colour in the range 0..255.
	G uint8 `json:"g"`
	// The amount of the primary additive blue colour in the range 0..255.
	B uint8 `json:"b"`
	// Alpha in the range 0..1.
	A float64 `json:"a"`
}

// String returns a CSS compatible string version of the receiver colour.
func (rgba RGBA) String() string {
	return fmt.Sprintf("rgba(%v, %v, %v, %v)", rgba.R, rgba.G, rgba.B, rgba.A)
}

// ToRGB colour.
//
// The alpha channel is dropped.
func (rgba RGBA) ToRGB() RGB {
	return RGB{R: rgba.R, G: rgba.G, B: rgba.B}
}
