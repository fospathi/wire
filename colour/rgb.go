package colour

import (
	"fmt"
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// An RGB (sRGB colour space) colour.
//
// The illuminant is D65 white.
type RGB struct {
	// The amount of the primary additive colours in the range 0..255.
	R, G, B uint8
}

// Colour is the standard Mechane colour format.
func (rgb RGB) Colour() mech.Colour {
	return mech.Colour{
		float32(rgb.R) / 255.0,
		float32(rgb.G) / 255.0,
		float32(rgb.B) / 255.0,
	}
}

// Colour64 returns the receiver colour in a vector format with components in
// the range 0..1.
func (rgb RGB) Colour64() maf.Vec {
	return maf.Vec{
		X: float64(rgb.R) / 255,
		Y: float64(rgb.G) / 255,
		Z: float64(rgb.B) / 255,
	}
}

// Lab returns the Lab equivalent of the receiver colour.
//
// Note that the returned colour uses the D50 reference white whereas the
// receiver colour uses the D65 reference white.
func (rgb RGB) Lab() Lab {
	return rgbToLab(rgb)
}

// LCh returns the LCh equivalent of the receiver colour.
//
// Note that the returned colour uses the D50 reference white whereas the
// receiver colour uses the D65 reference white.
func (rgb RGB) LCh() LCh {
	return rgb.Lab().LCh()
}

// OkLCh returns the OkLCh equivalent of the receiver colour.
func (rgb RGB) OkLCh() OkLCh {
	v := okLabToOkLCh(xyzToOkLab(
		linearToXYZ.Transform(rgb.Colour64().Map(GammaToLin)),
	))
	return OkLCh{L: v.X * 100, C: v.Y, H: v.Z}
}

// Opaque RGBA version of the receiver colour.
func (rgb RGB) Opaque() RGBA {
	return RGBA{R: rgb.R, G: rgb.G, B: rgb.B, A: 1}
}

// String returns a CSS compatible string version of the receiver colour.
func (rgb RGB) String() string {
	return fmt.Sprintf("rgb(%v %v %v)", rgb.R, rgb.G, rgb.B)
}

// ToRGBA with the given alpha value and the receiver colour's RGB channels.
func (rgb RGB) ToRGBA(a float64) RGBA {
	return RGBA{R: rgb.R, G: rgb.G, B: rgb.B, A: a}
}

// NewRGB creates and initialises a new RGB using the argument RGB value which
// shall be given in ratios of 1 format, i.e each value is in the range 0..1.
func NewRGB(rgb maf.Vec) RGB {
	return RGB{
		R: uint8(math.Round(rgb.X * 255)),
		G: uint8(math.Round(rgb.Y * 255)),
		B: uint8(math.Round(rgb.Z * 255)),
	}
}
