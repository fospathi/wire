package colour

import (
	"fmt"
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// An LCh (CIELCh colour space) colour.
//
// An LCh colour is a direct cylindrical coordinates equivalent of a Cartesian
// coordinates Lab colour.
//
// The illuminant is D50 white.
//
// In CIELCh the CIE acronym is a French title which translates as International
// Commission on Illumination.
type LCh struct {
	// L and C represent the colour's lightness (L*) and chroma (C*) values.
	//
	// L* = 0 gives black and L* = 100 gives diffuse white; specular white may
	// be higher.
	//
	// C* = 0 gives one of the true neutral grays. In practice C* does not
	// exceed 230 (since the corresponding a* and b* values in Cartesian
	// coordinates do not in practice exceed ±160).
	L, C float64

	// H represents the colour's hue (h*) value, and is the polar coordinates
	// angle in the range 0..360:
	//
	// * yellow = 90°
	//
	// * green = 180°
	//
	// * blue = 270°
	//
	// * red = 360°
	H float64
}

// Colour is an sRGB equivalent of the receiver colour.
//
// Note that the returned colour uses the D65 reference white whereas the
// receiver colour uses the D50 reference white.
func (lch LCh) Colour() mech.Colour {
	v := labToRGB(lch.Lab())
	return [3]float32{float32(v.X), float32(v.Y), float32(v.Z)}
}

// Lab returns the Lab equivalent of the receiver colour.
func (lch LCh) Lab() Lab {
	s, c := math.Sincos(maf.DegToRad(lch.H))
	return Lab{
		L: lch.L,
		A: lch.C * c,
		B: lch.C * s,
	}
}

// GoString returns a Go parsable string representing the receiver colour. Its
// components are rounded to two decimal places.
func (lch LCh) GoString() string {
	return fmt.Sprintf("LCh{L: %.2f, C: %.2f, H: %.2f}", lch.L, lch.C, lch.H)
}

// RGB returns the sRGB equivalent of the receiver colour.
//
// Note that the returned colour uses the D65 reference white whereas the
// receiver colour uses the D50 reference white.
func (lch LCh) RGB() RGB {
	return lch.Lab().RGB()
}
