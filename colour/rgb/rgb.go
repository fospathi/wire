/*
Package rgb contains the 148 CSS4 colour names as RGB type colours as well as
some other colours commonly used by Mechane.
*/
package rgb

import "gitlab.com/fospathi/wire/colour"

// New constructs a new RGB colour using the argument primary colour components.
func New(r, g, b uint8) colour.RGB {
	return colour.RGB{R: r, G: g, B: b}
}

// NewGray constructs a new gray RGB colour using the argument primary colour
// component.
func NewGray(rgb uint8) colour.RGB {
	return colour.RGB{R: rgb, G: rgb, B: rgb}
}

// NewGrey constructs a new grey RGB colour using the argument primary colour
// component.
func NewGrey(rgb uint8) colour.RGB {
	return colour.RGB{R: rgb, G: rgb, B: rgb}
}

// NamedColours has lowercase colour names as keys and sRGB colour space colours
// as values.
var NamedColours = map[string]colour.RGB{
	"aliceblue":            Aliceblue,
	"antiquewhite":         Antiquewhite,
	"aqua":                 Aqua,
	"aquamarine":           Aquamarine,
	"azure":                Azure,
	"beige":                Beige,
	"bisque":               Bisque,
	"black":                Black,
	"blanchedalmond":       Blanchedalmond,
	"blue":                 Blue,
	"blueviolet":           Blueviolet,
	"brown":                Brown,
	"burlywood":            Burlywood,
	"cadetblue":            Cadetblue,
	"chartreuse":           Chartreuse,
	"chocolate":            Chocolate,
	"coral":                Coral,
	"cornflowerblue":       Cornflowerblue,
	"cornsilk":             Cornsilk,
	"crimson":              Crimson,
	"cyan":                 Cyan,
	"darkblue":             Darkblue,
	"darkcyan":             Darkcyan,
	"darkgoldenrod":        Darkgoldenrod,
	"darkgray":             Darkgray,
	"darkgreen":            Darkgreen,
	"darkgrey":             Darkgrey,
	"darkkhaki":            Darkkhaki,
	"darkmagenta":          Darkmagenta,
	"darkolivegreen":       Darkolivegreen,
	"darkorange":           Darkorange,
	"darkorchid":           Darkorchid,
	"darkred":              Darkred,
	"darksalmon":           Darksalmon,
	"darkseagreen":         Darkseagreen,
	"darkslateblue":        Darkslateblue,
	"darkslategray":        Darkslategray,
	"darkslategrey":        Darkslategrey,
	"darkturquoise":        Darkturquoise,
	"darkviolet":           Darkviolet,
	"deeppink":             Deeppink,
	"deepskyblue":          Deepskyblue,
	"dimgray":              Dimgray,
	"dimgrey":              Dimgrey,
	"dodgerblue":           Dodgerblue,
	"firebrick":            Firebrick,
	"floralwhite":          Floralwhite,
	"forestgreen":          Forestgreen,
	"fuchsia":              Fuchsia,
	"gainsboro":            Gainsboro,
	"ghostwhite":           Ghostwhite,
	"gold":                 Gold,
	"goldenrod":            Goldenrod,
	"gray":                 Gray,
	"grey":                 Grey,
	"green":                Green,
	"greenyellow":          Greenyellow,
	"honeydew":             Honeydew,
	"hotpink":              Hotpink,
	"indianred":            Indianred,
	"indigo":               Indigo,
	"ivory":                Ivory,
	"khaki":                Khaki,
	"lavender":             Lavender,
	"lavenderblush":        Lavenderblush,
	"lawngreen":            Lawngreen,
	"lemonchiffon":         Lemonchiffon,
	"lightblue":            Lightblue,
	"lightcoral":           Lightcoral,
	"lightcyan":            Lightcyan,
	"lightgoldenrodyellow": Lightgoldenrodyellow,
	"lightgray":            Lightgray,
	"lightgreen":           Lightgreen,
	"lightgrey":            Lightgrey,
	"lightpink":            Lightpink,
	"lightsalmon":          Lightsalmon,
	"lightseagreen":        Lightseagreen,
	"lightskyblue":         Lightskyblue,
	"lightslategray":       Lightslategray,
	"lightslategrey":       Lightslategrey,
	"lightsteelblue":       Lightsteelblue,
	"lightyellow":          Lightyellow,
	"lime":                 Lime,
	"limegreen":            Limegreen,
	"linen":                Linen,
	"magenta":              Magenta,
	"maroon":               Maroon,
	"mediumaquamarine":     Mediumaquamarine,
	"mediumblue":           Mediumblue,
	"mediumorchid":         Mediumorchid,
	"mediumpurple":         Mediumpurple,
	"mediumseagreen":       Mediumseagreen,
	"mediumslateblue":      Mediumslateblue,
	"mediumspringgreen":    Mediumspringgreen,
	"mediumturquoise":      Mediumturquoise,
	"mediumvioletred":      Mediumvioletred,
	"midnightblue":         Midnightblue,
	"mintcream":            Mintcream,
	"mistyrose":            Mistyrose,
	"moccasin":             Moccasin,
	"navajowhite":          Navajowhite,
	"navy":                 Navy,
	"oldlace":              Oldlace,
	"olive":                Olive,
	"olivedrab":            Olivedrab,
	"orange":               Orange,
	"orangered":            Orangered,
	"orchid":               Orchid,
	"palegoldenrod":        Palegoldenrod,
	"palegreen":            Palegreen,
	"paleturquoise":        Paleturquoise,
	"palevioletred":        Palevioletred,
	"papayawhip":           Papayawhip,
	"peachpuff":            Peachpuff,
	"peru":                 Peru,
	"pink":                 Pink,
	"plum":                 Plum,
	"powderblue":           Powderblue,
	"purple":               Purple,
	"rebeccapurple":        Rebeccapurple,
	"red":                  Red,
	"rosybrown":            Rosybrown,
	"royalblue":            Royalblue,
	"saddlebrown":          Saddlebrown,
	"salmon":               Salmon,
	"sandybrown":           Sandybrown,
	"seagreen":             Seagreen,
	"seashell":             Seashell,
	"sienna":               Sienna,
	"silver":               Silver,
	"skyblue":              Skyblue,
	"slateblue":            Slateblue,
	"slategray":            Slategray,
	"slategrey":            Slategrey,
	"snow":                 Snow,
	"springgreen":          Springgreen,
	"steelblue":            Steelblue,
	"tan":                  Tan,
	"teal":                 Teal,
	"thistle":              Thistle,
	"tomato":               Tomato,
	"turquoise":            Turquoise,
	"violet":               Violet,
	"wheat":                Wheat,
	"white":                White,
	"whitesmoke":           Whitesmoke,
	"yellow":               Yellow,
	"yellowgreen":          Yellowgreen,
}

// An RGB format named colour, there is one for each of the 148 possible CSS4
// [named colours].
//
// [named colours]: https://www.w3.org/TR/css-color-4/#named-colors
var (
	Aliceblue            = colour.RGB{R: 240, G: 248, B: 255}
	Antiquewhite         = colour.RGB{R: 250, G: 235, B: 215}
	Aqua                 = colour.RGB{R: 0, G: 255, B: 255}
	Aquamarine           = colour.RGB{R: 127, G: 255, B: 212}
	Azure                = colour.RGB{R: 240, G: 255, B: 255}
	Beige                = colour.RGB{R: 245, G: 245, B: 220}
	Bisque               = colour.RGB{R: 255, G: 228, B: 196}
	Black                = colour.RGB{R: 0, G: 0, B: 0}
	Blanchedalmond       = colour.RGB{R: 255, G: 235, B: 205}
	Blue                 = colour.RGB{R: 0, G: 0, B: 255}
	Blueviolet           = colour.RGB{R: 138, G: 43, B: 226}
	Brown                = colour.RGB{R: 165, G: 42, B: 42}
	Burlywood            = colour.RGB{R: 222, G: 184, B: 135}
	Cadetblue            = colour.RGB{R: 95, G: 158, B: 160}
	Chartreuse           = colour.RGB{R: 127, G: 255, B: 0}
	Chocolate            = colour.RGB{R: 210, G: 105, B: 30}
	Coral                = colour.RGB{R: 255, G: 127, B: 80}
	Cornflowerblue       = colour.RGB{R: 100, G: 149, B: 237}
	Cornsilk             = colour.RGB{R: 255, G: 248, B: 220}
	Crimson              = colour.RGB{R: 220, G: 20, B: 60}
	Cyan                 = colour.RGB{R: 0, G: 255, B: 255}
	Darkblue             = colour.RGB{R: 0, G: 0, B: 139}
	Darkcyan             = colour.RGB{R: 0, G: 139, B: 139}
	Darkgoldenrod        = colour.RGB{R: 184, G: 134, B: 11}
	Darkgray             = colour.RGB{R: 169, G: 169, B: 169}
	Darkgreen            = colour.RGB{R: 0, G: 100, B: 0}
	Darkgrey             = colour.RGB{R: 169, G: 169, B: 169}
	Darkkhaki            = colour.RGB{R: 189, G: 183, B: 107}
	Darkmagenta          = colour.RGB{R: 139, G: 0, B: 139}
	Darkolivegreen       = colour.RGB{R: 85, G: 107, B: 47}
	Darkorange           = colour.RGB{R: 255, G: 140, B: 0}
	Darkorchid           = colour.RGB{R: 153, G: 50, B: 204}
	Darkred              = colour.RGB{R: 139, G: 0, B: 0}
	Darksalmon           = colour.RGB{R: 233, G: 150, B: 122}
	Darkseagreen         = colour.RGB{R: 143, G: 188, B: 143}
	Darkslateblue        = colour.RGB{R: 72, G: 61, B: 139}
	Darkslategray        = colour.RGB{R: 47, G: 79, B: 79}
	Darkslategrey        = colour.RGB{R: 47, G: 79, B: 79}
	Darkturquoise        = colour.RGB{R: 0, G: 206, B: 209}
	Darkviolet           = colour.RGB{R: 148, G: 0, B: 211}
	Deeppink             = colour.RGB{R: 255, G: 20, B: 147}
	Deepskyblue          = colour.RGB{R: 0, G: 191, B: 255}
	Dimgray              = colour.RGB{R: 105, G: 105, B: 105}
	Dimgrey              = colour.RGB{R: 105, G: 105, B: 105}
	Dodgerblue           = colour.RGB{R: 30, G: 144, B: 255}
	Firebrick            = colour.RGB{R: 178, G: 34, B: 34}
	Floralwhite          = colour.RGB{R: 255, G: 250, B: 240}
	Forestgreen          = colour.RGB{R: 34, G: 139, B: 34}
	Fuchsia              = colour.RGB{R: 255, G: 0, B: 255}
	Gainsboro            = colour.RGB{R: 220, G: 220, B: 220}
	Ghostwhite           = colour.RGB{R: 248, G: 248, B: 255}
	Gold                 = colour.RGB{R: 255, G: 215, B: 0}
	Goldenrod            = colour.RGB{R: 218, G: 165, B: 32}
	Gray                 = colour.RGB{R: 128, G: 128, B: 128}
	Grey                 = colour.RGB{R: 128, G: 128, B: 128}
	Green                = colour.RGB{R: 0, G: 128, B: 0}
	Greenyellow          = colour.RGB{R: 173, G: 255, B: 47}
	Honeydew             = colour.RGB{R: 240, G: 255, B: 240}
	Hotpink              = colour.RGB{R: 255, G: 105, B: 180}
	Indianred            = colour.RGB{R: 205, G: 92, B: 92}
	Indigo               = colour.RGB{R: 75, G: 0, B: 130}
	Ivory                = colour.RGB{R: 255, G: 255, B: 240}
	Khaki                = colour.RGB{R: 240, G: 230, B: 140}
	Lavender             = colour.RGB{R: 230, G: 230, B: 250}
	Lavenderblush        = colour.RGB{R: 255, G: 240, B: 245}
	Lawngreen            = colour.RGB{R: 124, G: 252, B: 0}
	Lemonchiffon         = colour.RGB{R: 255, G: 250, B: 205}
	Lightblue            = colour.RGB{R: 173, G: 216, B: 230}
	Lightcoral           = colour.RGB{R: 240, G: 128, B: 128}
	Lightcyan            = colour.RGB{R: 224, G: 255, B: 255}
	Lightgoldenrodyellow = colour.RGB{R: 250, G: 250, B: 210}
	Lightgray            = colour.RGB{R: 211, G: 211, B: 211}
	Lightgreen           = colour.RGB{R: 144, G: 238, B: 144}
	Lightgrey            = colour.RGB{R: 211, G: 211, B: 211}
	Lightpink            = colour.RGB{R: 255, G: 182, B: 193}
	Lightsalmon          = colour.RGB{R: 255, G: 160, B: 122}
	Lightseagreen        = colour.RGB{R: 32, G: 178, B: 170}
	Lightskyblue         = colour.RGB{R: 135, G: 206, B: 250}
	Lightslategray       = colour.RGB{R: 119, G: 136, B: 153}
	Lightslategrey       = colour.RGB{R: 119, G: 136, B: 153}
	Lightsteelblue       = colour.RGB{R: 176, G: 196, B: 222}
	Lightyellow          = colour.RGB{R: 255, G: 255, B: 224}
	Lime                 = colour.RGB{R: 0, G: 255, B: 0}
	Limegreen            = colour.RGB{R: 50, G: 205, B: 50}
	Linen                = colour.RGB{R: 250, G: 240, B: 230}
	Magenta              = colour.RGB{R: 255, G: 0, B: 255}
	Maroon               = colour.RGB{R: 128, G: 0, B: 0}
	Mediumaquamarine     = colour.RGB{R: 102, G: 205, B: 170}
	Mediumblue           = colour.RGB{R: 0, G: 0, B: 205}
	Mediumorchid         = colour.RGB{R: 186, G: 85, B: 211}
	Mediumpurple         = colour.RGB{R: 147, G: 112, B: 219}
	Mediumseagreen       = colour.RGB{R: 60, G: 179, B: 113}
	Mediumslateblue      = colour.RGB{R: 123, G: 104, B: 238}
	Mediumspringgreen    = colour.RGB{R: 0, G: 250, B: 154}
	Mediumturquoise      = colour.RGB{R: 72, G: 209, B: 204}
	Mediumvioletred      = colour.RGB{R: 199, G: 21, B: 133}
	Midnightblue         = colour.RGB{R: 25, G: 25, B: 112}
	Mintcream            = colour.RGB{R: 245, G: 255, B: 250}
	Mistyrose            = colour.RGB{R: 255, G: 228, B: 225}
	Moccasin             = colour.RGB{R: 255, G: 228, B: 181}
	Navajowhite          = colour.RGB{R: 255, G: 222, B: 173}
	Navy                 = colour.RGB{R: 0, G: 0, B: 128}
	Oldlace              = colour.RGB{R: 253, G: 245, B: 230}
	Olive                = colour.RGB{R: 128, G: 128, B: 0}
	Olivedrab            = colour.RGB{R: 107, G: 142, B: 35}
	Orange               = colour.RGB{R: 255, G: 165, B: 0}
	Orangered            = colour.RGB{R: 255, G: 69, B: 0}
	Orchid               = colour.RGB{R: 218, G: 112, B: 214}
	Palegoldenrod        = colour.RGB{R: 238, G: 232, B: 170}
	Palegreen            = colour.RGB{R: 152, G: 251, B: 152}
	Paleturquoise        = colour.RGB{R: 175, G: 238, B: 238}
	Palevioletred        = colour.RGB{R: 219, G: 112, B: 147}
	Papayawhip           = colour.RGB{R: 255, G: 239, B: 213}
	Peachpuff            = colour.RGB{R: 255, G: 218, B: 185}
	Peru                 = colour.RGB{R: 205, G: 133, B: 63}
	Pink                 = colour.RGB{R: 255, G: 192, B: 203}
	Plum                 = colour.RGB{R: 221, G: 160, B: 221}
	Powderblue           = colour.RGB{R: 176, G: 224, B: 230}
	Purple               = colour.RGB{R: 128, G: 0, B: 128}
	Rebeccapurple        = colour.RGB{R: 102, G: 51, B: 153}
	Red                  = colour.RGB{R: 255, G: 0, B: 0}
	Rosybrown            = colour.RGB{R: 188, G: 143, B: 143}
	Royalblue            = colour.RGB{R: 65, G: 105, B: 225}
	Saddlebrown          = colour.RGB{R: 139, G: 69, B: 19}
	Salmon               = colour.RGB{R: 250, G: 128, B: 114}
	Sandybrown           = colour.RGB{R: 244, G: 164, B: 96}
	Seagreen             = colour.RGB{R: 46, G: 139, B: 87}
	Seashell             = colour.RGB{R: 255, G: 245, B: 238}
	Sienna               = colour.RGB{R: 160, G: 82, B: 45}
	Silver               = colour.RGB{R: 192, G: 192, B: 192}
	Skyblue              = colour.RGB{R: 135, G: 206, B: 235}
	Slateblue            = colour.RGB{R: 106, G: 90, B: 205}
	Slategray            = colour.RGB{R: 112, G: 128, B: 144}
	Slategrey            = colour.RGB{R: 112, G: 128, B: 144}
	Snow                 = colour.RGB{R: 255, G: 250, B: 250}
	Springgreen          = colour.RGB{R: 0, G: 255, B: 127}
	Steelblue            = colour.RGB{R: 70, G: 130, B: 180}
	Tan                  = colour.RGB{R: 210, G: 180, B: 140}
	Teal                 = colour.RGB{R: 0, G: 128, B: 128}
	Thistle              = colour.RGB{R: 216, G: 191, B: 216}
	Tomato               = colour.RGB{R: 255, G: 99, B: 71}
	Turquoise            = colour.RGB{R: 64, G: 224, B: 208}
	Violet               = colour.RGB{R: 238, G: 130, B: 238}
	Wheat                = colour.RGB{R: 245, G: 222, B: 179}
	White                = colour.RGB{R: 255, G: 255, B: 255}
	Whitesmoke           = colour.RGB{R: 245, G: 245, B: 245}
	Yellow               = colour.RGB{R: 255, G: 255, B: 0}
	Yellowgreen          = colour.RGB{R: 154, G: 205, B: 50}
)
