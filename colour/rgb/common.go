package rgb

import "gitlab.com/fospathi/wire/colour"

var (
	CosmicLatte        = colour.RGB{R: 255, G: 248, B: 231}
	MechaneOrange      = Darkorange
	MechaneOrangeGlow  = colour.RGB{R: 255, G: 210, B: 0}
	MechaneOrangeLight = Orange
	PastelRed          = colour.RGB{R: 255, G: 105, B: 97}
	PastelYellow       = colour.RGB{R: 255, G: 250, B: 160}
	PrussianBlue       = colour.RGB{R: 0, G: 49, B: 83}
	PrussianBlueLight  = colour.RGB{R: 0, G: 100, B: 134}
	SchoolBusYellow    = colour.RGB{R: 255, G: 216, B: 0}
)
