package lch

import (
	"testing"

	"gitlab.com/fospathi/wire/colour"
)

func TestAliceblue_RGB(t *testing.T) {
	want := colour.RGB{R: 240, G: 248, B: 255}
	got := Aliceblue.RGB()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
