package colour

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// An OkLab colour.
//
// The illuminant is D65 white.
//
// https://www.w3.org/TR/css-color-4/#ok-lab.
type OkLab struct {
	// L is a percentage between 0 and 100. A and B are coordinates that do not
	// exceed ±0.5 in practice.
	//
	//                                  | (b) / 90 deg / mustard yellow
	//                                  |
	//                                  |
	//  180 deg / greenish cyan - - - - | - - - - (a) / 0 deg / purplish red
	//                                  |
	//                                  |
	//                                  | 270 deg / sky blue
	//
	// https://www.w3.org/TR/css-color-4/#funcdef-oklab
	L, A, B float64
}

// https://www.w3.org/TR/css-color-4/#color-conversion-code
var (
	// Given XYZ relative to D65, convert to OKLab. L in the range 0 to 1.
	xyzToLMS = maf.Mat3{
		0.8190224432164319, 0.0329836671980271, 0.048177199566046255,
		0.3619062562801221, 0.9292868468965546, 0.26423952494422764,
		-0.12887378261216414, 0.03614466816999844, 0.6335478258136937,
	}
	lmsToOKLab = maf.Mat3{
		0.2104542553, 1.9779984951, 0.0259040371,
		0.7936177850, -2.4285922050, 0.7827717662,
		-0.0040720468, 0.4505937099, -0.8086757660,
	}

	// Given OKLab, convert to XYZ relative to D65.
	okLabToLMS = maf.Mat3{
		0.99999999845051981432, 1.0000000088817607767, 1.0000000546724109177,
		0.39633779217376785678, -0.1055613423236563494, -0.089484182094965759684,
		0.21580375806075880339, -0.063854174771705903402, -1.2914855378640917399,
	}
	lmsToXYZ = maf.Mat3{
		1.2268798733741557, -0.04057576262431372, -0.07637294974672142,
		-0.5578149965554813, 1.1122868293970594, -0.4214933239627914,
		0.28139105017721583, -0.07171106666151701, 1.5869240244272418,
	}
)

// https://www.w3.org/TR/css-color-4/#color-conversion-code
//
// L in the range 0 to 1.
func xyzToOkLab(xyz maf.Vec) maf.Vec {
	lms := xyzToLMS.Transform(xyz)
	// L in range {0,1}. For use in CSS, multiply by 100 to get a percentage.
	return lmsToOKLab.Transform(lms.Map(math.Cbrt))
}

// https://www.w3.org/TR/css-color-4/#color-conversion-code
//
// L in the range 0 to 1.
func okLabToXYZ(okLab maf.Vec) maf.Vec {
	nlLMS := okLabToLMS.Transform(okLab)
	return lmsToXYZ.Transform(nlLMS.Map(func(c float64) float64 {
		return c * c * c
	}))
}

// https://www.w3.org/TR/css-color-4/#color-conversion-code
func okLabToOkLCh(okLab maf.Vec) maf.Vec {
	h := maf.RadToDeg(math.Atan2(okLab.Z, okLab.Y))
	if h < 0 {
		h += 360 // Hue, in degrees [0 to 360).
	}
	return maf.Vec{
		X: okLab.X,                                      // L is still L.
		Y: math.Sqrt(okLab.Y*okLab.Y + okLab.Z*okLab.Z), // Chroma.
		Z: h,
	}
}

// https://www.w3.org/TR/css-color-4/#color-conversion-code
func okLChToOkLab(OkLCh maf.Vec) maf.Vec {
	sin, cos := math.Sincos(maf.DegToRad(OkLCh.Z))
	return maf.Vec{
		X: OkLCh.X,       // L is still L.
		Y: OkLCh.Y * cos, // a.
		Z: OkLCh.Y * sin, // b.
	}
}
