package colour

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// A Lab (CIELAB colour space) colour.
//
// The illuminant is D50 white.
//
// In CIELAB the CIE acronym is a French title which translates as International
// Commission on Illumination.
type Lab struct {
	// L, A, and B represent the colour's L*, a*, and b* values.
	//
	// L* = 0 gives black and L* = 100 gives diffuse white; specular white may
	// be higher.
	//
	// a* = positive gives a red; a* = negative gives a green.
	//
	// b* = positive gives a yellow; b* = negative gives a blue.
	//
	// a* = b* = 0 gives one of the true neutral grays. In practice the a* and
	// b* values do not exceed ±160.
	L, A, B float64
}

// LCh returns the LCh equivalent of the receiver colour.
func (lab Lab) LCh() LCh {
	h := maf.RadToDeg(math.Atan2(lab.B, lab.A))
	if h < 0 {
		h += 360
	}
	return LCh{
		L: lab.L,
		C: math.Sqrt(lab.A*lab.A + lab.B*lab.B),
		H: h,
	}
}

// RGB returns the RGB equivalent of the receiver colour.
//
// Note that the returned colour uses the D65 reference white whereas the
// receiver colour uses the D50 reference white.
func (lab Lab) RGB() RGB {
	return NewRGB(labToRGB(lab))
}

// https://www.w3.org/TR/css-color-4/#color-conversion-code
var (
	// GammaToLin converts a gamma corrected sRGB component to linear-light
	// form.
	//
	// The component shall be in the range 0 to 1.
	GammaToLin = func(val float64) float64 {
		if val < 0.04045 {
			return val / 12.92
		}
		return math.Pow((val+0.055)/1.055, 2.4)
	}

	// Convert linear-light sRGB to XYZ.
	linearToXYZ = maf.Mat3{
		0.4124564, 0.2126729, 0.0193339,
		0.3575761, 0.7151522, 0.1191920,
		0.1804375, 0.0721750, 0.9503041,
	}

	// Convert XYZ to linear-light sRGB.
	xyzToLinear = maf.Mat3{
		3.2404542, -0.9692660, 0.0556434,
		-1.5371385, 1.8760108, -0.2040259,
		-0.4985314, 0.0415560, 1.0572252,
	}

	// LinToGamma converts a linear-light sRGB component to gamma corrected
	// form.
	//
	// The component shall be in the range 0 to 1.
	LinToGamma = func(val float64) float64 {
		if val > 0.0031308 {
			return 1.055*math.Pow(val, 1/2.4) - 0.055
		}
		return 12.92 * val
	}
)

func labToRGB(lab Lab) maf.Vec {
	// https://www.w3.org/TR/css-color-4/#color-conversion-code
	labToXYZ := func(lab Lab) maf.Vec {
		κ := 24389.0 / 27
		ε := 216.0 / 24389
		var f [3]float64
		f[1] = (lab.L + 16) / 116
		f[0] = lab.A/500 + f[1]
		f[2] = f[1] - lab.B/200
		var xyz maf.Vec
		if x := f[0] * f[0] * f[0]; x > ε {
			xyz.X = x
		} else {
			xyz.X = (116*f[0] - 16) / κ
		}
		if lab.L > κ*ε {
			xyz.Y = ((lab.L + 16) / 116) * ((lab.L + 16) / 116) * ((lab.L + 16) / 116)
		} else {
			xyz.Y = lab.L / κ
		}
		if z := f[2] * f[2] * f[2]; z > ε {
			xyz.Z = z
		} else {
			xyz.Z = (116*f[2] - 16) / κ
		}
		return maf.Vec{X: xyz.X * d50White[0], Y: xyz.Y * d50White[1], Z: xyz.Z * d50White[2]}
	}
	d50Tod65 := maf.Mat3{
		0.9555766, -0.0282895, 0.0122982,
		-0.0230393, 1.0099416, -0.0204830,
		0.0631636, 0.0210077, 1.3299098,
	}
	return xyzToLinear.Transform(
		d50Tod65.Transform(labToXYZ(lab)),
	).Map(LinToGamma)
}

type rgber interface {
	Colour64() maf.Vec
}

func rgbToLab(rgb rgber) Lab {
	// https://www.w3.org/TR/css-color-4/#color-conversion-code
	d65Tod50 := maf.Mat3{
		1.0478112, 0.0295424, -0.0092345,
		0.0228866, 0.9904844, 0.0150436,
		-0.0501270, -0.0170491, 0.7521316,
	}
	xyzToLab := func(xyz maf.Vec) Lab {
		ε := 216.0 / 24389
		κ := 24389.0 / 27
		xyz = maf.Vec{
			X: xyz.X / d50White[0],
			Y: xyz.Y / d50White[1],
			Z: xyz.Z / d50White[2],
		}
		toF := func(val float64) float64 {
			if val > ε {
				return math.Cbrt(val)
			}
			return (κ*val + 16) / 116
		}
		f := [3]float64{toF(xyz.X), toF(xyz.Y), toF(xyz.Z)}
		return Lab{
			L: (116 * f[1]) - 16,
			A: 500 * (f[0] - f[1]),
			B: 200 * (f[1] - f[2]),
		}
	}
	return xyzToLab(d65Tod50.Transform(
		linearToXYZ.Transform(rgb.Colour64().Map(GammaToLin)),
	))
}

// D50 reference white.
var d50White = [3]float64{0.96422, 1.00000, 0.82521}
