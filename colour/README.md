# Package colour

```go
import "gitlab.com/fospathi/wire/colour"
```

Go package colour implements colour types. The colour types mostly copy the W3C's [CSS Color Module Level 4](https://www.w3.org/TR/css-color-4/) semantics.
