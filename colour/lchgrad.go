package colour

import (
	"errors"

	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
)

// LChGrad is a colour gradient that linearly interpolates between two LCh
// colours.
type LChGrad struct {
	from LCh
	Δh   float64
	toC  float64
	toL  float64
}

// Colour takes a parameter that varies in the range 0..1 and returns a linearly
// interpolated RGB colour with components clamped to the range 0..1.
func (g LChGrad) Colour(t float64) mech.Colour {
	return g.LCh(t).Colour().Clamped()
}

// Colours is a slice of the receiver colour gradient between the given
// proportions of the interval in the range 0..1.
func (g LChGrad) Colours(t1, t2 float64) mech.Colourer {
	from := LCh{
		L: g.from.L + t1*(g.toL-g.from.L),
		C: g.from.C + t1*(g.toC-g.from.C),
		H: g.from.H + t1*g.Δh,
	}
	to := LCh{
		L: g.from.L + t2*(g.toL-g.from.L),
		C: g.from.C + t2*(g.toC-g.from.C),
		H: g.from.H + t2*g.Δh,
	}
	return NewLChGrad(from, to.L, to.C, to.H-from.H)
}

// LCh takes a parameter that varies in the range 0..1 and returns a linearly
// interpolated colour.
func (g LChGrad) LCh(t float64) LCh {
	var h float64
	if g.Δh == 0 {
		h = g.from.H
	} else {
		h = mod360(g.from.H + g.Δh*t)
	}
	return LCh{
		L: g.from.L + t*(g.toL-g.from.L),
		C: g.from.C + t*(g.toC-g.from.C),
		H: h,
	}
}

// Uniform reports whether the gradient is a single colour.
func (g LChGrad) Uniform() bool {
	return g.Δh == 0 && g.from.C == g.toC && g.from.L == g.toL
}

// NewLChGrad constructs a new LChGrad which covers the range from the given
// colour to the absolute L and C values and the relative hue value.
//
// The delta hue is measured in degrees and is clamped to the range -360..360.
func NewLChGrad(from LCh, toL float64, toC float64, Δh float64) LChGrad {
	if Δh < -360 {
		Δh = -360
	} else if Δh > 360 {
		Δh = 360
	}
	return LChGrad{from: from, Δh: Δh, toL: toL, toC: toC}
}

// NewHueGrad constructs a new LChGrad which covers the range of hue values
// starting from the given colour to the colour whose hue is the sum of the from
// and delta hues in the direction given by the sign of the delta hue.
//
// The delta hue is measured in degrees and is clamped to the range -360..360.
func NewHueGrad(from LCh, Δh float64) LChGrad {
	if Δh < -360 {
		Δh = -360
	} else if Δh > 360 {
		Δh = 360
	}
	return LChGrad{from: from, Δh: Δh, toL: from.L, toC: from.C}
}

// NewLChMinorGrad constructs a new LChGrad which covers the (minor arc) /
// (smallest range) of hue values between the given colours.
func NewLChMinorGrad(from LCh, to LCh) (LChGrad, error) {
	cw, acw := cwAngle(from.H, to.H), antiCWAngle(from.H, to.H)
	switch {
	case cw < acw:
		return LChGrad{from: from, Δh: -cw, toL: to.L, toC: to.C}, nil
	case acw < cw:
		return LChGrad{from: from, Δh: acw, toL: to.L, toC: to.C}, nil
	default:
		return LChGrad{}, errNoSuchArc
	}
}

// NewLChMajorGrad constructs a new LChGrad which covers the (major arc) /
// (largest range) of hue values between the given colours.
func NewLChMajorGrad(from LCh, to LCh) (LChGrad, error) {
	cw, acw := cwAngle(from.H, to.H), antiCWAngle(from.H, to.H)
	switch {
	case cw > acw:
		return LChGrad{from: from, Δh: -cw, toL: to.L, toC: to.C}, nil
	case acw > cw:
		return LChGrad{from: from, Δh: acw, toL: to.L, toC: to.C}, nil
	default:
		return LChGrad{}, errNoSuchArc
	}
}

// NewMonoGrad constructs a new MonoGrad with the given constant colour.
func NewMonoGrad(col LCh) MonoGrad {
	return MonoGrad{col: col.Colour()}
}

// MonoGrad is a constant colour gradient.
type MonoGrad struct {
	col mech.Colour
}

// Colour ignores the parameter and always returns the same colour.
func (g MonoGrad) Colour(t float64) mech.Colour {
	return g.col
}

// Colours ignores the parameters and always returns the receiver colour
// gradient.
func (g MonoGrad) Colours(t1, t2 float64) mech.Colourer {
	return g
}

// Uniform reports whether the gradient is a single colour.
func (g MonoGrad) Uniform() bool {
	return true
}

// NewJoin constructs a new Join.
func NewJoin() *Join {
	return &Join{
		in: interval.In{0, 1},
	}
}

// Join LCh colour gradients into one gradient.
type Join struct {
	in   interval.In
	vg   []LChGrad
	vw   []float64
	wSum float64
}

// Append the given gradient to the join and give it the given weighting.
//
// The weight determines the given gradient's proportion of the Join's domain:
// it is ultimately divided by the sum of all the join's weights.
//
// The minimum weight is 1.
func (j *Join) Append(g LChGrad, w float64) {
	if w < 1 {
		w = 1
	}
	j.vg = append(j.vg, g)
	j.vw = append(j.vw, w)
	j.wSum += float64(w)
}

// Colour takes a parameter that varies in the range 0..1 and returns a linearly
// interpolated RGB colour with components clamped to the range 0..1.
func (j *Join) Colour(t float64) mech.Colour {
	if j.wSum == 0 {
		return mech.Colour{}
	}
	var (
		i   int
		sum float64
		w   float64
	)
	for i, w = range j.vw {
		sum += w
		if t <= float64(sum)/j.wSum {
			break
		}
	}
	grad := j.vg[i]
	start := (sum - w) / j.wSum
	width := w / j.wSum
	return grad.Colour(j.in.ClampedT((t - start) / width))
}

// Colours is a slice of the receiver gradient's colours between the argument
// colour parameters.
func (j *Join) Colours(t1, t2 float64) mech.Colourer {
	if j.wSum == 0 {
		return LChGrad{}
	}
	var (
		i1, i2     int
		sum1, sum2 float64
		w1, w2     float64
	)
	for i1, w1 = range j.vw {
		sum1 += w1
		if t1 <= sum1/j.wSum {
			break
		}
	}
	for i2, w2 = range j.vw {
		sum2 += w2
		if t2 <= sum2/j.wSum {
			break
		}
	}
	if i1 == i2 {
		return j.vg[i1].Colours(t1, t2)
	}

	forward, g1, g2 := i1 < i2, j.vg[i1], j.vg[i2]

	// Specifies the part of a to-be-divided colourer that will be part of the
	// result. There are two such colourers: the ones overlapping the limits of
	// the slice.
	type endbit struct {
		// Proportions through the domain of the colourer in the range 0..1.
		//
		// t1 and t2 can be in either direction but their values are always
		// relative to the left side of the colourer's domain.
		t1, t2 float64
		// The absolute proportion of the colourer's domain occupied by t1 to
		// t2.
		prop float64
	}
	var end1, end2 endbit
	{
		if forward {
			a, b := (sum1-w1)/j.wSum, sum1/j.wSum
			end1.t1 = (t1 - a) / (b - a)
			end1.t2 = 1
			end1.prop = 1 - end1.t1
			a, b = (sum2-w2)/j.wSum, sum2/j.wSum
			end2.t1 = 0
			end2.t2 = (t2 - a) / (b - a)
			end2.prop = end2.t2
		} else {
			a, b := (sum1-w1)/j.wSum, sum1/j.wSum
			end1.t1 = (t1 - a) / (b - a)
			end1.t2 = 0
			end1.prop = end1.t1
			a, b = (sum2-w2)/j.wSum, sum2/j.wSum
			end2.t1 = 1
			end2.t2 = (t2 - a) / (b - a)
			end2.prop = 1 - end2.t2
		}
	}

	var (
		res      = NewJoin()
		from, to LCh
		Δh1, Δh2 float64
	)
	if forward {
		Δh1, Δh2 = g1.Δh*end1.prop, g2.Δh*end2.prop
	} else {
		Δh1, Δh2 = -g1.Δh*end1.prop, -g2.Δh*end2.prop
	}
	from, to = g1.LCh(end1.t1), g1.LCh(end1.t2)
	res.Append(NewLChGrad(from, to.L, to.C, Δh1), w1*end1.prop)
	if forward {
		for i := i1 + 1; i < i2; i++ {
			res.Append(j.vg[i], j.vw[i])
		}
	} else {
		for i := i1 - 1; i > i2; i-- {
			res.Append(j.vg[i], j.vw[i])
		}
	}
	from, to = g2.LCh(end2.t1), g2.LCh(end2.t2)
	res.Append(NewLChGrad(from, to.L, to.C, Δh2), w2*end2.prop)

	return res
}

// Uniform reports whether the gradient is a single colour.
func (j *Join) Uniform() bool {
	for _, g := range j.vg {
		if !g.Uniform() {
			return false
		}
	}
	if l := len(j.vg); l > 1 {
		col := j.vg[0].Colour(0)
		for i := 1; i < l; i++ {
			if c := j.vg[i].Colour(0); c != col {
				return false
			}
		}
	}
	return true
}

// Positive anticlockwise angle where from and to are in the range 0..360.
func antiCWAngle(from, to float64) float64 {
	if to > from {
		return to - from
	}
	return to + (360 - from)
}

// Positive clockwise angle where from and to are in the range 0..360.
func cwAngle(from, to float64) float64 {
	if to < from {
		return from - to
	}
	return from + (360 - to)
}

// Argument angle can be within one revolution above 360 or below -360.
func mod360(angle float64) float64 {
	if angle > 360 {
		return angle - 360
	} else if angle < 0 {
		return angle + 360
	}
	return angle
}

// IsNoSuchArcError is whether the argument error indicates that the specified
// arc does not exist.
//
// This may happen, for example, if two LCh colours are exactly 180 degrees
// apart in hue and a minor or major hue arc was expected.
func IsNoSuchArcError(err error) bool {
	return err == errNoSuchArc
}

var errNoSuchArc error = errors.New("no such arc/range")
