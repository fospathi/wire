package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewCatenary constructs a new Catenary with the given Y-axis intercept value
// which is initially oriented with a standard frame.
func NewCatenary(a float64) *Catenary {
	return &Catenary{depictioner.Figure{Orientation: maf.StdFrame}, a}
}

// Catenary is a wireframe catenary curve figure.
//
// Relative to its orientation frame the curve is in the Z=0 plane.
type Catenary struct {
	depictioner.Figure

	// A is the Y-intercept value and scales the curve.
	A float64
}

// Depict the given portions/(X-axis intervals) on the catenary curve,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are distance.
//
// The portions' quality has units of "quadratic Beziers per unit distance" and
// when left unset a sensible default is used.
func (fig *Catenary) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCatenary(fig.Orientation, fig.A, vp)
}

// DepictCatenary depicts the given portions/(parameter intervals) on a catenary
// curve.
func DepictCatenary(
	fr maf.Frame,
	a float64,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	f := func(x float64) (float64, float64, float64) {
		e := math.Exp(x / a)
		return (a / 2) * (e + 1/e), (e - 1/e) / 2, (1 / (2 * a)) * (e + 1/e)
	}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYCartesian(p, fr, f))
	}
	return pic
}
