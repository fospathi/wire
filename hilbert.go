package wire

import (
	"gitlab.com/fospathi/wire/compgeom/hilbert"
	"gitlab.com/fospathi/wire/compgeom/hilbert/hilbert3d"
)

// NewHilbert constructs a new Hilbert curve which is initially oriented with a
// standard frame.
func NewHilbert(spec hilbert.PlotSpec) Hilbert {
	return Hilbert{NewPolyLine(hilbert.Vertices(spec))}
}

// A Hilbert is a wireframe figure that depicts a pseudo Hilbert curve occupying
// a 2D square region.
//
// Relative to its orientation frame the curve is in a plane parallel to the
// XY-plane.
type Hilbert struct {
	*PolyLine
}

// NewHilbert3D constructs a new 3D Hilbert curve which is initially oriented
// with a standard frame.
func NewHilbert3D(spec hilbert3d.PlotSpec) Hilbert {
	return Hilbert{NewPolyLine(hilbert3d.Vertices(spec))}
}

// A Hilbert3D is a wireframe figure that depicts a pseudo Hilbert curve
// occupying a 3D cube region.
//
// Relative to its orientation frame the curve is in a cube whose faces are
// parallel to each of the respective planes of the frame's coordinate system.
type Hilbert3D struct {
	*PolyLine
}
