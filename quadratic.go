package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewQuadraticBezier constructs a new QuadraticBezier which is initially
// oriented with a standard frame.
func NewQuadraticBezier(bez maf.Bez) *QuadraticBezier {
	return &QuadraticBezier{depictioner.Figure{Orientation: maf.StdFrame}, bez}
}

// A QuadraticBezier curve figure defined by three control points.
type QuadraticBezier struct {
	depictioner.Figure

	Quadratic maf.Bez
}

// Depict the given portions/(parameter intervals) on the quadratic Bezier
// curve, overwriting the previous depiction(s) if any.
//
// The portions' parameter intervals shall be clamped to the range 0..1.
func (fig *QuadraticBezier) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictQuadraticBezier(fig.Orientation, fig.Quadratic, vp)
}

// DepictQuadraticBezier depicts the given portions/(parameter intervals) on a
// quadratic Bezier curve.
func DepictQuadraticBezier(
	fr maf.Frame,
	bez maf.Bez,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	if len(vp) == 1 && occupies0To1(vp[0]) && vp[0].Qual == 1 {
		return depictioner.DepictWholeQuadraticBezier(vp[0], fr, bez)
	}
	posTan := func(t float64) (maf.Vec, maf.Vec) {
		l := bez.Tangent(t)
		return l.P, l.Dir
	}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = 1
		}
		pic = pic.Append(depictioner.DepictPlanarParametric(p, fr, posTan))
	}
	return pic
}

// occupies0To1 is true if the portion's interval covers the range 0 to 1 in
// either direction.
func occupies0To1(p mech.Portion) bool {
	switch {
	case (p.In[0] == 0) && (p.In[1] == 1):
		return true
	case (p.In[0] == 1) && (p.In[1] == 0):
		return true
	default:
		return false
	}
}
