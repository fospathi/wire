package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewSphere constructs a new Sphere which is initially oriented with a standard
// frame.
func NewSphere(spec SphereFiguration) *Sphere {
	return &Sphere{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// SphereFiguration specifies the geometry of a wireframe sphere figure.
type SphereFiguration struct {
	// The signed lengths added to the circles' radiuses for the circles
	// perpendicular to the X, Y, and Z-axes respectively.
	//
	// Using non-zero values for these can create gaps that prevent issues with
	// the otherwise overlapping curves.
	DX, DY, DZ float64

	// The number of circles perpendicular to the X, Y, and Z-axes respectively.
	NX, NY, NZ int

	// The spacing method for the circles perpendicular to the X, Y, and Z-axes
	// respectively.
	//
	// By default the circles of (that is, perpendicular to) a particular axis
	// are spaced such that their planes, when used to cut into arcs a great
	// circle on the sphere which is parallel to the axis, generate arcs that
	// subtend equal angles at the origin.
	//
	// The alternate spacing method, selected by setting the respective flag to
	// true, simply spaces them an equal perpendicular distance apart.
	SX, SY, SZ bool

	// The radius of the sphere.
	R float64
}

// A Sphere is a wireframe sphere figure.
//
// Relative to its orientation frame the sphere is depicted by a number of
// circles which are perpendicular to and centered on the X, Y, and Z axes.
type Sphere struct {
	depictioner.Figure

	SphereFiguration
}

// A SpherePortion specifies which parts of a sphere's wireframe to show.
type SpherePortion struct {
	X []mech.Portion // Portions of each of the circles perpendicular to the X-axis.
	Y []mech.Portion // Portions of each of the circles perpendicular to the Y-axis.
	Z []mech.Portion // Portions of each of the circles perpendicular to the Z-axis.
}

// Depict the given portions/(parameter intervals) on the sphere, overwriting
// the previous depiction(s) if any.
func (fig *Sphere) Depict(p SpherePortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictSphere(fig.Orientation, fig.SphereFiguration, p)
}

// DepictSphere depicts the given sphere portions/(parameter intervals) on a
// sphere.
func DepictSphere(
	fr maf.Frame,
	spec SphereFiguration,
	p SpherePortion,
) mech.Depiction {

	pic := mech.Depiction{}
	var (
		latFr maf.Frame
		r     float64
	)
	latFr, r = fr.LocalRotPosY(maf.HalfPi), spec.R+spec.DX
	pic = pic.Append(DepictLatitudes(latFr, spec.NX, r, spec.SX, p.X))
	latFr, r = fr.LocalRotPosX(-maf.HalfPi), spec.R+spec.DY
	pic = pic.Append(DepictLatitudes(latFr, spec.NY, r, spec.SY, p.Y))
	latFr, r = fr, spec.R+spec.DZ
	pic = pic.Append(DepictLatitudes(latFr, spec.NZ, r, spec.SZ, p.Z))
	return pic
}

// DepictLatitudes draws latitudinal circles parallel to the frame's XY plane.
//
// Let the Z-axis of the frame be the main north to south axis of a sphere
// centered on the frame's origin.
func DepictLatitudes(
	fr maf.Frame,
	// The number of latitude circles.
	n int,
	// The radius of the sphere.
	r float64,
	// The latitude separation mode, either equidistant or equiangular.
	equid bool,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	if n == 0 {
		return pic
	}
	var (
		vLat []maf.LatHR
	)
	if equid {
		vLat = maf.EquidistantLatitudes(r, n)
	} else {
		vLat = maf.EquiangularLatitudes(r, n)
	}
	for _, lat := range vLat {
		latFr := fr.LocalDisPosZ(lat.H)
		pic = pic.Append(DepictCircle(latFr, lat.R, vp))
	}
	return pic
}
