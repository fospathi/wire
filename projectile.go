package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewTimeDepProjectile constructs a new Projectile which is initially oriented
// with a standard frame.
func NewTimeDepProjectile(spec kinem.Projectile) *TimeDepProjectile {
	return &TimeDepProjectile{
		depictioner.Figure{Orientation: maf.StdFrame},
		spec,
	}
}

// TimeDepProjectile is a wireframe parabola figure.
//
// Relative to its orientation frame the projectile is in the Z=0 plane and
// intersects the origin at which point it has the given velocity.
type TimeDepProjectile struct {
	depictioner.Figure

	kinem.Projectile
}

// Depict the given portions/(parameter intervals) on the projectile,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are time.
//
// The portions' quality has units of "quadratic Beziers per unit time" and when
// left unset a sensible default is used.
func (fig *TimeDepProjectile) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig =
		DepictTimeDepProjectile(fig.Orientation, fig.Projectile, vp)
}

// DepictTimeDepProjectile depicts the given portions/(parameter intervals) on a
// projectile.
func DepictTimeDepProjectile(
	fr maf.Frame,
	spec kinem.Projectile,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewTimeDepProjectilePosTan(spec)
	const projectileQual = 10
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = projectileQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen()))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}
