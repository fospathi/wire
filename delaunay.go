package wire

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/compgeom/delaunay"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewDelaunayTriangulation constructs a new Delaunay triangulation which is
// initially oriented with a standard frame.
func NewDelaunayTriangulation(
	spec DelaunayFiguration,
) *DelaunayTriangulation {

	pointSet := dryad.Set[maf.Vec2]{}
	pointSet.AddAll(spec.P...)
	tgn := delaunay.NewTriangulation(pointSet)

	var segs []maf.LineSeg2
	for e := range tgn.EdgeUsers {
		segs = append(segs, e)
	}

	return &DelaunayTriangulation{
		depictioner.Figure{Orientation: maf.StdFrame},
		spec,
		segs,
	}
}

// DelaunayFiguration specifies the geometry of a wireframe Delaunay
// triangulation figure.
type DelaunayFiguration struct {
	// The vertices of the triangles.
	P []maf.Vec2
}

// A DelaunayTriangulation is a wireframe Delaunay triangulation figure.
type DelaunayTriangulation struct {
	depictioner.Figure

	DelaunayFiguration

	Segs []maf.LineSeg2
}

// Depict the given portions/(parameter intervals) on the triangulation,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and they are
// in units of distance from the triangulation frame origin.
//
// The portions' quality is the literal number of quadratic Beziers to use for
// each line segment.
func (fig *DelaunayTriangulation) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictDelaunayTriangulation(fig.Orientation, fig.Segs, vp)
}

// DepictDelaunayTriangulation depicts the given portions/(parameter intervals)
// on a Delaunay triangulation.
func DepictDelaunayTriangulation(
	fr maf.Frame,
	segs []maf.LineSeg2,
	vp []mech.Portion,
) mech.Depiction {
	l := len(segs)
	if l == 0 {
		return mech.Depiction{}
	}

	pic := mech.Depiction{}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = LineQual
		}
		//r1, r2 :=
		a := maf.Annulus2{R1: min(p.In[0], p.In[1]), R2: max(p.In[0], p.In[1])}

		for _, seg := range segs {
			vSeg, n := a.LineSegIntersection(seg)
			if n == 0 {
				continue
			}

			t1, t2 := seg.T(vSeg[0].P1), seg.T(vSeg[0].P2)
			pic = pic.Append(depictioner.DepictLine(mech.Portion{
				Caps:     p.Caps,
				Colourer: p.Colours(t1, t2),
				In:       interval.In{t1, t2},
				Qual:     p.Qual,
				Th:       [2]float64{p.Thickness(t1), p.Thickness(t2)},
			}, fr, seg.Higher(0)))

			if n == 1 {
				continue
			}

			t1, t2 = seg.T(vSeg[1].P1), seg.T(vSeg[1].P2)
			pic = pic.Append(depictioner.DepictLine(mech.Portion{
				Caps:     p.Caps,
				Colourer: p.Colours(t1, t2),
				In:       interval.In{t1, t2},
				Qual:     p.Qual,
				Th:       [2]float64{p.Thickness(t1), p.Thickness(t2)},
			}, fr, seg.Higher(0)))
		}
	}

	return pic
}
