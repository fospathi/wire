package wire

import (
	"gitlab.com/fospathi/wire/compgeom/mcworter"
)

// NewPentadentrite constructs a new Pentadentrite curve which is initially
// oriented with a standard frame.
func NewPentadentrite(spec mcworter.PentadentritePlotSpec) Pentadentrite {
	return Pentadentrite{NewPolyLine(mcworter.PentadentriteVertices(spec))}
}

// A Pentadentrite is a wireframe figure that depicts a McWorter pentadentrite
// curve.
type Pentadentrite struct {
	*PolyLine
}
