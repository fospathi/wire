package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewHelix constructs a new Helix which is initially oriented with a standard
// frame.
func NewHelix(spec kirv.Helix) *Helix {
	return &Helix{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// Helix is a wireframe helix figure.
//
// Relative to its orientation frame the base of the helix is in the Z=0 plane
// and is centered on the origin.
type Helix struct {
	depictioner.Figure

	kirv.Helix
}

// Depict the given portions/(parameter intervals) on the helix, overwriting the
// previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians" and
// when left unset a sensible default is used.
func (fig *Helix) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictHelix(fig.Orientation, fig.Helix, vp)
}

// DepictHelix depicts the given portions/(parameter intervals) on a helix.
func DepictHelix(
	fr maf.Frame,
	spec kirv.Helix,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewHelixPosTan(spec)
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictParametric(p, fr, posTan))
	}

	return pic
}
