module gitlab.com/fospathi/wire

go 1.21

require (
	github.com/bcmills/unsafeslice v0.2.0
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20231102141658-eca20e8abded
	github.com/gorilla/websocket v1.5.0
	gitlab.com/fospathi/dryad v0.0.0-20231105005730-60ed499ed55b
	gitlab.com/fospathi/maf v0.0.0-20231105005409-5193b701a591
	gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769
)

require golang.org/x/exp v0.0.0-20231006140011-7918f672742d // indirect

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/universal => ../universal
