package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/compgeom/sierpinski"
)

// NewSierpinski constructs a new Sierpinski curve which is initially oriented
// with a standard frame.
func NewSierpinski(spec sierpinski.PlotSpec) Sierpinski {
	points := sierpinski.Vertices(spec)
	l := len(points)
	square := make([]maf.Vec, 2*l)
	rot := maf.NewZRot(maf.QuartPi)
	for i := 0; i < l; i++ {
		square[i] = rot.Transform(points[i])
	}
	rot = maf.NewZRot(-(maf.QuartPi + maf.HalfPi))
	for i := 0; i < l; i++ {
		square[l+i] = rot.Transform(points[i])
	}
	return Sierpinski{NewPolyLine(square)}
}

// A Sierpinski is a wireframe figure that depicts a Sierpinski curve occupying
// a 2D square region.
//
// Relative to its orientation frame the curve's region is parallel to the
// XY-plane and axis-aligned.
type Sierpinski struct {
	*PolyLine
}

// NewSierpinskiTriangle constructs a new SierpinskiTriangle curve which is
// initially oriented with a standard frame.
func NewSierpinskiTriangle(spec sierpinski.PlotSpec) SierpinskiTriangle {
	return SierpinskiTriangle{NewPolyLine(sierpinski.Vertices(spec))}
}

// A SierpinskiTriangle is a wireframe figure that depicts a Sierpinski curve
// occupying a 2D right isosceles region.
//
// Relative to its orientation frame the curve's region is parallel to the
// XY-plane and the hypotenuse is axis aligned.
type SierpinskiTriangle struct {
	*PolyLine
}
