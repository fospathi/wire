package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewCycloid constructs a new Cycloid, generated by a point on a circle with
// the given radius rolling along the positive X-axis, which is initially
// oriented with a standard frame.
func NewCycloid(r float64) *Cycloid {
	return &Cycloid{depictioner.Figure{Orientation: maf.StdFrame}, r}
}

// Cycloid is a wireframe cycloid figure.
//
// Relative to its orientation frame the cycloid is in the Z=0 plane and it
// begins at the origin and ends on the positive X-axis.
type Cycloid struct {
	depictioner.Figure

	// R is the radius of the rolling circle which generates the cycloid.
	R float64
}

// Depict the given portions/(parameter intervals) on the cycloid, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians", which
// is effectively the whole cycloid since the valid parameter range is from 0 to
// 2π, and when left unset a sensible default is used.
func (fig *Cycloid) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCycloid(fig.Orientation, fig.R, vp)
}

// DepictCycloid depicts the given portions/(parameter intervals) on a cycloid.
func DepictCycloid(
	fr maf.Frame,
	r float64,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewCycloidPosTan(r)
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}
