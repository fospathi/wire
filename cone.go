package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewCone constructs a new Cone which is initially oriented with a standard
// frame.
func NewCone(spec ConeFiguration) *Cone {
	return &Cone{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// ConeFiguration specifies the geometry of a wireframe cone figure.
//
// Cone terminology:
//  * apex: the pointed tip.
//  * axis: straight line passing through the apex, about which the base has a
//    circular symmetry.
//  * directrix: the perimeter of the base.
//  * generatrix: lateral line segments between the directrix and apex.
type ConeFiguration struct {
	// The number of concentric circles on the flat base, including the
	// directrix.
	CC int
	// The number of lateral circles, perpendicular to the axis, on the slanted
	// surface but not including the directrix.
	LC int

	// The number of diameter line segments on the base.
	D int
	// The number of pairs of generatrix.
	G int

	// The perpendicular distance from the apex to the base, that is, the cone's
	// height.
	H float64
	// The radius of the base.
	R float64
}

// A Cone is a wireframe right circular cone figure.
//
// Relative to its orientation frame the cone base is in the Z=0 plane and is
// centered on the origin.
type Cone struct {
	depictioner.Figure

	ConeFiguration
}

// A ConePortion specifies which parts of a cone's wireframe to show.
type ConePortion struct {
	Concentric []mech.Portion // Portions of each of the concentric base circles excluding the directrix.
	Directrix  []mech.Portion // Portions of the directrix.
	Generatrix []mech.Portion // Portions of each of the generatrices.
	Lateral    []mech.Portion // Portions of each of the lateral circles excluding the directrix.
}

// Depict the given portions/(parameter intervals) on the cone, overwriting the
// previous depiction(s) if any.
func (fig *Cone) Depict(p ConePortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCone(fig.Orientation, fig.ConeFiguration, p)
}

// DepictCone depicts the given cone portions/(parameter intervals) on a cone.
func DepictCone(
	fr maf.Frame,
	spec ConeFiguration,
	p ConePortion,
) mech.Depiction {

	pic := mech.Depiction{}
	{ // Base circles.
		pic = pic.Append(DepictCircle(fr, spec.R, p.Directrix))
		for i := 1; i < spec.CC; i++ {
			t := float64(i) / float64(spec.CC)
			r := t * spec.R
			pic = pic.Append(DepictCircle(fr, r, p.Concentric))
		}
	}
	{ // Lateral circles.
		for i := 1; i <= spec.LC; i++ {
			t := float64(i) / float64(spec.LC+1)
			h, r := t*spec.H, (1-t)*spec.R
			pic = pic.Append(DepictCircle(fr.LocalDisPosZ(h), r, p.Lateral))
		}
	}
	return pic
}
