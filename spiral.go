package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewTimeDepArchimedean constructs a new TimeDepArchimedean which is initially
// oriented with a standard frame.
func NewTimeDepArchimedean(spec kinem.Archimedean) *TimeDepArchimedean {
	return &TimeDepArchimedean{
		depictioner.Figure{Orientation: maf.StdFrame},
		spec,
	}
}

// TimeDepArchimedean is a wireframe Archimedean spiral figure.
//
// Relative to its orientation frame the spiral is in the Z=0 plane and is
// centered on the origin.
type TimeDepArchimedean struct {
	depictioner.Figure

	kinem.Archimedean
}

// Depict the given portions/(parameter intervals) on the Archimedean spiral,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are time.
//
// The portions' quality has units of "quadratic Beziers per revolution" and
// when left unset a sensible default is used.
func (fig *TimeDepArchimedean) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig =
		DepictTimeDepArchimedean(fig.Orientation, fig.Archimedean, vp)
}

// DepictTimeDepArchimedean depicts the given portions/(parameter intervals) on
// an Archimedean spiral.
func DepictTimeDepArchimedean(
	fr maf.Frame,
	spec kinem.Archimedean,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewTimeDepArchimedeanPosTan(spec)
	period := maf.TwoPi / spec.W
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / period))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}
