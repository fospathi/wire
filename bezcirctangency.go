package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewBezCircTangency constructs a new BezCircTangency which is initially
// oriented with a standard frame.
func NewBezCircTangency(spec BezCircTangencyFiguration) *BezCircTangency {
	return &BezCircTangency{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// BezCircTangencyFiguration specifies the input geometry required for
// visualizing the tangency to a circle by a quadratic Bezier curve.
//
// There are in general two solutions referred to here as the closer branch
// solution and the farther branch solution. Each solution is comprised of two
// distinct point sets, one point set being a circular arc of tangentially
// touched points on the circle and the other being the associated tangency
// locus for the middle control point of the quadratic Bezier.
//
// The closer branch solution consists of i.) the arc of tangentially touched
// points on the circle which is closer to the non-middle control points of the
// Bezier and ii.) the associated tangency locus for the middle control point.
//
// The farther branch solution consists of i.) the arc of tangentially touched
// points on the circle which is farther from the non-middle control points of
// the Bezier and ii.) the associated tangency locus for the middle control
// point.
type BezCircTangencyFiguration struct {
	// Bez has its non-middle control points A and C fixed.
	//
	// The middle control point B shall be non-collinear with the other two and
	// serves only to define the curve's intrinsic plane.
	//
	// The line AC shall be outside the sphere.
	Bez maf.Bez

	// Sphere shall intersect the plane of the Bezier curve thus generating the
	// circle that the Bezier curve touches tangentially.
	Sphere maf.Sphere

	// CloserBranchT and FartherBranchT give the proportion of the angle, in the
	// range 0..1, through their respective angle interval for their respective
	// branch.
	CloserBranchT, FartherBranchT float64

	// The radius of the circle displayed around the tangent point of the
	// respective branch.
	CloserBranchTangentCircleR, FartherBranchTangentCircleR float64
	// The radius of the circle displayed around the middle control point of the
	// respective branch.
	CloserBranchControlCircleR, FartherBranchControlCircleR float64

	// Put some distance between the branch depictions and the main circle with
	// a non-zero offset.
	CloserBranchZOffset, FartherBranchZOffset float64
}

// BezCircTangency is a wireframe tangency curve figure for the tangency to a
// circle by a quadratic Bezier curve.
type BezCircTangency struct {
	depictioner.Figure

	BezCircTangencyFiguration
}

// BezCircTangencyPortion specifies which parts of a BezCircTangency's wireframe
// to show.
//
// The arc, Bezier, and locus portions' parameter intervals are in unitless
// proportions of their respective branches angular domains and shall be clamped
// to the range 0..1.
//
// The circle, control circle, and tangent circle portions' parameter intervals
// are in radians.
type BezCircTangencyPortion struct {

	// Arcs, circles, Beziers.

	CloserBranchArc    []mech.Portion
	CloserBranchBezier []mech.Portion
	CloserBranchLocus  []mech.Portion

	FartherBranchArc    []mech.Portion
	FartherBranchBezier []mech.Portion
	FartherBranchLocus  []mech.Portion

	// Circles.

	Circle []mech.Portion

	CloserBranchControlCircle []mech.Portion
	CloserBranchTangentCircle []mech.Portion

	FartherBranchControlCircle []mech.Portion
	FartherBranchTangentCircle []mech.Portion

	// Lines.

	CloserBranchAB []mech.Portion
	CloserBranchCB []mech.Portion

	FartherBranchAB []mech.Portion
	FartherBranchCB []mech.Portion

	AC []mech.Portion
}

func (p BezCircTangencyPortion) scaledPortion(
	tang maf.BezCircleTangency,
) BezCircTangencyPortion {

	var (
		closerAdjuster  = tang.Closer.TSub
		fartherAdjuster = tang.Farther.TSub
	)

	p.CloserBranchArc = adjustIn(p.CloserBranchArc, closerAdjuster)
	p.CloserBranchLocus = adjustIn(p.CloserBranchLocus, closerAdjuster)

	p.FartherBranchArc = adjustIn(p.FartherBranchArc, fartherAdjuster)
	p.FartherBranchLocus = adjustIn(p.FartherBranchLocus, fartherAdjuster)

	return p
}

// Map the given portions into a new slice with adjusted intervals.
func adjustIn(
	vp []mech.Portion,
	adjuster func(interval.In) interval.In,
) []mech.Portion {

	l := len(vp)
	if l == 0 {
		return nil
	}
	var (
		por mech.Portion
		res = make([]mech.Portion, l)
	)
	for i := 0; i < l; i++ {
		por = vp[i]
		por.In = adjuster(por.In)
		res[i] = por
	}
	return res
}

// Depict the given portions/(parameter intervals) on the Bezier circle
// tangency, overwriting the previous depiction(s) if any.
func (fig *BezCircTangency) Depict(p BezCircTangencyPortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictBezCircTangency(
		fig.Orientation,
		fig.BezCircTangencyFiguration,
		p,
	)
}

// DepictBezCircTangency depicts the given portions/(parameter intervals) on a
// Bezier circle tangency.
func DepictBezCircTangency(
	fr maf.Frame,
	spec BezCircTangencyFiguration,
	p BezCircTangencyPortion,
) mech.Depiction {

	var (
		pic  = mech.Depiction{}
		tang = spec.Bez.CircleTangency(spec.Sphere)

		closerAngle  = tang.Closer.T(spec.CloserBranchT)
		fartherAngle = tang.Farther.T(spec.FartherBranchT)

		closerB  = tang.B(closerAngle)
		fartherB = tang.B(fartherAngle)

		out       = fr.Out()
		circFrame = maf.Frame{
			O:    tang.Circle.C,
			Axes: tang.AnglesBasis,
		}.Global(out)
	)

	p = p.scaledPortion(tang)

	// Main circle.

	if len(p.Circle) > 0 {
		pic = pic.Append(DepictCircle(circFrame, tang.Circle.R, p.Circle))
	}

	// Base line.

	if len(p.AC) > 0 {
		pic = pic.Append(DepictLineSeg(
			fr, maf.LineSeg{P1: tang.A, P2: tang.C}, p.AC,
		))
	}

	// Closer branch.

	if z := spec.CloserBranchZOffset; z != 0 {
		fr = fr.LocalDis(circFrame.Axes.Z.Scale(-z))
		circFrame = circFrame.LocalDisPosZ(-z)
	}

	if len(p.CloserBranchArc) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame,
			tang.Circle.R,
			p.CloserBranchArc,
		))
	}

	if len(p.CloserBranchBezier) > 0 {
		pic = pic.Append(DepictQuadraticBezier(
			fr, tang.TangentialBez(closerAngle), p.CloserBranchBezier,
		))
	}

	if len(p.CloserBranchLocus) > 0 {
		pic = pic.Append(DepictPlanarParametric(
			fr,
			kirv.NumericalPosInwardsTanFunc(tang.B, tang.Closer.Midpoint()),
			p.CloserBranchLocus,
		))
	}

	if len(p.CloserBranchControlCircle) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame.GlobalDis(tang.Circle.C.To(closerB)),
			spec.CloserBranchControlCircleR,
			p.CloserBranchControlCircle,
		))
	}

	if len(p.CloserBranchTangentCircle) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame.GlobalDis(tang.Circle.C.To(tang.T(closerAngle))),
			spec.CloserBranchTangentCircleR,
			p.CloserBranchTangentCircle,
		))
	}

	if len(p.CloserBranchAB) > 0 {
		pic = pic.Append(DepictLineSeg(
			fr, maf.LineSeg{P1: tang.A, P2: closerB}, p.CloserBranchAB,
		))
	}

	if len(p.CloserBranchCB) > 0 {
		pic = pic.Append(DepictLineSeg(
			fr, maf.LineSeg{P1: tang.C, P2: closerB}, p.CloserBranchCB,
		))
	}

	if z := spec.CloserBranchZOffset; z != 0 {
		fr = fr.LocalDis(circFrame.Axes.Z.Scale(z))
		circFrame = circFrame.LocalDisPosZ(z)
	}

	// Farther branch.

	if z := spec.FartherBranchZOffset; z != 0 {
		fr = fr.LocalDis(circFrame.Axes.Z.Scale(-z))
		circFrame = circFrame.LocalDisPosZ(-z)
	}

	if len(p.FartherBranchArc) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame,
			tang.Circle.R,
			p.FartherBranchArc,
		))
	}

	if len(p.FartherBranchBezier) > 0 {
		pic = pic.Append(DepictQuadraticBezier(
			fr, tang.TangentialBez(fartherAngle), p.FartherBranchBezier,
		))
	}

	if len(p.FartherBranchLocus) > 0 {
		pic = pic.Append(DepictPlanarParametric(
			fr,
			kirv.NumericalPosInwardsTanFunc(tang.B, tang.Farther.Midpoint()),
			p.FartherBranchLocus,
		))
	}

	if len(p.FartherBranchControlCircle) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame.GlobalDis(tang.Circle.C.To(fartherB)),
			spec.FartherBranchControlCircleR,
			p.FartherBranchControlCircle,
		))
	}

	if len(p.FartherBranchTangentCircle) > 0 {
		pic = pic.Append(DepictCircle(
			circFrame.GlobalDis(tang.Circle.C.To(tang.T(fartherAngle))),
			spec.FartherBranchTangentCircleR,
			p.FartherBranchTangentCircle,
		))
	}

	if len(p.FartherBranchAB) > 0 {
		pic = pic.Append(DepictLineSeg(
			fr, maf.LineSeg{P1: tang.A, P2: fartherB}, p.FartherBranchAB,
		))
	}

	if len(p.FartherBranchCB) > 0 {
		pic = pic.Append(DepictLineSeg(
			fr, maf.LineSeg{P1: tang.C, P2: fartherB}, p.FartherBranchCB,
		))
	}

	return pic
}
