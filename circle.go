package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewCircle constructs a new Circle with the given radius which is initially
// oriented with a standard frame.
func NewCircle(r float64) *Circle {
	return &Circle{depictioner.Figure{Orientation: maf.StdFrame}, r}
}

// Circle is a wireframe circle figure.
//
// Relative to its orientation frame the circle is in the Z=0 plane and is
// centered on the origin.
type Circle struct {
	depictioner.Figure

	// R is the circle's radius.
	R float64
}

// Depict the given portions/(parameter intervals) on the circle, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians" and
// when left unset a sensible default is used.
func (fig *Circle) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCircle(fig.Orientation, fig.R, vp)
}

// DepictCircle depicts the given portions/(parameter intervals) on a circle.
func DepictCircle(
	fr maf.Frame,
	r float64,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewCirclePosTan(r)
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}
