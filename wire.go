package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

const (
	// TrigQual is the default quality of a sin figure and some other
	// trigonometric figures.
	TrigQual = 12

	// LineQual is the default quality of a line figure and some other linear
	// figures.
	LineQual = 10
)

// DepictPlanarParametric depicts the given portions on the parametric curve
// defined by the given position tangent function.
func DepictPlanarParametric(
	fr maf.Frame,
	posTan func(t float64) (maf.Vec, maf.Vec),
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	for _, p := range vp {
		pic = pic.Append(depictioner.DepictPlanarParametric(p, fr, posTan))
	}
	return pic
}
