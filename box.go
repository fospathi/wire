package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewCube constructs a new Cube with the given side length which is initially
// oriented with a standard frame.
func NewCube(l float64) *Cube {
	return &Cube{depictioner.Figure{Orientation: maf.StdFrame}, l}
}

// A Cube is a wireframe cube figure.
//
// Relative to its orientation frame the cube is centered on the origin.
type Cube struct {
	depictioner.Figure

	// L is the length of any side of the cube.
	L float64
}

// A CubePortion specifies which parts of a cube's wireframe to show.
type CubePortion struct {
	Bottom   []mech.Portion // Portions of each of the four bottom lines.
	Top      []mech.Portion // Portions of each of the four top lines.
	Vertical []mech.Portion // Portions of each of the four vertical lines.
}

// Depict the given portions/(parameter intervals) on the cube, overwriting the
// previous depiction(s) if any.
func (fig *Cube) Depict(p CubePortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCube(fig.Orientation, fig.L, p)
}

// DepictCube depicts the given cube portions/(parameter intervals) on a cube.
func DepictCube(
	fr maf.Frame,
	l float64,
	p CubePortion,
) mech.Depiction {

	pic := mech.Depiction{}

	//      E  _____________  H
	//       /|            /|               The origin is at the centre of the
	//      / |           / |               cube.
	//     /  |          /  |
	//  A /_____________/ D |
	//    |   |_________|___|
	//    | F /         |   / G
	//    |  /          |  /
	//    | /           | /
	//    |/____________|/
	//  B                 C
	l = l / 2
	a := maf.Vec{X: -l, Y: l, Z: l}
	b := maf.Vec{X: -l, Y: -l, Z: l}
	c := maf.Vec{X: l, Y: -l, Z: l}
	d := maf.Vec{X: l, Y: l, Z: l}
	e := maf.Vec{X: -l, Y: l, Z: -l}
	f := maf.Vec{X: -l, Y: -l, Z: -l}
	g := maf.Vec{X: l, Y: -l, Z: -l}
	h := maf.Vec{X: l, Y: l, Z: -l}

	{ // Vertical lines.
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: b, P2: a}, p.Vertical))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: c, P2: d}, p.Vertical))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: f, P2: e}, p.Vertical))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: g, P2: h}, p.Vertical))
	}
	{ // Top lines.
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: a, P2: d}, p.Top))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: d, P2: h}, p.Top))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: h, P2: e}, p.Top))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: e, P2: a}, p.Top))
	}
	{ // Bottom lines.
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: b, P2: c}, p.Bottom))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: c, P2: g}, p.Bottom))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: g, P2: f}, p.Bottom))
		pic = pic.Append(
			DepictLineSeg(fr, maf.LineSeg{P1: f, P2: b}, p.Bottom))
	}
	return pic
}
