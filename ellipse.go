package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewEllipse constructs a new Ellipse with the given properties which is
// initially oriented with a standard frame.
func NewEllipse(spec kirv.Ellipse) *Ellipse {
	return &Ellipse{
		Figure: depictioner.Figure{Orientation: maf.StdFrame},
		A:      spec.A,
		B:      spec.B,
	}
}

// Ellipse is a wireframe ellipse figure.
//
// Relative to its orientation frame the ellipse is in the Z=0 plane and is
// centered on the origin.
type Ellipse struct {
	depictioner.Figure

	// A is the ellipse's semi-major axis.
	A float64
	// B is the ellipse's semi-minor axis.
	B float64
}

// Depict the given portions/(parameter intervals) on the ellipse, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians" and
// when left unset a sensible default is used.
func (fig *Ellipse) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictEllipse(fig.Orientation, fig.A, fig.B, vp)
}

// DepictEllipse depicts the given portions/(parameter intervals) on an ellipse.
func DepictEllipse(
	fr maf.Frame,
	a float64,
	b float64,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewEllipsePosTan(kirv.Ellipse{A: a, B: b})
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}
