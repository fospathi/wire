package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewSinusoid constructs a new Sinusoid which is initially oriented with a
// standard frame.
func NewSinusoid(spec SinusoidFiguration) *Sinusoid {
	return &Sinusoid{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// SinusoidFiguration specifies the geometry of a wireframe sine wave figure.
type SinusoidFiguration struct {
	Sinusoid kirv.Sinusoid
	ExpDecay kirv.ExpDecay
	// The time value.
	T float64
}

// Sinusoid is a wireframe sine wave figure.
//
// Relative to its orientation frame the curve is in the Z=0 plane.
type Sinusoid struct {
	depictioner.Figure

	SinusoidFiguration
}

// Depict the given portions/(parameter intervals) on the sinusoid, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are distance.
//
// The portions' quality has units of "quadratic Beziers per wavelength" and
// when left unset a sensible default is used.
func (fig *Sinusoid) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig =
		DepictSinusoid(fig.Orientation, fig.SinusoidFiguration, vp)
}

// DepictSinusoid depicts the given portions/(parameter intervals) on a
// sinusoid.
func DepictSinusoid(
	fr maf.Frame,
	spec SinusoidFiguration,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	posTan := kirv.NewSinusoidPosTan(spec.Sinusoid, spec.ExpDecay)(spec.T)
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / spec.Sinusoid.L))
		pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
	}
	return pic
}

// NewStandingWave constructs a new StandingWave which is initially oriented
// with a standard frame.
func NewStandingWave(spec StandingWaveFiguration) *StandingWave {
	return &StandingWave{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// StandingWaveFiguration specifies the geometry of a wireframe sinusoidal
// standing wave figure.
type StandingWaveFiguration struct {
	Standing kirv.Standing
	ExpDecay kirv.ExpDecay
	// The time value.
	T float64

	// Put some distance between the two constituent right/left moving wave
	// depictions and the main standing wave with a non-zero offset.
	RightwardsZOffset, LeftwardsZOffset float64
}

// StandingWave is a wireframe sinusoidal standing wave figure.
//
// Relative to its orientation frame the curve is in the Z=0 plane.
type StandingWave struct {
	depictioner.Figure

	StandingWaveFiguration
}

// StandingWavePortion specifies which parts of a standing wave's wireframe to
// show along with the two sinusoids that combine to form the standing wave.
type StandingWavePortion struct {
	Standing   []mech.Portion
	Rightwards []mech.Portion
	Leftwards  []mech.Portion
}

// Depict the given portions/(parameter intervals) on the standing wave,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are distance.
//
// The portions' quality has units of "quadratic Beziers per wavelength" and
// when left unset a sensible default is used.
func (fig *StandingWave) Depict(p StandingWavePortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig =
		DepictStandingWave(fig.Orientation, fig.StandingWaveFiguration, p)
}

// DepictStandingWave depicts the given portions/(parameter intervals) on a
// sinusoidal standing wave.
func DepictStandingWave(
	fr maf.Frame,
	spec StandingWaveFiguration,
	p StandingWavePortion,
) mech.Depiction {

	pic := mech.Depiction{}

	if len(p.Standing) > 0 {
		posTan := kirv.NewStandingPosTan(spec.Standing, spec.ExpDecay)(spec.T)
		for _, p := range p.Standing {
			if p.Qual < 1 {
				p.Qual = TrigQual
			}
			p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / spec.Standing.L))
			pic = pic.Append(depictioner.DepictXYParametric(p, fr, posTan))
		}
	}

	if len(p.Rightwards) > 0 {
		pic = pic.Append(DepictSinusoid(
			fr.LocalDisPosZ(spec.RightwardsZOffset),
			SinusoidFiguration{
				Sinusoid: kirv.Sinusoid{
					A: spec.Standing.A / 2,
					L: spec.Standing.L,
					V: spec.Standing.L * (spec.Standing.W / maf.TwoPi),
				},
				ExpDecay: spec.ExpDecay,
				T:        spec.T,
			},
			p.Rightwards,
		))
	}

	if len(p.Leftwards) > 0 {
		pic = pic.Append(DepictSinusoid(
			fr.LocalDisPosZ(spec.LeftwardsZOffset),
			SinusoidFiguration{
				Sinusoid: kirv.Sinusoid{
					A: spec.Standing.A / 2,
					L: spec.Standing.L,
					V: spec.Standing.L * (spec.Standing.W / maf.TwoPi),
				},
				ExpDecay: spec.ExpDecay,
				T:        -spec.T,
			},
			p.Leftwards,
		))
	}

	return pic
}
