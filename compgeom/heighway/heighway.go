package heighway

import (
	"math"

	"gitlab.com/fospathi/maf"
)

var (
	// Heighway.
	rotAnti  = maf.NewZRot(math.Pi / 4)
	rotClock = maf.NewZRot(-math.Pi / 4)
	rat      = 1 / math.Sqrt2 // Hypotenuse to leg length in a right isosceles triangle.

	// Golden.
	ratGold       = math.Pow(1/math.Phi, 1/math.Phi)
	ratGold2      = ratGold * ratGold
	rotGoldAnti   = maf.NewZRot(maf.CosineRule(ratGold, 1.0, ratGold2))
	rotGoldClock2 = maf.NewZRot(-maf.CosineRule(ratGold2, 1.0, ratGold))

	// Terdragon.
	rotTer = maf.NewZRot(math.Pi / 6)
	ratTer = 1 / math.Sqrt(3)
)

func Magnify(seg maf.LineSeg, anti bool) [2]maf.LineSeg {
	rot := rotClock
	if anti {
		rot = rotAnti
	}
	p := seg.P1.Add(rot.Transform(seg.P1.To(seg.P2).Scale(rat)))
	return [2]maf.LineSeg{
		{P1: seg.P1, P2: p},
		{P1: p, P2: seg.P2},
	}
}

func MagnifyGolden(seg maf.LineSeg, anti bool) [2]maf.LineSeg {
	var (
		rat float64
		rot maf.Aff
	)
	if anti {
		rat = ratGold
		rot = rotGoldAnti
	} else {
		rat = ratGold2
		rot = rotGoldClock2
	}
	p := seg.P1.Add(rot.Transform(seg.P1.To(seg.P2).Scale(rat)))
	return [2]maf.LineSeg{
		{P1: seg.P1, P2: p},
		{P1: p, P2: seg.P2},
	}
}

func MagnifyTerdragon(seg maf.LineSeg) [3]maf.LineSeg {
	v := rotTer.Transform(seg.P1.To(seg.P2).Scale(ratTer))
	p1 := seg.P1.Add(v)
	p2 := seg.P2.Subtract(v)
	return [3]maf.LineSeg{
		{P1: seg.P1, P2: p1},
		{P1: p1, P2: p2},
		{P1: p2, P2: seg.P2},
	}
}
