package heighway

import (
	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// PlotSpec specifies the properties of a Heighway Dragon curve.
type PlotSpec struct {
	// Iterations of the algorithm.
	Iterations int
	// Reflect the points along the given axis.
	ReflectX, ReflectY bool
	// Seg is the line segment to which the iterative division process is
	// applied.
	//
	// Seg shall be parallel to the Z=0 XY-plane.
	Seg maf.LineSeg
}

// xRefl is a reflection transformation that will reverse the left-right
// direction of the curve's vertices.
func (spec PlotSpec) xRefl() maf.Aff {
	dir := spec.Seg.P1.To(spec.Seg.P2).Unit()
	return maf.NewRefl(maf.Plane{
		P:      spec.Seg.Midpoint(),
		Normal: dir,
	})
}

// yRefl is a reflection transformation that will reverse the up-down direction
// of the curve's vertices.
func (spec PlotSpec) yRefl() maf.Aff {
	dir := spec.Seg.P1.To(spec.Seg.P2).Unit()
	return maf.NewRefl(maf.Plane{
		P:      spec.Seg.Midpoint(),
		Normal: maf.Vec{X: -dir.Y, Y: dir.X, Z: dir.Z},
	})
}

// Vertices of a Heighway Dragon curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: spec.Seg,
	})

	for i := 0; i < spec.Iterations; i++ {
		anti := false
		for _, node := range eqnode.Leaves(root) {
			vSeg := Magnify(node.Content, anti)
			anti = !anti
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[0]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[1]})
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	if spec.ReflectX {
		l := len(vp)
		refl := spec.xRefl()
		for i := 0; i < l; i++ {
			vp[i] = refl.Transform(vp[i])
		}
	}

	if spec.ReflectY {
		l := len(vp)
		refl := spec.yRefl()
		for i := 0; i < l; i++ {
			vp[i] = refl.Transform(vp[i])
		}
	}

	return slices.Compact(vp)
}

// TerdragonPlotSpec specifies the properties of a Terdragon curve.
type TerdragonPlotSpec struct {
	// Iterations of the algorithm.
	Iterations int
	// Seg is the line segment to which the iterative division process is
	// applied.
	//
	// Seg shall be parallel to the Z=0 XY-plane.
	Seg maf.LineSeg
}

// TerdragonVertices are the vertices of a Terdragon curve.
func TerdragonVertices(spec TerdragonPlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: spec.Seg,
	})

	for i := 0; i < spec.Iterations; i++ {
		for _, node := range eqnode.Leaves(root) {
			vSeg := MagnifyTerdragon(node.Content)
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[0]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[1]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[2]})
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}

// FudgeflakePlotSpec specifies the properties of a Heighway fudgeflake curve.
type FudgeflakePlotSpec struct {
	// Iterations of the algorithm.
	Iterations int
	// Equilateral is the equilateral triangle whose edges are the subject of
	// the iterative division process.
	//
	// The triangle shall be parallel to the Z=0 XY-plane.
	Equilateral maf.Triangle
}

// FudgeflakeVertices are the vertices of a Heighway fudgeflake curve.
func FudgeflakeVertices(spec FudgeflakePlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.A, P2: spec.Equilateral.B},
	})
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.B, P2: spec.Equilateral.C},
	})
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.C, P2: spec.Equilateral.A},
	})

	for i := 0; i < spec.Iterations; i++ {
		for _, node := range eqnode.Leaves(root) {
			vSeg := MagnifyTerdragon(node.Content)
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[0]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[1]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[2]})
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}

// GoldenVertices are the vertices of a Heighway golden dragon curve.
func GoldenVertices(spec TerdragonPlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: spec.Seg,
	})

	for i := 0; i < spec.Iterations; i++ {
		anti := true
		for _, node := range eqnode.Leaves(root) {
			vSeg := MagnifyGolden(node.Content, anti)
			anti = !anti
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[0]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[1]})
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}
