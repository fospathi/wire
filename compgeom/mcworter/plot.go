package mcworter

import (
	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// PentadentritePlotSpec specifies the properties of a pentadentrite curve.
type PentadentritePlotSpec struct {
	// Iterations of the algorithm.
	Iterations int
	// Seg is the line segment to which the iterative division process is
	// applied.
	//
	// Seg shall be parallel to the Z=0 XY-plane.
	Seg maf.LineSeg
}

// PentadentriteVertices are the vertices of a McWorter pentadentrite curve.
func PentadentriteVertices(spec PentadentritePlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: spec.Seg,
	})

	for i := 0; i < spec.Iterations; i++ {
		for _, node := range eqnode.Leaves(root) {
			vSeg := MagnifyPentadentrite(node.Content)
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[0]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[1]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[2]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[3]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[4]})
			node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: vSeg[5]})
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}
