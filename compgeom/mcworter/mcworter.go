package mcworter

import (
	"math"

	"gitlab.com/fospathi/maf"
)

var (
	// Pentadentrite.
	//
	// https://larryriddle.agnesscott.org/ifs/pentaden/geomtrig.htm
	ratPen      = math.Sqrt((6 - math.Sqrt(5)) / 31.0)
	rotPenAntiA = maf.NewZRot(math.Asin(
		((math.Sqrt(5) - 1) / 4.0) * math.Sqrt((25+math.Sqrt(5))/62.0),
	))
	rotPenAnti72   = maf.NewZRot(maf.DegToRad(72))
	rotPenClock72  = maf.NewZRot(-maf.DegToRad(72))
	rotPenClock144 = maf.NewZRot(-maf.DegToRad(144))
)

func MagnifyPentadentrite(seg maf.LineSeg) [6]maf.LineSeg {
	v1 := rotPenAntiA.Transform(seg.P1.To(seg.P2).Scale(ratPen))
	v2 := rotPenAnti72.Transform(v1)
	v3 := v1
	v4 := rotPenClock144.Transform(v3)
	v5 := rotPenAnti72.Transform(v4)
	p1 := seg.P1.Add(v1)
	p2 := p1.Add(v2)
	p3 := p2.Add(v3)
	p4 := p3.Add(v4)
	p5 := p4.Add(v5)
	return [6]maf.LineSeg{
		{P1: seg.P1, P2: p1},
		{P1: p1, P2: p2},
		{P1: p2, P2: p3},
		{P1: p3, P2: p4},
		{P1: p4, P2: p5},
		{P1: p5, P2: seg.P2},
	}
}
