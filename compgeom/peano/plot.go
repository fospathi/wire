package peano

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// CurveRegion of a Peano curve.
//
// Each curve region consists of a square divided into nine sub squares.
type CurveRegion struct {
	Centre maf.Vec     // The centre of the region.
	Ori    Orientation // The orientation of the region's basic motif.
	ThirdW float64     // Third of the length of the region.
}

// Vertices of the curve in the region in the order of the curve's path through
// the region.
func (reg CurveRegion) Vertices() [9]maf.Vec {
	l := reg.ThirdW
	switch reg.Ori {
	case P:
		return [subRegions]maf.Vec{
			reg.Centre.Add(maf.Vec{X: -l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: -l, Y: l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: 0}),
			reg.Centre.Add(maf.Vec{X: 0, Y: -l}),
			reg.Centre.Add(maf.Vec{X: l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: l, Y: l}),
		}
	case Q:
		return [subRegions]maf.Vec{
			reg.Centre.Add(maf.Vec{X: l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: l, Y: l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: 0}),
			reg.Centre.Add(maf.Vec{X: 0, Y: -l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: -l, Y: l}),
		}
	case R:
		return [subRegions]maf.Vec{
			reg.Centre.Add(maf.Vec{X: l, Y: l}),
			reg.Centre.Add(maf.Vec{X: l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: -l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: 0}),
			reg.Centre.Add(maf.Vec{X: 0, Y: l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: -l, Y: -l}),
		}
	default: // S.
		return [subRegions]maf.Vec{
			reg.Centre.Add(maf.Vec{X: -l, Y: l}),
			reg.Centre.Add(maf.Vec{X: -l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: -l, Y: -l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: -l}),
			reg.Centre.Add(maf.Vec{X: 0, Y: 0}),
			reg.Centre.Add(maf.Vec{X: 0, Y: l}),
			reg.Centre.Add(maf.Vec{X: l, Y: l}),
			reg.Centre.Add(maf.Vec{X: l, Y: 0}),
			reg.Centre.Add(maf.Vec{X: l, Y: -l}),
		}
	}
}

// SubRegions of the receiver region in order of the direction of the curve
// through the sub regions.
func (q CurveRegion) SubRegions() [subRegions]CurveRegion {
	res := [subRegions]CurveRegion{}
	for i, path := range Magnify(q.Ori) {
		var cen maf.Vec
		switch path.Region {
		case A:
			cen = q.Centre.Add(maf.Vec{X: -q.ThirdW, Y: -q.ThirdW})
		case B:
			cen = q.Centre.Add(maf.Vec{X: -q.ThirdW, Y: 0})
		case C:
			cen = q.Centre.Add(maf.Vec{X: -q.ThirdW, Y: q.ThirdW})
		case D:
			cen = q.Centre.Add(maf.Vec{X: 0, Y: q.ThirdW})
		case E:
			cen = q.Centre.Add(maf.Vec{X: 0, Y: 0})
		case F:
			cen = q.Centre.Add(maf.Vec{X: 0, Y: -q.ThirdW})
		case G:
			cen = q.Centre.Add(maf.Vec{X: q.ThirdW, Y: -q.ThirdW})
		case H:
			cen = q.Centre.Add(maf.Vec{X: q.ThirdW, Y: 0})
		default:
			cen = q.Centre.Add(maf.Vec{X: q.ThirdW, Y: q.ThirdW})
		}
		res[i] = CurveRegion{
			Centre: cen,
			Ori:    path.Orientation,
			ThirdW: q.ThirdW / 3,
		}
	}
	return res
}

// PlotSpec specifies the properties of a space-filling Peano curve.
type PlotSpec struct {
	// Centre of the 2D square region occupied by the Peano curve.
	//
	// The vertices of the curve will be in a plane parallel to the Z=0 plane.
	Centre maf.Vec
	// Order of the Peano curve.
	Order int
	// Width of the 2D square region occupied by the Peano curve.
	W float64
}

// Vertices of a 2D Peano curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[CurveRegion]{}
	root.AddChild(&dryad.NodeOfEq[CurveRegion]{
		Content: CurveRegion{
			Centre: spec.Centre,
			Ori:    P,
			ThirdW: spec.W / 3,
		},
	})

	for i := 1; i < spec.Order; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, reg := range node.Content.SubRegions() {
				node.AddChild(&dryad.NodeOfEq[CurveRegion]{Content: reg})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[CurveRegion]) {
		if len(node.Kids) == 0 {
			points := node.Content.Vertices()
			vp = append(vp, points[:]...)
		}
	})
	return vp
}
