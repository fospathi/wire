/*
Package peano produces the vertices of a space-filling Peano curve.
*/
package peano

// An orientation of the basic motif.
//
// For a detailed explanation of these orientations see (Bader 38).
//
//   - (Bader 38).
//     Bader, Michael. Space-Filling Curves, An Introduction with Applications
//     in Scientific Computing. Springer. 2013.
const (
	P = Orientation(iota) // XY -> XY.
	Q                     // XY -> XnY.
	R                     // XY -> XnYn.
	S                     // XY -> XYn.
)

// =          | - - - | - - - | - - - |
// =          |   C   |   D   |   I   |
// =          | - - - | - - - | - - - |
// =          |   B   |   E   |   H   |
// =          | - - - | - - - | - - - |
// =          |   A   |   F   |   G   |
// =          | - - - | - - - | - - - |
const (
	A = Region(iota)
	B
	C
	D
	E
	F
	G
	H
	I
)

const subRegions = 9

// Orientation of the basic motif.
type Orientation uint

// A Region that can be occupied by a suitably oriented basic motif.
type Region uint

// CurvePath through the sub regions.
//
// The curve path progresses through the indices in increasing order: 0, 1, 2,
// 3, 4, 5, 6, 7, 8.
type CurvePath [subRegions]struct {
	Region
	Orientation
}

// Magnify a section of the curve which consists of the basic motif with the
// given orientation.
func Magnify(ori Orientation) CurvePath {
	switch ori {
	case P:
		return CurvePath{
			{A, P}, {B, Q}, {C, P},
			{D, S}, {E, R}, {F, S},
			{G, P}, {H, Q}, {I, P},
		}
	case Q:
		return CurvePath{
			{G, Q}, {H, P}, {I, Q},
			{D, R}, {E, S}, {F, R},
			{A, Q}, {B, P}, {C, Q},
		}
	case R:
		return CurvePath{
			{I, R}, {H, S}, {G, R},
			{F, Q}, {E, P}, {D, Q},
			{C, R}, {B, S}, {A, R},
		}
	default: // S.
		return CurvePath{
			{C, S}, {B, R}, {A, S},
			{F, P}, {E, Q}, {D, P},
			{I, S}, {H, R}, {G, S},
		}
	}
}
