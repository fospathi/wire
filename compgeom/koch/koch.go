package koch

import (
	"math"

	"gitlab.com/fospathi/maf"
)

var (
	rotAnti  = maf.NewZRot(math.Pi / 3)
	rotClock = maf.NewZRot(-math.Pi / 3)
)

// Magnify a section of a Koch curve.
func Magnify(seg maf.LineSeg) [4]maf.LineSeg {
	sub := seg.P1.To(seg.P2).Scale(1.0 / 3.0)
	anti, clock := rotAnti.Transform(sub), rotClock.Transform(sub)
	a := seg.P1.Add(sub)
	b := a.Add(anti)
	c := b.Add(clock)
	return [4]maf.LineSeg{
		{P1: seg.P1, P2: a},
		{P1: a, P2: b},
		{P1: b, P2: c},
		{P1: c, P2: seg.P2},
	}
}

// MagnifyAntiSnowflake curve section.
func MagnifyAntiSnowflake(seg maf.LineSeg) [4]maf.LineSeg {
	sub := seg.P1.To(seg.P2).Scale(1.0 / 3.0)
	anti, clock := rotAnti.Transform(sub), rotClock.Transform(sub)
	a := seg.P1.Add(sub)
	b := a.Add(clock)
	c := b.Add(anti)
	return [4]maf.LineSeg{
		{P1: seg.P1, P2: a},
		{P1: a, P2: b},
		{P1: b, P2: c},
		{P1: c, P2: seg.P2},
	}
}
