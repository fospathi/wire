package koch

import (
	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// PlotSpec specifies the properties of a Koch curve.
type PlotSpec struct {
	// Order of the Koch curve.
	Order int
	// Seg is the line segment to which the iterative division process is
	// applied.
	Seg maf.LineSeg
}

// Vertices of a Koch curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: spec.Seg,
	})

	for i := 0; i < spec.Order; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, seg := range Magnify(node.Content) {
				node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: seg})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}

// SnowflakePlotSpec specifies the properties of a Koch snowflake curve.
type SnowflakePlotSpec struct {
	// AntiSnowflake is true to obtain a Koch anti-snowflake curve.
	AntiSnowflake bool
	// Order of the Koch snowflake curve.
	Order int
	// Equilateral is the equilateral triangle whose edges are the subject of
	// the iterative division process.
	Equilateral maf.Triangle
}

// SnowflakeVertices of a Koch snowflake curve.
func SnowflakeVertices(spec SnowflakePlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[maf.LineSeg]{}
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.A, P2: spec.Equilateral.B},
	})
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.B, P2: spec.Equilateral.C},
	})
	root.AddChild(&dryad.NodeOfEq[maf.LineSeg]{
		Content: maf.LineSeg{P1: spec.Equilateral.C, P2: spec.Equilateral.A},
	})

	magnify := Magnify
	if spec.AntiSnowflake {
		magnify = MagnifyAntiSnowflake
	}

	for i := 1; i < spec.Order; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, seg := range magnify(node.Content) {
				node.AddChild(&dryad.NodeOfEq[maf.LineSeg]{Content: seg})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[maf.LineSeg]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.P1, node.Content.P2)
		}
	})

	return slices.Compact(vp)
}
