/*
Package sierpinski produces the vertices of a space-filling Sierpinski curve.
*/
package sierpinski

// An orientation of the basic motif.
//
// For a detailed explanation of these orientations see (Bader 80).
//
//   - (Bader 80).
//     Bader, Michael. Space-Filling Curves, An Introduction with Applications
//     in Scientific Computing. Springer. 2013.
const (
	S = Orientation(iota) // XY -> XY.
	R                     // XY -> YXn.
	P                     // XY -> YnX.
	Z                     // XY -> XnYn.
)

// =               \  C  |  B  /
// =                 \   |   /
// =              D    \ | /    A
// =            - - - - -|- - - - -
// =              E    / | \    H
// =                 /   |   \
// =               /  F  |  G  \
const (
	A = Region(iota)
	B
	C
	D
	E
	F
	G
	H
)

// Orientation of the basic motif.
type Orientation uint

// A Region that can be occupied by a suitably oriented basic motif.
type Region uint

// CurvePath through the sub regions.
//
// The curve path progresses through the indices in increasing order: 0, 1, 2,
// 3.
type CurvePath [4]struct {
	Region
	Orientation
}

// Magnify a section of the curve which consists of the basic motif with the
// given orientation.
func Magnify(ori Orientation) CurvePath {
	switch ori {
	case S:
		return CurvePath{{D, S}, {C, R}, {B, P}, {A, S}}
	case R:
		return CurvePath{{F, R}, {E, Z}, {D, S}, {C, R}}
	case P:
		return CurvePath{{B, P}, {A, S}, {H, Z}, {G, P}}
	default: // Z.
		return CurvePath{{H, Z}, {G, P}, {F, R}, {E, Z}}
	}
}
