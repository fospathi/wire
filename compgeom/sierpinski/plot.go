package sierpinski

import (
	"math"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// CurveRegion of a Sierpinski curve.
//
// Each curve region consists of a right isosceles divided into four right
// isosceles.
type CurveRegion struct {
	MidHypot  maf.Vec     // The midpoint of the region's hypotenuse.
	HalfHypot float64     // Half the length of the hypotenuse of the region.
	Ori       Orientation // The orientation of the region's basic motif.
}

// Vertices of the curve in the region in the order of the curve's path through
// the region.
func (reg CurveRegion) Vertices() [4]maf.Vec {

	// =                                                     - - -
	// =                   /|\                                 |
	// =                 /  |  \                               |
	// =               /    |    \                             |
	// =             /      |      \                           | h
	// =           / \ * * *|* * * / \          - - -          |
	// =         /   * \    |    / *   \          | l2         |
	// =       /    *    \  |  /    *    \      - - -          |
	// =     /             \|/             \      | l2         |
	// =     - - - - - - - - - - - - - - - -    - - -        - - -
	// =            <- l1 ->
	// =
	// =
	// =      <----- h ---->
	// =
	h := reg.HalfHypot
	l1, l2 := h/2, h/4

	switch reg.Ori {
	case S:
		return [4]maf.Vec{
			reg.MidHypot.Add(maf.Vec{X: -l1, Y: l2}),
			reg.MidHypot.Add(maf.Vec{X: -l2, Y: l1}),
			reg.MidHypot.Add(maf.Vec{X: l2, Y: l1}),
			reg.MidHypot.Add(maf.Vec{X: l1, Y: l2}),
		}
	case R:
		return [4]maf.Vec{
			reg.MidHypot.Add(maf.Vec{X: -l2, Y: -l1}),
			reg.MidHypot.Add(maf.Vec{X: -l1, Y: -l2}),
			reg.MidHypot.Add(maf.Vec{X: -l1, Y: l2}),
			reg.MidHypot.Add(maf.Vec{X: -l2, Y: l1}),
		}
	case P:
		return [4]maf.Vec{
			reg.MidHypot.Add(maf.Vec{X: l2, Y: l1}),
			reg.MidHypot.Add(maf.Vec{X: l1, Y: l2}),
			reg.MidHypot.Add(maf.Vec{X: l1, Y: -l2}),
			reg.MidHypot.Add(maf.Vec{X: l2, Y: -l1}),
		}
	default: // Z.
		return [4]maf.Vec{
			reg.MidHypot.Add(maf.Vec{X: l1, Y: -l2}),
			reg.MidHypot.Add(maf.Vec{X: l2, Y: -l1}),
			reg.MidHypot.Add(maf.Vec{X: -l2, Y: -l1}),
			reg.MidHypot.Add(maf.Vec{X: -l1, Y: -l2}),
		}
	}
}

// SubRegions of the receiver region in order of the direction of the curve
// through the sub regions.
func (q CurveRegion) SubRegions() [4]CurveRegion {
	q.HalfHypot = q.HalfHypot / 2
	res := [4]CurveRegion{}
	for i, path := range Magnify(q.Ori) {
		var mid maf.Vec
		switch path.Region {
		case B:
			fallthrough
		case C:
			mid = q.MidHypot.Add(maf.Vec{X: 0, Y: q.HalfHypot})
		case D:
			fallthrough
		case E:
			mid = q.MidHypot.Add(maf.Vec{X: -q.HalfHypot, Y: 0})
		case F:
			fallthrough
		case G:
			mid = q.MidHypot.Add(maf.Vec{X: 0, Y: -q.HalfHypot})
		case A:
			fallthrough
		default:
			mid = q.MidHypot.Add(maf.Vec{X: q.HalfHypot, Y: 0})
		}
		res[i] = CurveRegion{
			MidHypot:  mid,
			HalfHypot: q.HalfHypot,
			Ori:       path.Orientation,
		}
	}
	return res
}

// PlotSpec specifies the properties of a Sierpinski curve.
type PlotSpec struct {
	// Centre of the 2D square region occupied by the Sierpinski curve.
	//
	// The vertices of the curve will be in a plane parallel to the Z=0 plane.
	Centre maf.Vec
	// Iterations of the magnification process applied to the basic motif.
	//
	// The number of iterations does not equal the order of the curve because
	// the algorithm skips every other order of the curve.
	Iterations int
	// Width of the 2D square region occupied by the Sierpinski curve.
	W float64
}

// Vertices of a 2D Sierpinski curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[CurveRegion]{}
	root.AddChild(&dryad.NodeOfEq[CurveRegion]{
		Content: CurveRegion{
			MidHypot:  spec.Centre,
			HalfHypot: math.Sqrt(spec.W*spec.W+spec.W*spec.W) / 2,
			Ori:       S,
		},
	})

	for i := 0; i < spec.Iterations; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, reg := range node.Content.SubRegions() {
				node.AddChild(&dryad.NodeOfEq[CurveRegion]{Content: reg})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[CurveRegion]) {
		if len(node.Kids) == 0 {
			points := node.Content.Vertices()
			vp = append(vp, points[:]...)
		}
	})
	return vp
}
