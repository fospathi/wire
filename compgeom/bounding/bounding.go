package bounding

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
)

// NewBox creates a new bounding Box around the given vertices.
func NewBox(vp []maf.Vec2) Box {
	if len(vp) == 0 {
		return Box{}
	}
	x, y := interval.In{vp[0].X, vp[0].X}, interval.In{vp[0].Y, vp[0].Y}
	for _, p := range vp {
		if p.X < x[0] {
			x[0] = p.X
		} else if p.X > x[1] {
			x[1] = p.X
		}
		if p.Y < y[0] {
			y[0] = p.Y
		} else if p.Y > y[1] {
			y[1] = p.Y
		}
	}
	return Box{X: x, Y: y}
}

// Box with axis aligned edges.
type Box struct {
	X, Y interval.In
}

// Centre of the box.
func (b Box) Centre() maf.Vec2 {
	return maf.Vec2{X: b.X.Midpoint(), Y: b.Y.Midpoint()}
}

// Corners of the box.
func (b Box) Corners() Corners {
	return Corners{
		TopRight:    maf.Vec2{X: b.X[1], Y: b.Y[1]},
		TopLeft:     maf.Vec2{X: b.X[0], Y: b.Y[1]},
		BottomLeft:  maf.Vec2{X: b.X[0], Y: b.Y[0]},
		BottomRight: maf.Vec2{X: b.X[1], Y: b.Y[0]},
	}
}

// Expand the width and height of the box by the given length.
//
// The expansion occurs symmetrically about its centre.
func (b Box) Expand(l float64) Box {
	return Box{
		X: interval.In{b.X[0] - l/2, b.X[1] + l/2},
		Y: interval.In{b.Y[0] - l/2, b.Y[1] + l/2},
	}
}

// Corners of a box.
type Corners struct {
	TopRight, TopLeft, BottomLeft, BottomRight maf.Vec2
}

// NewBoxWithZ creates a new bounding Box around the given vertices which shall
// be in a plane parallel to the Z=0 plane.
func NewBoxWithZ(vp []maf.Vec) BoxWithZ {
	if len(vp) == 0 {
		return BoxWithZ{}
	}
	x, y := interval.In{vp[0].X, vp[0].X}, interval.In{vp[0].Y, vp[0].Y}
	for _, p := range vp {
		if p.X < x[0] {
			x[0] = p.X
		} else if p.X > x[1] {
			x[1] = p.X
		}
		if p.Y < y[0] {
			y[0] = p.Y
		} else if p.Y > y[1] {
			y[1] = p.Y
		}
	}
	return BoxWithZ{X: x, Y: y, Z: vp[0].Z}
}

// BoxWithZ is a box with axis aligned edges that contains points which have a
// constant z value.
type BoxWithZ struct {
	X, Y interval.In
	Z    float64
}

// Corners of the box in anticlockwise order around the centre starting with the
// top right corner.
func (b BoxWithZ) Corners() [4]maf.Vec {
	return [4]maf.Vec{
		{X: b.X[1], Y: b.Y[1], Z: b.Z},
		{X: b.X[0], Y: b.Y[1], Z: b.Z},
		{X: b.X[0], Y: b.Y[0], Z: b.Z},
		{X: b.X[1], Y: b.Y[0], Z: b.Z},
	}
}

// Expand the box in both axis directions by the given positive length.
func (b BoxWithZ) Expand(l float64) BoxWithZ {
	return BoxWithZ{
		X: interval.In{b.X[0] - l/2, b.X[1] + l/2},
		Y: interval.In{b.Y[0] - l/2, b.Y[1] + l/2},
		Z: b.Z,
	}
}
