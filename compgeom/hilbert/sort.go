package hilbert

import "gitlab.com/fospathi/maf"

// Compare the given points using the Hilbert less function.
func Compare(p1, p2 maf.Vec2) int {
	switch {
	case p1 == p2:
		return 0
	case Less(p1, p2):
		return -1
	default:
		return 1
	}
}

// Less reports whether the first of the two given 2D points occurs before the
// second along a space filling Hilbert curve which is occupying the region.
//
// Point coordinates shall be constrained to the range 0 to 1.
//
// By sorting along a Hilbert curve, points which are close together in 2D space
// are still close together when sorted.
func Less(p1, p2 maf.Vec2) bool {
	if p1 == p2 {
		return false
	}

	var (
		quadW  = 0.5 // Square of length 1 split into 4 => 0.5 per edge.
		path   = InitialPath
		x0, y0 = 0.0, 0.0

		left, bottom bool
		q1, q2       Quadrant
	)
	for {
		left, bottom = p1.X <= x0+quadW, p1.Y <= y0+quadW
		switch {
		case left && bottom:
			q1 = C
		case left && !bottom:
			q1 = B
		case !left && !bottom:
			q1 = A
		default:
			q1 = D
		}

		left, bottom = p2.X <= x0+quadW, p2.Y <= y0+quadW
		switch {
		case left && bottom:
			q2 = C
		case left && !bottom:
			q2, y0 = B, y0+quadW
		case !left && !bottom:
			q2, x0, y0 = A, x0+quadW, y0+quadW
		default:
			q2, x0 = D, x0+quadW
		}

		if q1 == q2 {
			quadW /= 2
			switch q1 {
			case path[0].Quadrant:
				path = Magnify(path[0].Orientation)
			case path[1].Quadrant:
				path = Magnify(path[1].Orientation)
			case path[2].Quadrant:
				path = Magnify(path[2].Orientation)
			case path[3].Quadrant:
				path = Magnify(path[3].Orientation)
			}
			continue
		}

		// At least one of the points is somewhere in the first three quadrants
		// so its safe to stop checking which point comes first at index 2.
		switch {
		case path[0].Quadrant == q1:
			return true
		case path[0].Quadrant == q2:
			return false

		case path[1].Quadrant == q1:
			return true
		case path[1].Quadrant == q2:
			return false

		case path[2].Quadrant == q1:
			return true
		case path[2].Quadrant == q2:
			return false
		}
	}
}
