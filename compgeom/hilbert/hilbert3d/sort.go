package hilbert3d

import "gitlab.com/fospathi/maf"

// Compare the given points using the Hilbert less function.
func Compare(p1, p2 maf.Vec) int {
	switch {
	case p1 == p2:
		return 0
	case Less(p1, p2):
		return -1
	default:
		return 1
	}
}

// Less reports whether the first of the two given 3D points occurs before the
// second along a space filling Hilbert curve which is occupying the volume.
//
// Point coordinates shall be constrained to the range 0 to 1.
//
// By sorting along a Hilbert curve, points which are close together in 3D are
// still close together when sorted.
func Less(p1, p2 maf.Vec) bool {
	if p1 == p2 {
		return false
	}

	var (
		octW       = 0.5 // Cube of length 1 is split into 8 => 0.5 per edge.
		path       = InitialPath
		x0, y0, z0 = 0.0, 0.0, 0.0

		left, bottom, rear bool
		right, top, front  bool
		o1, o2             Octant
	)

	for {
		left, bottom, rear = p1.X <= x0+octW, p1.Y <= y0+octW, p1.Z <= z0+octW
		right, top, front = !left, !bottom, !rear
		switch {
		case right && top && front:
			o1 = A
		case left && top && front:
			o1 = B
		case left && bottom && front:
			o1 = C
		case right && bottom && front:
			o1 = D
		case right && top && rear:
			o1 = E
		case left && top && rear:
			o1 = F
		case left && bottom && rear:
			o1 = G
		default:
			o1 = H
		}

		left, bottom, rear = p2.X <= x0+octW, p2.Y <= y0+octW, p2.Z <= z0+octW
		right, top, front = !left, !bottom, !rear
		switch {
		case right && top && front:
			o2, x0, y0, z0 = A, x0+octW, y0+octW, z0+octW
		case left && top && front:
			o2, y0, z0 = B, y0+octW, z0+octW
		case left && bottom && front:
			o2, z0 = C, z0+octW
		case right && bottom && front:
			o2, x0, z0 = D, x0+octW, z0+octW
		case right && top && rear:
			o2, x0, y0 = E, x0+octW, y0+octW
		case left && top && rear:
			o2, y0 = F, y0+octW
		case left && bottom && rear:
			o2 = G
		default:
			o2, x0 = H, x0+octW
		}

		if o1 == o2 {
			octW /= 2
			switch o1 {
			case path[0].Octant:
				path = Magnify(path[0].Orientation)
			case path[1].Octant:
				path = Magnify(path[1].Orientation)
			case path[2].Octant:
				path = Magnify(path[2].Orientation)
			case path[3].Octant:
				path = Magnify(path[3].Orientation)
			case path[4].Octant:
				path = Magnify(path[4].Orientation)
			case path[5].Octant:
				path = Magnify(path[5].Orientation)
			case path[6].Octant:
				path = Magnify(path[6].Orientation)
			case path[7].Octant:
				path = Magnify(path[7].Orientation)
			}
			continue
		}

		// At least one of the points is somewhere in the first seven octants so
		// its safe to stop checking which point comes first at index 6.
		switch {
		case path[0].Octant == o1:
			return true
		case path[0].Octant == o2:
			return false

		case path[1].Octant == o1:
			return true
		case path[1].Octant == o2:
			return false

		case path[2].Octant == o1:
			return true
		case path[2].Octant == o2:
			return false

		case path[3].Octant == o1:
			return true
		case path[3].Octant == o2:
			return false

		case path[4].Octant == o1:
			return true
		case path[4].Octant == o2:
			return false

		case path[5].Octant == o1:
			return true
		case path[5].Octant == o2:
			return false

		case path[6].Octant == o1:
			return true
		case path[6].Octant == o2:
			return false
		}
	}
}
