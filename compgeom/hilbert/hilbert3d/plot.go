package hilbert3d

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// CurveOctant is one of the octants of a pseudo Hilbert curve.
type CurveOctant struct {
	Centre maf.Vec     // The center of the octant.
	HalfW  float64     // Half the width of the octant.
	Ori    Orientation // The orientation of the octant's basic motif.
}

// SubOctants of the receiver octant in order of the direction of the curve
// through the sub octants.
func (q CurveOctant) SubOctants() [8]CurveOctant {
	q.HalfW = q.HalfW / 2
	res := [8]CurveOctant{}
	for i, path := range Magnify(q.Ori) {
		var cen maf.Vec
		switch path.Octant {
		case A:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: q.HalfW, Z: q.HalfW})
		case B:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: q.HalfW, Z: q.HalfW})
		case C:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: -q.HalfW, Z: q.HalfW})
		case D:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: -q.HalfW, Z: q.HalfW})
		case E:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: q.HalfW, Z: -q.HalfW})
		case F:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: q.HalfW, Z: -q.HalfW})
		case G:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: -q.HalfW, Z: -q.HalfW})
		default:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: -q.HalfW, Z: -q.HalfW})
		}
		res[i] = CurveOctant{
			Centre: cen,
			HalfW:  q.HalfW,
			Ori:    path.Orientation,
		}
	}
	return res
}

// PlotSpec specifies the properties of a pseudo Hilbert curve.
type PlotSpec struct {
	// Centre of the 3D axis-aligned cube region occupied by the Hilbert curve.
	Centre maf.Vec
	// Order of the Hilbert curve.
	Order int
	// Width of the 3D cube region occupied by the Hilbert curve.
	W float64
}

// Vertices of a 3D pseudo Hilbert curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[CurveOctant]{}
	root.AddChild(&dryad.NodeOfEq[CurveOctant]{
		Content: CurveOctant{
			Centre: spec.Centre,
			HalfW:  spec.W / 2,
			Ori:    XYZ,
		},
	})

	for i := 0; i < spec.Order; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, q := range node.Content.SubOctants() {
				node.AddChild(&dryad.NodeOfEq[CurveOctant]{Content: q})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[CurveOctant]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.Centre)
		}
	})
	return vp
}
