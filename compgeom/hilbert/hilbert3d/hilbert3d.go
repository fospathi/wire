/*
Package hilbert facilitates sorting 3D points along a single space-filling
Hilbert curve.
*/
package hilbert3d

// An orientation specifies the mapping of the basic motif's positive X, Y, and
// Z-axis unit vectors respectively.
//
// A small case 'n' suffix after a capital X, Y, or Z indicates a negative axis.
//
// For a detailed explanation of these orientations see (Bader 115).
//
//   - (Bader 115).
//     Bader, Michael. Space-Filling Curves, An Introduction with Applications
//     in Scientific Computing. Springer. 2013.
const (
	XYZ = Orientation(iota) // Standard orientation.
	XYnZn

	YZX
	YZnXn

	ZXY
	ZXnYn

	XnYZn
	XnYnZ

	YnZXn
	YnZnX

	ZnXYn
	ZnXnY
)

// As the Hilbert curve passes through a cube of eight sub cubes (octants), the
// curve will visit each octant once. The order in which the octants are visited
// depends on the orientation of the basic motif.
//
// Octant diagrams:
//
//	=       Positive Z                         Negative Z
//	=
//	=            Y                                 Y
//	=            |                                 |
//	=        B   |   A                         F   |   E
//	=      - - - | - - - X                   - - - | - - - X
//	=        C   |   D                         G   |   H
//	=            |                                 |
const (
	A = Octant(iota)
	B
	C
	D
	E
	F
	G
	H
)

// InitialPath is a Hilbert curve of order 2.
var InitialPath = Magnify(XYZ)

// Orientation of the basic motif.
//
// The basic motif is two upside down rectangular 'U' shapes joined together by
// a line parallel to the Z axis and it starts at the Z=0 side.
type Orientation uint

// An Octant is one the eight equal sized parts that a cube is divided into when
// increasing the order of the curve by one.
type Octant uint

// CurvePath through a cube which is split into eight equal sized sub cubes.
//
// The curve path progresses through the indices in increasing order: 0, 1, 2,
// 3, 4, 5, 6, 7.
type CurvePath [8]struct {
	Octant
	Orientation
}

// Magnify a cubic section of the curve which consists of the basic motif with
// the given orientation.
func Magnify(ori Orientation) CurvePath {
	switch ori {
	case XYZ:
		return CurvePath{
			{C, YZX},
			{G, ZXY},
			{H, ZXY},
			{D, XnYnZ},
			{A, XnYnZ},
			{E, ZnXYn},
			{F, ZnXYn},
			{B, YZnXn},
		}
	case XYnZn:
		return CurvePath{
			{F, YZnXn},
			{B, ZXnYn},
			{A, ZXnYn},
			{E, XnYZn},
			{H, XnYZn},
			{D, ZnXnY},
			{C, ZnXnY},
			{G, YZX},
		}
	case YZX:
		return CurvePath{
			{C, ZXY},
			{D, XYZ},
			{A, XYZ},
			{B, YnZXn},
			{F, YnZXn},
			{E, XYnZn},
			{H, XYnZn},
			{G, ZnXnY},
		}
	case YZnXn:
		return CurvePath{
			{F, ZXnYn},
			{E, XYnZn},
			{H, XYnZn},
			{G, YnZnX},
			{C, YnZnX},
			{D, XYZ},
			{A, XYZ},
			{B, ZnXYn},
		}
	case ZXY:
		return CurvePath{
			{C, XYZ},
			{B, YZX},
			{F, YZX},
			{G, ZXnYn},
			{H, ZXnYn},
			{E, YnZnX},
			{A, YnZnX},
			{D, XnYZn},
		}
	case ZXnYn:
		return CurvePath{
			{F, XYnZn},
			{G, YZnXn},
			{C, YZnXn},
			{B, ZXY},
			{A, ZXY},
			{D, YnZXn},
			{H, YnZXn},
			{E, XnYnZ},
		}
	case XnYZn:
		return CurvePath{
			{A, YnZXn},
			{E, ZnXYn},
			{F, ZnXYn},
			{B, XYnZn},
			{C, XYnZn},
			{G, ZXY},
			{H, ZXY},
			{D, YnZnX},
		}
	case XnYnZ:
		return CurvePath{
			{H, YnZnX},
			{D, ZnXnY},
			{C, ZnXnY},
			{G, XYZ},
			{F, XYZ},
			{B, ZXnYn},
			{A, ZXnYn},
			{E, YnZXn},
		}
	case YnZXn:
		return CurvePath{
			{A, ZnXYn},
			{B, XnYZn},
			{C, XnYZn},
			{D, YZX},
			{H, YZX},
			{G, XnYnZ},
			{F, XnYnZ},
			{E, ZXnYn},
		}
	case YnZnX:
		return CurvePath{
			{H, ZnXnY},
			{G, XnYnZ},
			{F, XnYnZ},
			{E, YZnXn},
			{A, YZnXn},
			{B, XnYZn},
			{C, XnYZn},
			{D, ZXY},
		}
	case ZnXYn:
		return CurvePath{
			{A, XnYZn},
			{D, YnZXn},
			{H, YnZXn},
			{E, ZnXnY},
			{F, ZnXnY},
			{G, YZnXn},
			{C, YZnXn},
			{B, XYZ},
		}
	default: // ZnXnY
		return CurvePath{
			{H, XnYnZ},
			{E, YnZnX},
			{A, YnZnX},
			{D, ZnXYn},
			{C, ZnXYn},
			{B, YZX},
			{F, YZX},
			{G, XYnZn},
		}
	}
}
