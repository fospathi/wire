/*
Package hilbert facilitates sorting 2D points along a single space-filling
Hilbert curve.
*/
package hilbert

// An orientation specifies the mapping of the basic motif's positive X and
// Y-axis unit vectors respectively.
//
// A small case 'n' suffix after a capital X or Y indicates a negative axis.
const (
	XY   = Orientation(iota) // Standard orientation.
	YnXn                     // Reflection of XY in the line y=-x.
	XnYn                     // Rotation of XY by 180 deg.
	YX                       // Reflection of XY in the line y=x.
)

const (
	A = Quadrant(iota) // Top right.
	B                  // Top left.
	C                  // Bottom left.
	D                  // Bottom Right.
)

// InitialPath is a Hilbert curve of order 2.
var InitialPath = Magnify(XY)

// Orientation of the basic motif.
//
// The basic motif is an upside down rectangular 'U' shape and it goes from left
// to right.
type Orientation uint

// A Quadrant is one of the four equal sized parts that a square is divided into
// when increasing the order of the curve by one.
type Quadrant uint

// CurvePath through a square which is split into four equal sized sub squares.
//
// The curve path progresses through the indices in increasing order: 0, 1, 2,
// 3.
type CurvePath [4]struct {
	Quadrant
	Orientation
}

// Magnify a square section of the curve which consists of the basic motif with
// the given orientation.
func Magnify(ori Orientation) CurvePath {
	switch ori {
	case XY:
		return CurvePath{{C, YX}, {B, XY}, {A, XY}, {D, YnXn}}
	case YnXn:
		return CurvePath{{A, XnYn}, {B, YnXn}, {C, YnXn}, {D, XY}}
	case XnYn:
		return CurvePath{{A, YnXn}, {D, XnYn}, {C, XnYn}, {B, YX}}
	default:
		return CurvePath{{C, XY}, {D, YX}, {A, YX}, {B, XnYn}}
	}
}
