package hilbert_test

import (
	"math/rand"
	"slices"
	"testing"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/compgeom/hilbert"
)

func BenchmarkLess(b *testing.B) {
	rdm := rand.New(rand.NewSource(time.Now().UnixNano()))
	const n = 400
	const m = 50000
	points := make([][]maf.Vec2, m)
	for j := 0; j < m; j++ {
		vp := make([]maf.Vec2, n)
		for i := 0; i < n; i++ {
			vp[i] = maf.Vec2{X: rdm.Float64(), Y: rdm.Float64()}
		}
		points[j] = vp
	}

	var i int
	for n := 0; n < b.N; n++ {
		i = (i + 1) % m
		slices.SortFunc(points[i], hilbert.Compare)
	}
}

func TestVertices(t *testing.T) {
	vp := hilbert.Vertices(hilbert.PlotSpec{
		Centre: maf.Vec{X: 0.5, Y: 0.5},
		Order:  1,
		W:      1.0,
	})

	want := []maf.Vec{
		{X: 0.25, Y: 0.25},
		{X: 0.25, Y: 0.75},
		{X: 0.75, Y: 0.75},
		{X: 0.75, Y: 0.25},
	}
	got := vp
	if !slices.Equal(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	vp = hilbert.Vertices(hilbert.PlotSpec{
		Centre: maf.Vec{X: 0, Y: 0},
		Order:  1,
		W:      1.0,
	})

	want = []maf.Vec{
		{X: -0.25, Y: -0.25},
		{X: -0.25, Y: 0.25},
		{X: 0.25, Y: 0.25},
		{X: 0.25, Y: -0.25},
	}
	got = vp
	if !slices.Equal(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	vp = hilbert.Vertices(hilbert.PlotSpec{
		Centre: maf.Vec{X: 4, Y: 4},
		Order:  2,
		W:      8.0,
	})

	want = []maf.Vec{
		{X: 1, Y: 1},
		{X: 3, Y: 1},
		{X: 3, Y: 3},
		{X: 1, Y: 3},

		{X: 1, Y: 5},
		{X: 1, Y: 7},
		{X: 3, Y: 7},
		{X: 3, Y: 5},

		{X: 5, Y: 5},
		{X: 5, Y: 7},
		{X: 7, Y: 7},
		{X: 7, Y: 5},

		{X: 7, Y: 3},
		{X: 5, Y: 3},
		{X: 5, Y: 1},
		{X: 7, Y: 1},
	}
	got = vp
	if !slices.Equal(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
