package hilbert

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqnode"
	"gitlab.com/fospathi/maf"
)

// CurveQuadrant is one of the quadrants of a pseudo Hilbert curve.
type CurveQuadrant struct {
	Centre maf.Vec     // The center of the quadrant.
	HalfW  float64     // Half the width of the quadrant.
	Ori    Orientation // The orientation of the quadrant's basic motif.
}

// SubQuadrants of the receiver quadrant in order of the direction of the curve
// through the sub quadrants.
func (q CurveQuadrant) SubQuadrants() [4]CurveQuadrant {
	q.HalfW = q.HalfW / 2
	res := [4]CurveQuadrant{}
	for i, path := range Magnify(q.Ori) {
		var cen maf.Vec
		switch path.Quadrant {
		case A:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: q.HalfW})
		case B:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: q.HalfW})
		case C:
			cen = q.Centre.Add(maf.Vec{X: -q.HalfW, Y: -q.HalfW})
		default:
			cen = q.Centre.Add(maf.Vec{X: q.HalfW, Y: -q.HalfW})
		}
		res[i] = CurveQuadrant{
			Centre: cen,
			HalfW:  q.HalfW,
			Ori:    path.Orientation,
		}
	}
	return res
}

// PlotSpec specifies the properties of a pseudo Hilbert curve.
type PlotSpec struct {
	// Centre of the 2D square region occupied by the Hilbert curve.
	//
	// The vertices of the curve will be in a plane parallel to the Z=0 plane.
	Centre maf.Vec
	// Order of the Hilbert curve.
	Order int
	// Width of the 2D square region occupied by the Hilbert curve.
	W float64
}

// Vertices of a 2D pseudo Hilbert curve.
func Vertices(spec PlotSpec) []maf.Vec {
	root := &dryad.NodeOfEq[CurveQuadrant]{}
	root.AddChild(&dryad.NodeOfEq[CurveQuadrant]{
		Content: CurveQuadrant{
			Centre: spec.Centre,
			HalfW:  spec.W / 2,
			Ori:    XY,
		},
	})

	for i := 0; i < spec.Order; i++ {
		for _, node := range eqnode.Leaves(root) {
			for _, q := range node.Content.SubQuadrants() {
				node.AddChild(&dryad.NodeOfEq[CurveQuadrant]{Content: q})
			}
		}
	}

	var vp []maf.Vec
	eqnode.Traverse(root, func(node *dryad.NodeOfEq[CurveQuadrant]) {
		if len(node.Kids) == 0 {
			vp = append(vp, node.Content.Centre)
		}
	})
	return vp
}
