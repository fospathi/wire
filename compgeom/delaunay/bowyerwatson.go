package delaunay

import (
	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/compgeom/bounding"
)

// NewTriangulation constructs a new Delaunay Triangulation of the given point
// set which shall contain at least three points.
func NewTriangulation(points dryad.Set[maf.Vec2]) Triangulation {
	// Construct a new Delaunay triangulation for the set of discrete points
	// using a basic implementation of the Bowyer-Watson algorithm.

	res := Triangulation{
		Triangles: dryad.Set[maf.Triangle2]{},
		Vertices:  dryad.Set[maf.Vec2]{},

		EdgeUsers:   map[maf.LineSeg2]dryad.Set[maf.Triangle2]{},
		VertexUsers: map[maf.Vec2]dryad.Set[maf.Triangle2]{},
	}
	if points.Len() < 3 {
		return res
	}

	super := superTriangle(points.Elements())
	res.add(super)

	for p := range points {
		badTriangles := newTriangleSet()
		badTriangles.add(res.circlers(p))
		res.remove(badTriangles.triangles)
		for e := range badTriangles.singleUseEdges() {
			res.add(maf.Triangle2{A: p, B: e.P1, C: e.P2}.Canonical())
		}
	}

	defunct := dryad.Set[maf.Triangle2]{}
	for tri := range res.Triangles {
		if super.SharesAnyVertexWith(tri) {
			defunct.Add(tri)
		}
	}
	res.remove(defunct)

	return res
}

// Triangulation is a Delaunay triangulation of a set of points.
type Triangulation struct {
	Triangles dryad.Set[maf.Triangle2]
	Vertices  dryad.Set[maf.Vec2]

	// Triangles which use the edge.
	EdgeUsers map[maf.LineSeg2]dryad.Set[maf.Triangle2]
	// Triangles which use the vertex.
	VertexUsers map[maf.Vec2]dryad.Set[maf.Triangle2]
}

// Add the given triangles which should be given in canonical form.
func (tgn Triangulation) add(vt ...maf.Triangle2) {
	for _, tri := range vt {

		tgn.Triangles.Add(tri)
		for _, e := range tri.Edges() {
			e = e.Canonical()
			eUsers, ok := tgn.EdgeUsers[e]
			if !ok {
				eUsers = dryad.Set[maf.Triangle2]{}
				tgn.EdgeUsers[e] = eUsers
			}
			eUsers.Add(tri)
		}

		tgn.Vertices.AddAll(tri.A, tri.B, tri.C)
		for _, p := range tri.Vertices() {
			vUsers, ok := tgn.VertexUsers[p]
			if !ok {
				vUsers = dryad.Set[maf.Triangle2]{}
				tgn.VertexUsers[p] = vUsers
			}
			vUsers.Add(tri)
		}

	}
}

// Circlers are the triangles whose circumcircle contains the point.
func (tgn Triangulation) circlers(p maf.Vec2) []maf.Triangle2 {
	var vt []maf.Triangle2
	for tri := range tgn.Triangles {
		if tri.CircumcircleContains(p) {
			vt = append(vt, tri)
		}
	}
	return vt
}

// Remove the given triangles which should be given in canonical form and have
// been previously added.
func (tgn Triangulation) remove(triSet dryad.Set[maf.Triangle2]) {
	for tri := range triSet {

		tgn.Triangles.Delete(tri)

		for _, e := range tri.Edges() {
			e = e.Canonical()
			eUsers, ok := tgn.EdgeUsers[e]
			if !ok {
				continue
			}
			eUsers.Delete(tri)
			if eUsers.IsEmpty() {
				delete(tgn.EdgeUsers, e)
			}
		}

		for _, p := range tri.Vertices() {
			vUsers, ok := tgn.VertexUsers[p]
			if !ok {
				continue
			}
			vUsers.Delete(tri)
			if vUsers.IsEmpty() {
				delete(tgn.VertexUsers, p)
				tgn.Vertices.Delete(p)
			}
		}
	}
}

func newTriangleSet() triangleSet {
	return triangleSet{
		edgeUsers: map[maf.LineSeg2]dryad.Set[maf.Triangle2]{},
		triangles: dryad.Set[maf.Triangle2]{},
	}
}

type triangleSet struct {
	edgeUsers map[maf.LineSeg2]dryad.Set[maf.Triangle2] // Triangles which use the edge.
	triangles dryad.Set[maf.Triangle2]
}

// Add the given triangles which should be given in canonical form.
func (ts triangleSet) add(vt []maf.Triangle2) {
	for _, tri := range vt {
		ts.triangles.Add(tri)
		for _, e := range tri.Edges() {
			e = e.Canonical()
			eUsers, ok := ts.edgeUsers[e]
			if !ok {
				eUsers = dryad.Set[maf.Triangle2]{}
				ts.edgeUsers[e] = eUsers
			}
			eUsers.Add(tri)
		}
	}
}

func (ts triangleSet) singleUseEdges() dryad.Set[maf.LineSeg2] {
	es := dryad.Set[maf.LineSeg2]{}
	for e, edgeUsers := range ts.edgeUsers {
		if edgeUsers.Len() == 1 {
			es.Add(e)
		}
	}
	return es
}

// SuperTriangle is a triangle in canonical form that contains the points.
func superTriangle(vp []maf.Vec2) maf.Triangle2 {
	box := bounding.NewBox(vp)
	cen := box.Centre()
	cor := box.Corners()

	rightLine := maf.Line2{
		P:   cen.Add(cen.To(cor.TopRight).Scale(2)),
		Dir: maf.Vec2{X: -1, Y: 1}.Unit(),
	}
	leftLine := maf.Line2{
		P:   cen.Add(cen.To(cor.TopLeft).Scale(2)),
		Dir: maf.Vec2{X: 1, Y: 1}.Unit(),
	}
	botLine := maf.Line2{
		P:   cen.Add(cen.To(cor.BottomLeft.MidTo(cor.BottomRight)).Scale(2)),
		Dir: maf.Vec2{X: 1, Y: 0},
	}

	return maf.Triangle2{
		A: rightLine.ClosestPoint(leftLine),
		B: leftLine.ClosestPoint(botLine),
		C: botLine.ClosestPoint(rightLine),
	}.Canonical()
}
