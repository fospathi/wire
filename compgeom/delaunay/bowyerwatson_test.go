package delaunay_test

import (
	"testing"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/compgeom/delaunay"
)

func TestNewTriangulation(t *testing.T) {
	vp := []maf.Vec2{{X: 0, Y: 0}, {X: 2, Y: 0}, {X: 1, Y: 1}}
	sp := dryad.Set[maf.Vec2]{}
	sp.AddAll(vp...)
	tgn := delaunay.NewTriangulation(sp)

	// Test the triangulation consists of a single triangle.

	{
		want, got := true, tgn.Vertices.Len() == 3 &&
			tgn.Vertices.ContainsAll(vp...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true, tgn.Triangles.Len() == 1 &&
			tgn.Triangles.Any().CoincidesWith(
				maf.Triangle2{A: vp[0], B: vp[1], C: vp[2]},
			)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true,
			len(tgn.EdgeUsers) == 3 &&
				eqset.FromKeys(tgn.EdgeUsers).ContainsAll([]maf.LineSeg2{
					maf.LineSeg2{P1: vp[0], P2: vp[1]}.Canonical(),
					maf.LineSeg2{P1: vp[1], P2: vp[2]}.Canonical(),
					maf.LineSeg2{P1: vp[2], P2: vp[0]}.Canonical(),
				}...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true, eqset.FromKeys(tgn.VertexUsers).ContainsAll(vp...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a two triangle triangulation.

	var (
		p1 = maf.Vec2{X: 0, Y: 0}
		p2 = maf.Vec2{X: 5, Y: 1}
		p3 = maf.Vec2{X: 5.1, Y: 0}
		p4 = maf.Vec2{X: 5, Y: -1}
	)

	vp = []maf.Vec2{p1, p2, p3, p4}
	sp = dryad.Set[maf.Vec2]{}
	sp.AddAll(vp...)
	tgn = delaunay.NewTriangulation(sp)

	expectedTriangles := dryad.Set[maf.Triangle2]{}
	expectedTriangles.AddAll([]maf.Triangle2{
		maf.Triangle2{A: p1, B: p2, C: p3}.Canonical(),
		maf.Triangle2{A: p1, B: p4, C: p3}.Canonical(),
	}...)

	{
		want, got := true, tgn.Vertices.Len() == 4 &&
			tgn.Vertices.ContainsAll(vp...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true, tgn.Triangles.Len() == 2 &&
			tgn.Triangles.Equals(expectedTriangles)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true,
			len(tgn.EdgeUsers) == 5 &&
				eqset.FromKeys(tgn.EdgeUsers).ContainsAll([]maf.LineSeg2{
					maf.LineSeg2{P1: p1, P2: p2}.Canonical(),
					maf.LineSeg2{P1: p1, P2: p3}.Canonical(),
					maf.LineSeg2{P1: p2, P2: p3}.Canonical(),
					maf.LineSeg2{P1: p4, P2: p3}.Canonical(),
					maf.LineSeg2{P1: p1, P2: p4}.Canonical(),
				}...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := true, eqset.FromKeys(tgn.VertexUsers).ContainsAll(vp...)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
