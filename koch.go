package wire

import (
	"gitlab.com/fospathi/wire/compgeom/koch"
)

// NewKoch constructs a new Koch curve which is initially oriented with a
// standard frame.
func NewKoch(spec koch.PlotSpec) Koch {
	return Koch{NewPolyLine(koch.Vertices(spec))}
}

// A Koch is a wireframe figure that depicts a Koch curve.
type Koch struct {
	*PolyLine
}

// NewKochSnowflake constructs a new KochSnowflake curve which is initially
// oriented with a standard frame.
func NewKochSnowflake(spec koch.SnowflakePlotSpec) KochSnowflake {
	return KochSnowflake{NewPolyLine(koch.SnowflakeVertices(spec))}
}

// A KochSnowflake is a wireframe figure that depicts a Koch snowflake curve.
type KochSnowflake struct {
	*PolyLine
}
