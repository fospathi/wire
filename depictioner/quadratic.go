package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// DepictWholeQuadraticBezier depicts a whole quadratic Bezier curve with a
// single quadratic Bezier.
//
// The portion interval limits shall be 0 and 1 in either direction.
func DepictWholeQuadraticBezier(
	p mech.Portion,
	// The curve is defined in the XY plane of this frame.
	fr maf.Frame,
	bez maf.Bez,
) mech.Depiction {

	pic := mech.Depiction{}
	toGlobal := fr.Out().Transform

	// Current number of vertices added, i.e. the abstract current write
	// position into the vertex attributes array.
	var n uint32

	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	a, b, c := bez.AtT(p.In[0]), bez.B, bez.AtT(p.In[1])
	a, b, c = toGlobal(a), toGlobal(b), toGlobal(c)
	pic, n = AppendInitial(pic, n, a, p.Colour(0), p.Thickness32(0))
	pic, n = AppendQuadratic(pic, n, b, c, p.Colour(1), p.Thickness32(1))

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}
