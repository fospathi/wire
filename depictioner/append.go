package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

var (
	// Use to extend the vertex attributes array len by one vertex.
	extendBy1 = make([]float32, mech.Float32sNonEye)
	// Use to extend the vertex attributes array len by two vertices.
	extendBy2 = make([]float32, 2*mech.Float32sNonEye)
)

// AppendInitial appends a portion's unshared first vertex to the depiction.
//
// Returns the possibly reallocated depiction and the new abstract write
// position into the vertex attributes array.
func AppendInitial(
	pic mech.Depiction,
	n uint32,
	global maf.Vec,
	rgb [3]float32,
	th float32, // Thickness at start.
) (mech.Depiction, uint32) {

	i := len(pic.VertexAttributes)
	pic.VertexAttributes = append(pic.VertexAttributes, extendBy1...)
	pos := toV32(global)
	pic.VertexAttributes[i+0] = pos[0]
	pic.VertexAttributes[i+1] = pos[1]
	pic.VertexAttributes[i+2] = pos[2]
	pic.VertexAttributes[i+3] = rgb[0]
	pic.VertexAttributes[i+4] = rgb[1]
	pic.VertexAttributes[i+5] = rgb[2]
	pic.VertexAttributes[i+6] = th
	n++
	return pic, n
}

// AppendQuadratic appends a quadratic Bezier to the depiction.
//
// The first control point shall already be written, by either the append
// initial function or as a shared control point with a previous append
// function.
//
// Returns the possibly reallocated depiction and the new abstract write
// position into the vertex attributes array.
func AppendQuadratic(
	pic mech.Depiction,
	n uint32,
	b maf.Vec,
	c maf.Vec,
	rgb [3]float32,
	th float32,
) (mech.Depiction, uint32) {

	// The quadratic Bezier's first vertex is already written, just add the
	// middle and last vertex's attributes.

	pic.Quadratics = append(pic.Quadratics, n-1, n, n+1)
	i := len(pic.VertexAttributes)
	pic.VertexAttributes = append(pic.VertexAttributes, extendBy2...)
	b32 := toV32(b)
	pic.VertexAttributes[i+0] = b32[0]
	pic.VertexAttributes[i+1] = b32[1]
	pic.VertexAttributes[i+2] = b32[2]
	// 3, 4, 5, 6 (colour/thickness) are not currently used by middle control
	// points.
	c32 := toV32(c)
	pic.VertexAttributes[i+7] = c32[0]
	pic.VertexAttributes[i+8] = c32[1]
	pic.VertexAttributes[i+9] = c32[2]
	pic.VertexAttributes[i+10] = rgb[0]
	pic.VertexAttributes[i+11] = rgb[1]
	pic.VertexAttributes[i+12] = rgb[2]
	pic.VertexAttributes[i+13] = th

	n += 2
	return pic, n
}

// AppendXYInflection appends to the depiction two quadratic Beziers which in
// the case of an inflection will make an almost flat S shape which doesn't
// leave gaps as a single linear quadratic would.
//
// A single quadratic can't handle an inflection.
//
// The first control point shall already be written, by either the append
// initial function or as a shared control point with a previous append
// function.
//
// Returns the possibly reallocated depiction and the new abstract write
// position into the vertex attributes array.
func AppendXYInflection(
	pic mech.Depiction,
	p mech.Portion,
	n uint32,
	fr maf.Frame,
	a, c xySample,
) (mech.Depiction, uint32) {

	// Arbitrary small ratio of the distance A to C. Tis the distance travelled
	// along the tangent lines from each end to the (middle) control points.
	const r = 0.1
	d := r * a.local.To(c.local).Mag()
	b1 := a.local.Add(a.tan.Dir.Scale(d))
	b2 := c.local.Add(c.tan.Dir.Scale(-d))
	tM := (a.t + c.t) / 2
	m := xySample{
		global: fr.PlotXY(b1.MidTo(b2)),
		rgb:    p.Colour(tM),
		th:     p.Thickness32(tM),
	}
	pic, n = AppendQuadratic(pic, n, fr.PlotXY(b1), m.global, m.rgb, m.th)
	pic, n = AppendQuadratic(pic, n, fr.PlotXY(b2), c.global, c.rgb, c.th)
	return pic, n
}

// AppendInflection appends to the depiction two quadratic Beziers which in the
// case of an inflection will make an almost flat S shape which doesn't leave
// gaps as a single linear quadratic would.
//
// A single quadratic can't handle an inflection.
//
// The first control point shall already be written, by either the append
// initial function or as a shared control point with a previous append
// function.
//
// Returns the possibly reallocated depiction and the new abstract write
// position into the vertex attributes array.
func AppendInflection(
	pic mech.Depiction,
	p mech.Portion,
	n uint32,
	fr maf.Frame,
	a, c curveSample3D,
) (mech.Depiction, uint32) {

	global := fr.Out().Transform
	// Arbitrary small ratio of the distance A to C. Tis the distance travelled
	// along the tangent lines from each end to the (middle) control points.
	const r = 0.1
	d := r * a.local.To(c.local).Mag()
	b1 := a.local.Add(a.tan.Dir.Scale(d))
	b2 := c.local.Add(c.tan.Dir.Scale(-d))
	tM := (a.t + c.t) / 2
	m := curveSample3D{
		global: global(b1.MidTo(b2)),
		rgb:    p.Colour(tM),
		th:     p.Thickness32(tM),
	}
	pic, n = AppendQuadratic(pic, n, global(b1), m.global, m.rgb, m.th)
	pic, n = AppendQuadratic(pic, n, global(b2), c.global, c.rgb, c.th)
	return pic, n
}

// The vertex attributes array uses 32 bit floats for the sake of the OpenGL
// renderer.
func toV32(v maf.Vec) [3]float32 {
	return [3]float32{float32(v.X), float32(v.Y), float32(v.Z)}
}
