package depictioner

import (
	"sync"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// A Figure is able to produce a depiction of itself made from quadratic Bezier
// curves.
type Figure struct {
	Fig         mech.Depiction
	FigName     string
	Orientation maf.Frame // The Bezier curve positions are relative to this frame.
	sync.Mutex
}

// Depiction of the figure.
func (fig *Figure) Depiction() mech.Depiction {
	fig.Lock()
	defer fig.Unlock()
	return fig.Fig
}

// Name of the figure.
func (fig *Figure) Name() string {
	fig.Lock()
	defer fig.Unlock()
	return fig.FigName
}

// SetName of the figure.
func (fig *Figure) SetName(name string) {
	fig.Lock()
	defer fig.Unlock()
	fig.FigName = name
}
