package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// A point on a 2D curve.
type xySample struct {
	global maf.Vec    // Global position relative to global/parent frame.
	local  maf.Vec2   // Local position relative to orientation frame.
	rgb    [3]float32 // Colour.
	t      float64    // Proportion, 0..1, through the portion's domain interval.
	tan    maf.Line2  // Local coordinates tangent line.
	th     float32    // Thickness.
}

// Make the start and end control points of the quadratic Beziers for a
// parametric curve.
//
// The control points slice has only the non-middle ones. Except for the very
// first and very last control points each control point is a shared
// starting/ending control point between connected quadratics.
func sampleXYParametric(
	p mech.Portion,
	fr maf.Frame,
	fn XYParametric,
) []xySample {

	vcp := make([]xySample, 1+p.Qual)
	dir := p.Dir()
	for i := 0; i < 1+p.Qual; i++ {
		t := float64(i) / float64(p.Qual)
		local, tan := fn(p.In.T(t))
		vcp[i] = xySample{
			global: fr.PlotXY(local),
			local:  local,
			rgb:    p.Colour(t),
			t:      t,
			tan:    maf.Line2{P: local, Dir: tan.Scale(dir)},
			th:     p.Thickness32(t),
		}
	}
	return vcp
}

// A point on a 3D curve.
type curveSample3D struct {
	global maf.Vec    // Global position relative to global/parent frame.
	local  maf.Vec    // Local position relative to orientation frame.
	rgb    [3]float32 // Colour.
	t      float64    // Proportion, 0..1, through the portion's domain interval.
	tan    maf.Line   // Local coordinates tangent line.
	th     float32    // Thickness.
}

// Make the start and end control points of the quadratic Beziers for a
// parametric curve.
//
// The control points slice has only the non-middles. Except for the very first
// and very last control points each control point is a shared starting/ending
// control point between connected quadratics.
func sampleParametric(
	p mech.Portion,
	fr maf.Frame,
	// The position and tangential unit direction vector.
	fn func(float64) (maf.Vec, maf.Vec),
) []curveSample3D {

	vcp := make([]curveSample3D, 1+p.Qual)
	dir := p.Dir()
	toGlobal := fr.Out().Transform
	for i := 0; i < 1+p.Qual; i++ {
		t := float64(i) / float64(p.Qual)
		local, tan := fn(p.In.T(t))
		vcp[i] = curveSample3D{
			global: toGlobal(local),
			local:  local,
			rgb:    p.Colour(t),
			t:      t,
			tan:    maf.Line{P: local, Dir: tan.Scale(dir)},
			th:     p.Thickness32(t),
		}
	}
	return vcp
}

// A point on a line segment.
type lineSegSample struct {
	global maf.Vec    // Global position relative to global/parent frame.
	rgb    [3]float32 // Colour.
	th     float32    // Thickness.
}

// Make the start and end control points of the quadratic Beziers for a line
// segment.
//
// The control points slice has only the non-middle ones. Except for the very
// first and very last control points each control point is a shared
// starting/ending control point between connected quadratics.
func sampleLineSeg(
	p mech.Portion,
	fr maf.Frame,
	seg maf.LineSeg,
) []lineSegSample {

	vcp := make([]lineSegSample, 1+p.Qual)
	toGlobal := fr.Out().Transform
	// Make the control points.
	//
	// The control points slice has only the non-middle ones. Except for the
	// very first and very last control points each control point is a shared
	// starting/ending control point between connected quadratics.
	for i := 0; i < 1+p.Qual; i++ {
		t := float64(i) / float64(p.Qual)
		vcp[i] = lineSegSample{
			global: toGlobal(seg.AtT(p.In.T(t))),
			rgb:    p.Colour(t),
			th:     p.Thickness32(t),
		}
	}
	return vcp
}

// Control point in a cartesian curve's quadratic Bezier.
type xyCartesianSample struct {
	xySample
	curvature float64 // Output from curvature.
	grad      float64 // Gradient.
}

// sampleXYCartesian makes the start and end control points of the quadratic
// Beziers for a Cartesian curve.
//
// The control points slice has only the non-middle ones. Except for the very
// first and very last control points each control point is a shared
// starting/ending control point between connected quadratics.
func sampleXYCartesian(
	p mech.Portion,
	fr maf.Frame,
	fn XYCartesian,
) []xyCartesianSample {

	vcp := make([]xyCartesianSample, 1+p.Qual)
	for i := 0; i < 1+p.Qual; i++ {
		t := float64(i) / float64(p.Qual)
		x := p.In.T(t)
		y, grad, cvr := fn(x)
		local := maf.Vec2{X: x, Y: y}
		vcp[i] = xyCartesianSample{
			xySample: xySample{
				global: fr.PlotXY(local),
				local:  local,
				rgb:    p.Colour(t),
				t:      t,
				tan: maf.Line2{
					P:   local,
					Dir: maf.Vec2{X: p.Dir(), Y: grad}.Unit(),
				},
				th: p.Thickness32(t),
			},
			curvature: cvr,
			grad:      grad,
		}
	}
	return vcp
}

type functionSample lineSegSample

// sampleFunction makes the start and end control points of the quadratic
// Beziers for an arbitrary function/curve.
//
// The control points slice has only the non-middle ones. Except for the very
// first and very last control points each control point is a shared
// starting/ending control point between connected quadratics.
func sampleFunction(
	p mech.Portion,
	fr maf.Frame,
	fn func(t float64) maf.Vec,
) []functionSample {

	vcp := make([]functionSample, 1+p.Qual)
	toGlobal := fr.Out().Transform
	for i := 0; i < 1+p.Qual; i++ {
		t := float64(i) / float64(p.Qual)
		x := p.In.T(t)
		vcp[i] = functionSample{
			global: toGlobal(fn(x)),
			rgb:    p.Colour(t),
			th:     p.Thickness32(t),
		}
	}
	return vcp
}
