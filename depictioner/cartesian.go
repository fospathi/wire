package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// XYCartesian defines a Cartesian function/graph in the XY plane and maps an
// abscissa (X-coordinate) to the function's ordinate (Y-coordinate), gradient,
// and concavity.
type XYCartesian func(float64) (float64, float64, float64)

// DepictXYCartesian depicts a portion of a Cartesian function's curve
// using quadratic Bezier curves.
//
// The input quality parameter shall be sufficiently large that no interval
// division contains more than one inflection point.
func DepictXYCartesian(
	p mech.Portion,
	// The curve is defined in the XY plane of this frame.
	fr maf.Frame,
	fn XYCartesian,
) mech.Depiction {

	pic := mech.Depiction{}
	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	// Current number of abstract vertex attributes added, i.e. the abstract
	// current write position into the vertex attributes array.
	var n uint32

	// Opposite curvature signs indicate an inflection.
	inflects := func(cvr0, cvr1 float64) bool {
		return (cvr0 >= 0) != (cvr1 >= 0)
	}

	vcp := sampleXYCartesian(p, fr, fn)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i++ {
		a, c := vcp[i], vcp[i+1]
		if inflects(a.curvature, c.curvature) {
			// An inflection point switches between opposite curvatures.
			pic, n = AppendXYInflection(pic, p, n, fr, a.xySample, c.xySample)
			continue
		}
		b := a.tan.ClosestPoint(c.tan)
		if a.tan.Dir.Dot(a.local.To(b)) <= 0 || c.tan.Dir.Dot(c.local.To(b)) >= 0 {
			// Something went wrong if b, the middle control point, ends up
			// "outside" the other two control points. Just use a straight line
			// in that case.
			pic, n = AppendQuadratic(
				pic, n, a.global.MidTo(c.global), c.global, c.rgb, c.th)
			continue
		}
		pic, n = AppendQuadratic(pic, n, fr.PlotXY(b), c.global, c.rgb, c.th)
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}
