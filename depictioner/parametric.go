package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// XYParametric defines a parametric function/graph in the XY plane and maps a
// parameter to the function's position and unit tangent direction vector.
type XYParametric func(float64) (maf.Vec2, maf.Vec2)

// PlanarParametric defines a parametric function/graph which is known to be
// constrained to a plane in 3D space and maps a parameter to the function's
// position and unit tangent direction vector.
type PlanarParametric func(float64) (maf.Vec, maf.Vec)

// Parametric defines a parametric function/graph in 3D space and maps a
// parameter to the function's position and unit tangent direction vector.
type Parametric func(float64) (maf.Vec, maf.Vec)

// DepictXYParametric depicts a portion of a parametric function's curve using
// quadratic Bezier curves.
func DepictXYParametric(
	p mech.Portion,
	fr maf.Frame, // The curve is defined in the XY plane of this frame.
	fn XYParametric,
) mech.Depiction {

	var (
		pic = mech.Depiction{}

		// Current number of vertices added, i.e. the abstract current write
		// position into the vertex attributes array.
		n uint32
	)

	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	vcp := sampleXYParametric(p, fr, fn)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i++ {
		a, c := vcp[i], vcp[i+1]
		b, _, ok := a.tan.Closest(c.tan)
		if !ok ||
			a.tan.Dir.Dot(a.local.To(b)) <= 0 ||
			c.tan.Dir.Dot(c.local.To(b)) >= 0 {
			// Either an inflection-like change of curvature or the curve folds
			// back on itself.

			pic, n = AppendXYInflection(pic, p, n, fr, a, c)
			continue
		}
		pic, n = AppendQuadratic(pic, n, fr.PlotXY(b), c.global, c.rgb, c.th)
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}

// DepictPlanarParametric depicts a portion of a planar parametric function's
// curve using quadratic Bezier curves.
func DepictPlanarParametric(
	p mech.Portion,
	fr maf.Frame, // The curve is defined/oriented in this frame.
	fn PlanarParametric,
) mech.Depiction {

	var (
		pic = mech.Depiction{}

		// Current number of vertices added, i.e. the abstract current write
		// position into the vertex attributes array.
		n uint32

		global = fr.Out().Transform
	)

	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	vcp := sampleParametric(p, fr, fn)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i++ {
		a, c := vcp[i], vcp[i+1]
		b, _, ok := a.tan.ClosestPoints(c.tan)
		if !ok ||
			a.tan.Dir.Dot(a.local.To(b)) <= 0 ||
			c.tan.Dir.Dot(c.local.To(b)) >= 0 {
			// Either an inflection-like change of curvature or the curve folds
			// back on itself.

			pic, n = AppendInflection(pic, p, n, fr, a, c)
			continue
		}
		pic, n = AppendQuadratic(pic, n, global(b), c.global, c.rgb, c.th)
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}

// DepictParametric depicts a portion of a parametric function's curve
// using quadratic Bezier curves.
func DepictParametric(
	p mech.Portion,
	fr maf.Frame, // The curve is defined/oriented in this frame.
	fn Parametric,
) mech.Depiction {

	var (
		pic = mech.Depiction{}

		// Current number of vertices added, i.e. the abstract current write
		// position into the vertex attributes array.
		n uint32

		global = fr.Out().Transform
	)

	if p.Qual < 2 {
		p.Qual = 2
	} else if p.Qual%2 != 0 {
		// The quality needs to be an even number since the depictions are
		// made with pairs of quadratic Beziers.
		p.Qual++
	}

	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	vcp := sampleParametric(p, fr, fn)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i += 2 {
		p1, p2, p3 := vcp[i], vcp[i+1], vcp[i+2]   // non-middle control points of two quadratic Beziers.
		b1, _, ok1 := p1.tan.ClosestPoints(p2.tan) // middle control point of first quadratic Bezier.
		_, b2, ok2 := p2.tan.ClosestPoints(p3.tan) // middle control point of second quadratic Bezier.
		if !ok1 || !ok2 ||
			p1.tan.Dir.Dot(p1.local.To(p2.local)) <= 0 ||
			p3.tan.Dir.Dot(p3.local.To(p2.local)) >= 0 {
			// Either an inflection-like change of curvature or the curve folds
			// back on itself.

			pic, n = AppendInflection(pic, p, n, fr, p1, p3)
			continue
		}
		b := maf.Line{P: b1, Dir: b1.To(b2).Unit()}.Closest(p2.local)
		pic, n = AppendQuadratic(pic, n, global(b1), global(b), p2.rgb, p2.th)
		pic, n = AppendQuadratic(pic, n, global(b2), p3.global, p3.rgb, p3.th)
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}
