package depictioner

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// DepictLine depicts a portion of a line segment using quadratic Bezier curves.
func DepictLine(
	p mech.Portion,
	// The line segment is defined/oriented in this frame.
	fr maf.Frame,
	seg maf.LineSeg,
) mech.Depiction {

	pic := mech.Depiction{}
	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	// Current number of vertices added, i.e. the abstract current write
	// position into the vertex attributes array.
	var n uint32

	vcp := sampleLineSeg(p, fr, seg)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i++ {
		a, c := vcp[i], vcp[i+1]
		pic, n = AppendQuadratic(
			pic, n, a.global.MidTo(c.global), c.global, c.rgb, c.th)
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}

// DepictFunction depicts the line segments connecting a sequence of points on
// the given function using quadratic Bezier curves.
func DepictFunction(
	p mech.Portion,
	// The function is defined/oriented in this frame.
	fr maf.Frame,
	fn func(t float64) maf.Vec,
) mech.Depiction {

	pic := mech.Depiction{}
	if p.Caps[0] == mech.Circle {
		pic.Opening = append(pic.Opening, 0, 1, 2)
	}

	// Current number of vertices added, i.e. the abstract current write
	// position into the vertex attributes array.
	var n uint32

	vcp := sampleFunction(p, fr, fn)
	pic, n = AppendInitial(pic, n, vcp[0].global, vcp[0].rgb, vcp[0].th)
	// Add the rest of the vertex attributes.
	for i := 0; i < p.Qual; i++ {
		a, c := vcp[i], vcp[i+1]
		pic, n = AppendQuadratic(
			pic, n, a.global.MidTo(c.global), c.global, c.rgb, c.th)

		// Circular line caps hide gaps left by adjacent discontinuous
		// quadratics.
		if p.Qual > 1 {
			if i != 0 {
				pic.Opening = append(pic.Opening, n-3, n-2, n-1)
			} else if i != p.Qual-1 {
				pic.Closing = append(pic.Closing, n-3, n-2, n-1)
			}
		}
	}

	if p.Caps[1] == mech.Circle {
		pic.Closing = append(pic.Closing, n-3, n-2, n-1)
	}

	return pic
}
