# Package cerial

```go
import "gitlab.com/fospathi/wire/wireserver/cerial"
```

Go package cerial assists with websocket data shifting tasks.
