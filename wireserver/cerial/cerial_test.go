package cerial_test

import (
	"encoding/gob"
	"io"
	"log"
	"net"
	"net/http"
	"reflect"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wireserver/cerial"
)

func TestBinaryReader(t *testing.T) {
	result := make(chan mech.Depiction)

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var (
			c   *websocket.Conn
			err error
			u   = websocket.Upgrader{}
		)
		if c, err = u.Upgrade(w, r, nil); err != nil {
			t.Fatal(err)
		}
		c.SetReadLimit(1e7)
		defer c.Close()
		gd := gob.NewDecoder(&cerial.BinaryReader{Conn: c})
		for {
			var pic mech.Depiction

			if err = gd.Decode(&pic); err != nil {
				if websocket.IsCloseError(err, websocket.CloseNormalClosure) {
					break
				} else if err != io.EOF {
					t.Fatal(err)
				}
			}

			result <- pic
		}
	})

	var (
		err      error
		host     = "127.0.0.1:0"
		listener net.Listener
	)
	if listener, err = net.Listen("tcp", host); err != nil {
		t.Fatal(err)
	}
	svr := http.Server{}
	svr.Handler = mux
	go func() {
		if err = svr.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()

	var (
		c    *websocket.Conn
		addr = "ws://" + listener.Addr().String()
	)
	c, _, err = websocket.DefaultDialer.Dial(addr, nil)
	if err != nil {
		t.Fatal(err)
	}
	ge := gob.NewEncoder(cerial.BinaryWriter{Conn: c})

	{
		want := mech.Depiction{
			VertexAttributes: []float32{1, 2, 3},
			Quadratics:       []uint32{4, 5},
			Closing:          []uint32{0},
		}
		ge.Encode(want)
		got := <-result

		if !reflect.DeepEqual(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = mech.Depiction{
			VertexAttributes: []float32{1, 2, 3, 4, 5, 6},
			Quadratics:       []uint32{7, 8, 9},
			Opening:          []uint32{0},
		}
		ge.Encode(want)
		got = <-result

		if !reflect.DeepEqual(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		mediumSlice := make([]float32, 1e5)
		mediumSlice[1e5-1] = 1
		mediumSlice[1e5-100] = 2
		mediumSlice[1e5-1000] = 3
		mediumSlice[0] = 4
		mediumSlice[100] = 5
		mediumSlice[10000] = 6
		want := mech.Depiction{
			VertexAttributes: mediumSlice,
			Quadratics:       []uint32{7, 8, 9},
			Opening:          []uint32{10},
		}
		ge.Encode(want)
		got := <-result

		if !reflect.DeepEqual(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "bigSlice", got)
		}
	}

	{
		bigSlice := make([]float32, 1e6)
		bigSlice[1e6-1] = 1
		bigSlice[1e6-100] = 2
		bigSlice[1e6-1000] = 3
		bigSlice[0] = 4
		bigSlice[100] = 5
		bigSlice[10000] = 6
		want := mech.Depiction{
			VertexAttributes: bigSlice,
			Quadratics:       []uint32{7, 8, 9},
			Opening:          []uint32{10},
		}
		ge.Encode(want)
		got := <-result

		if !reflect.DeepEqual(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "bigSlice", got)
		}
	}

	c.WriteControl(
		websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, "closing"),
		time.Now().Add(1*time.Second),
	)
	time.Sleep(time.Second)
}
