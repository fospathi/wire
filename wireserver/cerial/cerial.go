package cerial

import (
	"bytes"
	"errors"

	"github.com/gorilla/websocket"
)

// BinaryReader wraps a websocket connection and implements the io.Reader
// interface.
type BinaryReader struct {
	Conn *websocket.Conn
	r    *bytes.Reader
}

// Read binary messages from the websocket into the argument buffer.
func (br *BinaryReader) Read(p []byte) (n int, err error) {
	if br.Conn == nil {
		return 0, errAbsentConnection
	}

	var (
		b   []byte
		typ int
	)

	if br.r != nil {
		n, err = br.r.Read(p)
		if br.r.Len() == 0 {
			br.r = nil
		}
		return
	}

	for {
		if typ, b, err = br.Conn.ReadMessage(); err != nil {
			return
		}
		if websocket.BinaryMessage != typ {
			continue
		}
		br.r = bytes.NewReader(b)
		n, err = br.r.Read(p)
		if br.r.Len() == 0 {
			br.r = nil
		}
		return
	}
}

// BinaryWriter wraps a websocket connection and implements the io.Writer
// interface.
type BinaryWriter struct {
	Conn *websocket.Conn
}

// Write binary messages to the websocket from the argument buffer.
func (bw BinaryWriter) Write(p []byte) (n int, err error) {
	if bw.Conn == nil {
		return 0, errAbsentConnection
	}
	if err = bw.Conn.WriteMessage(websocket.BinaryMessage, p); err != nil {
		return 0, err
	}
	return len(p), nil
}

var errAbsentConnection = errors.New("the websocket connection is nil")
