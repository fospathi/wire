# Package wireserver

```go
import "gitlab.com/fospathi/wire/wireserver"
```

Go package wireserver wraps a [wiregl](https://gitlab.com/fospathi/wire/-/tree/master/wiregl) context with a HTTP server and serves images in response to requests containing scenes composed of quadratic Bezier curves.

