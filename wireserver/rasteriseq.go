package wireserver

import (
	"errors"

	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/universal/watchable"
	"gitlab.com/fospathi/wire/wiregl"
)

// newRasteriseQ constructs a new DrawQ.
func newRasteriseQ(cx *wiregl.Context) rasteriseQ {
	q := watchable.NewBoundedQ(mech.WireserverMaxRasteriseQLen)
	go func() {
		var w = q.Watch()
	loop:
		for {
			select {
			case <-q.Done:
				break loop

			case <-w:
				var v interface{}
				{
					var err error
					if v, err = q.Pop(); err != nil {
						w = q.Watch()
						continue
					}
				}
				qi := v.(qItem)
				qi.c <- rasteriseRes{err: nil, pix: cx.Rasterise(qi.s)}
			}
		}
	}()
	return rasteriseQ{
		cx: cx,
		q:  q,
	}
}

// rasteriseQ is a queue of rasterise requests that feeds the rasterise requests
// one at a time into the wrapped wiregl context.
type rasteriseQ struct {
	cx *wiregl.Context
	q  *watchable.BoundedQ
}

// Rasterise the scene and return its pixels.
func (q rasteriseQ) Rasterise(s wiregl.Scene) (wiregl.Pixels, error) {
	c := make(chan rasteriseRes)
	if err := q.q.Add(qItem{c: c, s: s}); err != nil {
		return wiregl.Pixels{}, err
	}
	rr := <-c
	return rr.pix, rr.err
}

// Shutdown closes the queue and ends queued rasterise requests with a shutdown
// error.
func (q rasteriseQ) Shutdown() {
	vQI := q.q.PopAllAndClose()
	for _, v := range vQI {
		qi := v.(qItem)
		qi.c <- rasteriseRes{err: errShutdown}
	}
}

type rasteriseRes struct {
	err error
	pix wiregl.Pixels
}

type qItem struct {
	c chan rasteriseRes
	s wiregl.Scene
}

// IsShutdownError is whether the argument error indicates that the thing is
// shutting down.
func IsShutdownError(err error) bool {
	return err == errShutdown
}

var errShutdown = errors.New("shutdown in progress")
