/*
Package wireserver implements a wiregl server that wraps around a wiregl context
and serves images in response to HTTP requests containing scenes composed of
quadratic Bezier curves.
*/
package wireserver

import (
	"embed"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

var (
	// webFS is a file system containing the files which implement the
	// wireserver's browser based home page.
	//
	//go:embed wireserver/web
	webFS   embed.FS
	webPath = "wireserver/web"
)

// Serve accepts incoming scene rasterisation requests on a new wireserver.
//
// Serve shall be executed in the process' main thread.
//
// After a successful start the new wireserver's host is printed to stdout with
// the given prefix.
func Serve(l *log.Logger, prefix string) {
	hostname := flag.String("hostname", mech.WireserverHostname,
		"set the hostname of the wireserver's host")
	port := flag.Uint("port", mech.WireserverPort,
		"set the port of the wireserver's host")
	flag.Parse()
	host := *hostname + ":" + strconv.Itoa(int(*port))

	mc, initRes := wiregl.NewContext(wiregl.ContextSpec{
		Samples: mech.MSAA,
	})

	go func() {
		res := <-initRes
		if res.Err != nil {
			l.Println(res.Err)
			return
		}
		cx := res.Context
		defer cx.Close()

		var (
			err      error
			listener net.Listener
			mux      = http.NewServeMux()
			q        = newRasteriseQ(cx)
			root     fs.FS
			svr      = http.Server{
				ErrorLog: l,
				Handler:  mux,
			}
		)
		if root, err = fs.Sub(webFS, webPath); err != nil {
			l.Println(err)
			return
		}
		if listener, err = net.Listen("tcp", host); err != nil {
			l.Println(err)
			return
		}
		mux.HandleFunc(mech.WireserverWSGobPath, wsGobRasteriseHandler(l, q))
		mux.HandleFunc(mech.WireserverTCPGobPath, tcpGobRasteriseHandler(l, q))
		mux.Handle("/", http.FileServer(http.FS(root)))
		fmt.Fprintln(os.Stdout, prefix+"http://"+listener.Addr().String())

		go func() {
			defer svr.Close()
			sc := make(chan os.Signal, 1)
			signal.Notify(sc, os.Interrupt, syscall.SIGTERM)
			sig := <-sc
			fmt.Printf("\nIntercepted interrupt signal: %v.\n", sig)
			fmt.Printf("Shutting down...\n")
		}()

		if err = svr.Serve(listener); err != nil {
			if err != http.ErrServerClosed {
				l.Println(err)
			}
			return
		}
	}()

	for f := range mc {
		f()
	}
}
