package wireserver

import (
	"encoding/gob"
	"errors"
	"net/url"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
	"gitlab.com/fospathi/wire/wireserver/cerial"
)

// WSGobClient is a websocket connection to a wireserver that sends scenes to be
// drawn and returns their pixels.
type WSGobClient struct {
	c    *websocket.Conn
	decC <-chan rasteriseRes
	done chan struct{}
	enc  *gob.Encoder
	host string
	m    *sync.Mutex
	md   *sync.Mutex
}

// Close the websocket and this client's websocket read loop.
//
// Close is a slow op because it includes a noticeable delay to give enough time
// for the close message to be sent.
func (cl WSGobClient) Close() {
	cl.m.Lock()
	defer cl.m.Unlock()

	select {
	case <-cl.done:
		return
	default:
	}

	mech.NormalClosure(cl.c, "WSGobClient closing")
	close(cl.done)
}

// decode the pixels returned by the wireserver in response to a previous encode.
func (cl WSGobClient) decode() (wiregl.Pixels, error) {
	dr, ok := <-cl.decC
	if !ok {
		return wiregl.Pixels{}, errClosed
	}
	return dr.pix, dr.err
}

// encode the scene and send it to the wireserver to be drawn.
func (cl WSGobClient) encode(sc wiregl.Scene) error {
	select {
	case <-cl.done:
		return errClosed
	default:
	}

	return cl.enc.Encode(sc)
}

// Host of the wireserver which was connected to, as given to the receiver at
// construction.
func (cl WSGobClient) Host() string {
	return cl.host
}

// Rasterise the scene and return its pixels.
func (cl WSGobClient) Rasterise(sc wiregl.Scene) (wiregl.Pixels, error) {
	cl.md.Lock()
	defer cl.md.Unlock()

	if err := cl.encode(sc); err != nil {
		return wiregl.Pixels{}, err
	}
	// TODO verify the rasterise result is sensible.
	return cl.decode()
}

// NewWSGobClient constructs a new WSGobClient.
func NewWSGobClient(host string) (WSGobClient, error) {
	u := url.URL{Scheme: "ws", Host: host, Path: mech.WireserverWSGobPath}
	dialer := websocket.Dialer{}
	c, _, err := dialer.Dial(u.String(), nil)
	if err != nil {
		return WSGobClient{}, err
	}
	dec := gob.NewDecoder(&cerial.BinaryReader{Conn: c})
	decC := make(chan rasteriseRes)
	done := make(chan struct{})
	go func() {
		defer close(decC)
		var (
			err error
			pix wiregl.Pixels
		)
	loop:
		for {
			err = dec.Decode(&pix)
			select {
			case <-done:
				break loop
			case decC <- rasteriseRes{err: err, pix: pix}:
			}
		}
	}()
	return WSGobClient{
		c:    c,
		decC: decC,
		done: done,
		enc:  gob.NewEncoder(cerial.BinaryWriter{Conn: c}),
		host: host,
		m:    &sync.Mutex{},
		md:   &sync.Mutex{},
	}, nil
}

// IsClosedError is whether the argument error indicates that the connection or
// thing is closed.
//
// TODO test this function, do websocket errors propagate up in closures?
func IsClosedError(err error) bool {
	if err == errClosed || websocket.IsUnexpectedCloseError(err) {
		return true
	}
	return false
}

var errClosed = errors.New("closed")
