package wireserver

import (
	"encoding/gob"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
	"gitlab.com/fospathi/wire/wireserver/cerial"
)

// wsGobRasteriseHandler handles ws wireserver rasterise requests and expects
// the scene/content to be in gob format.
func wsGobRasteriseHandler(l *log.Logger, q rasteriseQ) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u := websocket.Upgrader{}
		c, err := u.Upgrade(w, r, nil)
		if err != nil {
			l.Printf("ws upgrade failure: %v: %v\n", mech.WireserverWSGobPath, err)
			return
		}
		defer c.Close()

		dec := gob.NewDecoder(&cerial.BinaryReader{Conn: c})
		enc := gob.NewEncoder(cerial.BinaryWriter{Conn: c})
		for {
			var (
				err error
				pix wiregl.Pixels
				s   wiregl.Scene
			)
			if err = dec.Decode(&s); err != nil {
				l.Println("ws gob decode error: " + r.RemoteAddr)
				mech.PolicyViolation(c,
					"decode failure: possible incorrect message format")
				return
			}
			if pix, err = q.Rasterise(s); err != nil {
				if IsShutdownError(err) {
					mech.InternalServerError(c,
						"internal server error for rasterising a scene: "+
							"server is shutting down")
					return
				}
				l.Println("ws gob draw error: " + r.RemoteAddr)
				mech.InternalServerError(c,
					"internal server error for rasterising a scene")
				return
			}
			if err = enc.Encode(pix); err != nil {
				l.Println("ws gob encode error")
				mech.InternalServerError(c,
					"internal server error for encoding scene's pixels")
				return
			}
		}
	}
}

// tcpGobRasteriseHandler hijacks wireserver draw requests to get the underlying
// TCP connection and expects the scene/content to be in gob format.
func tcpGobRasteriseHandler(l *log.Logger, q rasteriseQ) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		hj, ok := w.(http.Hijacker)
		if !ok {
			http.Error(w,
				"webserver doesn't support hijacking",
				http.StatusInternalServerError)
			return
		}
		c, rw, err := hj.Hijack()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer c.Close()

		enc := gob.NewEncoder(rw)
		dec := gob.NewDecoder(rw)
		for {
			var (
				err error
				pix wiregl.Pixels
				s   wiregl.Scene
			)
			// TODO is eof check superfluous?
			if err = dec.Decode(&s); err != nil && err != io.EOF {
				l.Println("tcp gob decode error: " + r.RemoteAddr)
				return
			}
			if pix, err = q.Rasterise(s); err != nil {
				if IsShutdownError(err) {
					return
				}
				l.Println("tcp gob rasterise error: " + r.RemoteAddr)
				return
			}
			if err = enc.Encode(pix); err != nil {
				l.Println("tcp gob encode error")
				return
			}
			if err = rw.Flush(); err != nil {
				l.Println("tcp gob flush error")
				return
			}
		}
	}
}
