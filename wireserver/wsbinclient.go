package wireserver

import (
	"errors"
	"net/url"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

// WSBinClient is a websocket connection to a wireserver that sends scenes to be
// drawn and returns their pixels.
type WSBinClient struct {
	c    *websocket.Conn
	decC <-chan binaryRasteriseRes
	done chan struct{}
	m    *sync.Mutex
	md   *sync.Mutex
}

// Close the websocket and this client's websocket read loop.
//
// Close is a slow op because it includes a noticeable delay to give enough time
// for the close message to be sent.
func (cl WSBinClient) Close() {
	cl.m.Lock()
	defer cl.m.Unlock()

	select {
	case <-cl.done:
		return
	default:
	}

	mech.NormalClosure(cl.c, "WSBinClient closing")
	close(cl.done)
}

// decode the pixels returned by the wireserver in response to a previous encode.
func (cl WSBinClient) decode() (wiregl.BinaryPixelData, error) {
	res, ok := <-cl.decC
	if !ok {
		return wiregl.BinaryPixelData{}, errClosed
	}
	return res.pix, res.err
}

// encode the scene and send it to the wireserver to be drawn.
func (cl WSBinClient) encode(sc wiregl.Scene) error {
	select {
	case <-cl.done:
		return errClosed
	default:
	}

	// TODO sc.ToBinary()
	return cl.c.WriteMessage(websocket.BinaryMessage, []byte{})
}

// Rasterise the scene and return its pixels.
func (cl WSBinClient) Rasterise(sc wiregl.Scene) (wiregl.BinaryPixelData, error) {
	cl.md.Lock()
	defer cl.md.Unlock()

	if err := cl.encode(sc); err != nil {
		return wiregl.BinaryPixelData{}, err
	}
	// TODO verify the rasterise result is sensible.
	return cl.decode()
}

// NewWSBinClient constructs a new WSBinClient.
func NewWSBinClient(host string) (WSBinClient, error) {
	u := url.URL{Scheme: "ws", Host: host, Path: mech.WireserverWSBinPath}
	dialer := websocket.Dialer{}
	c, _, err := dialer.Dial(u.String(), nil)
	if err != nil {
		return WSBinClient{}, err
	}
	decC := make(chan binaryRasteriseRes)
	done := make(chan struct{})
	go func() {
		defer close(decC)
		var (
			b       []byte
			err     error
			msgType int
		)
	loop:
		for {
			msgType, b, err = c.ReadMessage()
			if err == nil && msgType != websocket.BinaryMessage {
				err = errors.New("ws bin client: expected binary message")
			}
			select {
			case <-done:
				break loop
			case decC <- binaryRasteriseRes{err: err, pix: b}:
			}
		}
	}()
	return WSBinClient{
		c:    c,
		decC: decC,
		done: done,
		m:    &sync.Mutex{},
		md:   &sync.Mutex{},
	}, nil
}

type binaryRasteriseRes struct {
	err error
	pix wiregl.BinaryPixelData
}
