package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/fospathi/wire/wireserver"
)

func main() {
	l := log.New(os.Stderr, "mechane wireserver: ", log.LstdFlags)
	wireserver.Serve(l, parse())
}

// Parse the 'prefix' command-line flag.
func parse() string {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of wireserver:\n")
		flag.PrintDefaults()
	}
	prefix := flag.String(
		"prefix",
		"mechane wireserver: ",
		"Use this prefix when printing the wireserver's host on stdout",
	)
	flag.Parse()
	return *prefix
}
