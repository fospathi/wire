package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewGrid constructs a new Grid which is initially oriented with a standard
// frame.
func NewGrid(spec GridFiguration) *Grid {
	return &Grid{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// GridFiguration specifies the geometry of a rectangular grid.
type GridFiguration struct {
	// The horizontal and vertical cell dimensions respectively.
	CX, CY float64
	// The horizontal and vertical grid dimensions respectively.
	GX, GY float64

	// The offset of the grid pattern from its default position.
	//
	// The default grid pattern places an intersection point of a horizontal and
	// vertical grid line at the centre of the grid. This intersection point is
	// displaced by the given values modulo their respective cell dimensions.
	//
	// Even though the grid's pattern is displaced for non-zero values, the grid
	// area itself is not displaced and it occupies the same rectangular area.
	OX, OY float64

	// The displacements added to the grid lines' Z-values for the lines
	// parallel to the X and Y-axes respectively.
	//
	// Using non-zero values for these can create gaps that prevent issues with
	// the otherwise overlapping lines.
	ZX, ZY float64
}

// A Grid is a wireframe grid figure.
//
// Relative to its orientation frame the grid area is in the Z=0 plane and is
// centered on the origin.
type Grid struct {
	depictioner.Figure

	GridFiguration
}

// A GridPortion specifies which parts of a grid's wireframe to show.
type GridPortion struct {
	X []mech.Portion // Portions of each of the horizontal lines.
	Y []mech.Portion // Portions of each of the vertical lines.
}

// Depict the given portions/(parameter intervals) on the grid, overwriting the
// previous depiction(s) if any.
func (fig *Grid) Depict(p GridPortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictGrid(fig.Orientation, fig.GridFiguration, p)
}

// DepictGrid depicts the given grid portions/(parameter intervals) on a grid.
func DepictGrid(
	fr maf.Frame,
	spec GridFiguration,
	p GridPortion,
) mech.Depiction {

	pic := mech.Depiction{}

	if spec.OX != 0 {
		spec.OX = math.Mod(spec.OX, spec.CX)
	}
	if spec.OY != 0 {
		spec.OY = math.Mod(spec.OY, spec.CY)
	}

	// Start at leftmost and bottommost lines.
	safetyX := spec.GX / 1e10 // Compensate for floating point rounding errors.
	safetyY := spec.GY / 1e10
	x1, x2 := -(spec.GX+safetyX)/2, (spec.GX+safetyX)/2
	y1, y2 := -(spec.GY+safetyY)/2, (spec.GY+safetyY)/2
	//             |
	//             |                 A coordinate system whose origin
	//             |                 is at the centre of the grid area.
	//   - - - - - | - - - - - |
	//             | O         |
	//             |           |
	//   <---------|           V (-GY/2)
	// (-GX/2)
	//
	// The formula for the x coordinates of all possible grid lines is
	//     x = OX + n * CX      (for any n: ..., -2, -1, 0, 1, 2, ...)
	// Rearrange for n
	//     n = (x - OX) / CX
	// Now, to get the lowest value of n inside the grid area, let x = -GX/2
	//     n = (-GX/2 - OX) / CX
	// Take the smallest integer greater than this value. Plug that integer back
	// into the original equation to get the x coordinate of the leftmost valid
	// grid line.
	//
	// Similarly for the y value.
	x := spec.OX + math.Ceil((x1-spec.OX)/spec.CX)*spec.CX
	y := spec.OY + math.Ceil((y1-spec.OY)/spec.CY)*spec.CY
	lineFr := fr
	{ // Vertical lines.
		lineFr.O.Z = spec.ZY
		seg := maf.LineSeg{
			P1: maf.Vec{Y: -spec.GY / 2},
			P2: maf.Vec{Y: spec.GY / 2},
		}
		for ; x < x2; x += spec.CX {
			seg.P1.X, seg.P2.X = x, x
			pic = pic.Append(DepictLineSeg(lineFr, seg, p.Y))
		}
	}
	{ // Horizontal lines.
		lineFr.O.Z = spec.ZX
		seg := maf.LineSeg{
			P1: maf.Vec{X: -spec.GX / 2},
			P2: maf.Vec{X: spec.GX / 2},
		}
		for ; y < y2; y += spec.CY {
			seg.P1.Y, seg.P2.Y = y, y
			pic = pic.Append(DepictLineSeg(lineFr, seg, p.X))
		}
	}

	return pic
}

// NewGridDisk constructs a new GridDisk which is initially oriented with a
// standard frame.
func NewGridDisk(spec GridDiskFiguration) *GridDisk {
	return &GridDisk{depictioner.Figure{Orientation: maf.StdFrame}, spec}
}

// GridDiskFiguration specifies the geometry of a circular section of a
// rectangular grid.
type GridDiskFiguration struct {
	GridFiguration
	// The radius of the disk.
	R float64
}

// A GridDisk is a circular section of a wireframe grid figure.
//
// Relative to its orientation frame the disk is in the Z=0 plane and is
// centered on the origin.
type GridDisk struct {
	depictioner.Figure

	GridDiskFiguration
}

// Depict the grid disk, overwriting the previous depiction(s) if any.
func (fig *GridDisk) Depict(p GridPortion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictGridDisk(fig.Orientation, fig.GridDiskFiguration, p)
}

// DepictGridDisk depicts the given grid portions/(parameter intervals) on a
// grid disk.
func DepictGridDisk(
	fr maf.Frame,
	spec GridDiskFiguration,
	p GridPortion,
) mech.Depiction {

	pic := mech.Depiction{}

	if spec.OX != 0 {
		spec.OX = math.Mod(spec.OX, spec.CX)
	}
	if spec.OY != 0 {
		spec.OY = math.Mod(spec.OY, spec.CY)
	}

	safetyX := spec.GX / 1e10 // Compensate for floating point rounding errors.
	safetyY := spec.GY / 1e10
	x1, x2 := -(spec.GX+safetyX)/2, (spec.GX+safetyX)/2
	y1, y2 := -(spec.GY+safetyY)/2, (spec.GY+safetyY)/2
	x := spec.OX + math.Ceil((x1-spec.OX)/spec.CX)*spec.CX
	y := spec.OY + math.Ceil((y1-spec.OY)/spec.CY)*spec.CY
	lineFr := fr
	{ // Vertical lines.
		lineFr.O.Z = spec.ZY
		seg := maf.LineSeg{
			P1: maf.Vec{Y: -spec.GY / 2},
			P2: maf.Vec{Y: spec.GY / 2},
		}
		for ; x < x2; x += spec.CX {
			if x <= -spec.R {
				continue
			}
			if x >= spec.R {
				break
			}
			seg.P1.X, seg.P2.X = x, x
			chord := 2 * math.Sqrt(spec.R*spec.R-x*x)
			if chord >= spec.GY {
				pic = pic.Append(DepictLineSeg(lineFr, seg, p.Y))
				continue
			}
			t := (chord / spec.GY) / 2
			// The Y-interval of the line circle overlap.
			yIn := interval.In{0.5 - t, 0.5 + t}
			pic = pic.Append(DepictLineSeg(lineFr, seg, intersection(p.Y, yIn)))
		}
	}
	{ // Horizontal lines.
		lineFr.O.Z = spec.ZX
		seg := maf.LineSeg{
			P1: maf.Vec{X: -spec.GX / 2},
			P2: maf.Vec{X: spec.GX / 2},
		}
		for ; y < y2; y += spec.CY {
			if y <= -spec.R {
				continue
			}
			if y >= spec.R {
				break
			}
			seg.P1.Y, seg.P2.Y = y, y
			chord := 2 * math.Sqrt(spec.R*spec.R-y*y)
			if chord >= spec.GX {
				pic = pic.Append(DepictLineSeg(lineFr, seg, p.X))
				continue
			}
			t := (chord / spec.GX) / 2
			// The X-interval of the line circle overlap.
			xIn := interval.In{0.5 - t, 0.5 + t}
			pic = pic.Append(DepictLineSeg(lineFr, seg, intersection(p.X, xIn)))
		}
	}

	return pic
}

// Intersection of the interval and the portions.
func intersection(vp []mech.Portion, in interval.In) []mech.Portion {
	var res []mech.Portion
	for _, p := range vp {
		if overlap, ok := p.Intersection(in); ok {
			res = append(res, overlap)
		}
	}
	return res
}
