package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewLineSeg constructs a new line segment which is initially oriented with a
// standard frame.
func NewLineSeg(seg maf.LineSeg) *LineSeg {
	return &LineSeg{depictioner.Figure{Orientation: maf.StdFrame}, seg}
}

// A LineSeg is a wireframe line segment figure.
type LineSeg struct {
	depictioner.Figure

	maf.LineSeg
}

// Depict the given portions/(parameter intervals) on the line segment,
// overwriting the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and they are
// unitless proportions of the line segment's length in the range 0..1.
//
// The portions' quality is the literal number of quadratic Beziers to use.
func (fig *LineSeg) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictLineSeg(fig.Orientation, fig.LineSeg, vp)
}

// DepictLineSeg depicts the given line segment portions/(parameter intervals)
// on a line segment.
func DepictLineSeg(
	fr maf.Frame,
	seg maf.LineSeg,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = LineQual
		}
		pic = pic.Append(depictioner.DepictLine(p, fr, seg))
	}
	return pic
}
