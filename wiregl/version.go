package wiregl

// If you change the version don't forget to change the imports:
// "github.com/go-gl/gl/v4.5-core/gl"
const (
	contextVersionMajor = 4
	contextVersionMinor = 5
)
