#version 450

// Image dimensions in units of pixels.
uniform float full_h;
uniform float full_w;
uniform float small_h;
uniform float small_w;

// Image positions relative to the bottom left corner of the full image in units
// of pixels.
uniform float small_bottom;
uniform float small_left;

out vec2 v_full_uv;
out vec2 v_small_uv;

struct QuadCorner
{
  vec2 pos;
  vec2 full_uv;
};

// Cover the whole view with a quad (two triangles). The full size image covers
// the quad.
const QuadCorner quad_corners[] = QuadCorner[]
(
  QuadCorner(vec2(-1.0,  1.0), vec2(0.0,  1.0)),
  QuadCorner(vec2(-1.0, -1.0), vec2(0.0,  0.0)),
  QuadCorner(vec2( 1.0,  1.0), vec2(1.0,  1.0)),
  QuadCorner(vec2( 1.0, -1.0), vec2(1.0,  0.0))
);
 
void main()
{
  gl_Position = vec4(quad_corners[gl_VertexID].pos, 0.0, 1.0);
  v_full_uv = quad_corners[gl_VertexID].full_uv;
  vec2 pos = quad_corners[gl_VertexID].pos;
  v_small_uv = vec2(
    (((pos.x - (-1.0))/2.0)*full_w - small_left)/small_w,
    (((pos.y - (-1.0))/2.0)*full_h - small_bottom)/small_h
  );
}
