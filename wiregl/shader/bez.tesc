#version 450

const float ndc_height = 2; // The height in NDC is from -1 to 1, i.e. 2.
const vec3 zero = vec3(0.0, 0.0, 0.0);

uniform float density;
uniform mat4 eye_to_clip;
uniform uint min_divs;

layout(vertices = 3) out;

in vec3 v_colour[];
in float v_thickness[];
out vec3 tc_colour[];
out float tc_thickness[];
patch out float tc_is_linear;
patch out float tc_is_sharp;
patch out float tc_is_small;

void main() {
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tc_colour[gl_InvocationID] = v_colour[gl_InvocationID];
	tc_thickness[gl_InvocationID] = v_thickness[gl_InvocationID];

	if (0 == gl_InvocationID) {
		vec3 a = gl_in[0].gl_Position.xyz;
		vec3 b = gl_in[1].gl_Position.xyz;
		vec3 c = gl_in[2].gl_Position.xyz;
		vec3 ab = b - a;
		vec3 bc = c - b;

		vec4 a_clip = eye_to_clip * gl_in[0].gl_Position;
		vec4 b_clip = eye_to_clip * gl_in[1].gl_Position;
		vec4 c_clip = eye_to_clip * gl_in[2].gl_Position;
		vec3 a_ndc = a_clip.xyz / a_clip.w;
		vec3 b_ndc = b_clip.xyz / b_clip.w;
		vec3 c_ndc = c_clip.xyz / c_clip.w;

		// Pre-clip those fully to one side of some NDC cube faces.
		//
		// A full triangle cube intersection test is expensive hence the simple
		// tests which don't catch all cases.
		//
		// To get half the thickness: since the thickness is a proportion 0..1
		// of the NDC's height of 2, it is multiplied by 2, then half it for
		// half the thickness.
		float half_thickness_ndc = max(v_thickness[0], max(v_thickness[1], v_thickness[2]));
		float ub = 1 + half_thickness_ndc; // Upper bound in NDC.
		float lb = -ub; // Lower bound in NDC.
		uint tessLevelOuter0 = 
			((a_ndc.x < lb && b_ndc.x < lb && c_ndc.x < lb) ||
			 (a_ndc.y < lb && b_ndc.y < lb && c_ndc.y < lb) ||
			 (a_ndc.z < 0 && b_ndc.z < 0 && c_ndc.z < 0) || // z == 0 => Far clipping plane.
			 (a_ndc.x > ub && b_ndc.x > ub && c_ndc.x > ub) ||
			 (a_ndc.y > ub && b_ndc.y > ub && c_ndc.y > ub) ||
			 (a_ndc.z > 1 && b_ndc.z > 1 && c_ndc.z > 1)) ? // z == 1 => Near clipping plane.
			0 : 1;

		vec2 ab_ndc = b_ndc.xy - a_ndc.xy;
		vec2 bc_ndc = c_ndc.xy - b_ndc.xy;
		vec2 ab_mid_ndc = a_ndc.xy + ab_ndc/2;
		vec2 bc_mid_ndc = b_ndc.xy + bc_ndc/2;

		//
		//                                   ->   
		//                                   AB  ' P
		//                                      '
		//                                     '            
		//        B  _ _ _ _ _ _ C     =>   B '_ _ _ _ _ C  
		//         /                         /      ->
		//        /                         /       BC
		//       /                         /
		//      A                         A
		//
		// Angle PBC is small, and cos(PBC) is close to 1, when the control
		// points ABC are close to collinear.
		float cosPBC = dot(normalize(ab), normalize(bc));

		// As angle PBC increases, and cos(PBC) decreases, and the curvature is
		// increasing.
		
		// Record whether the control points are essentially collinear.
		// cosPBC ~= 0.9994 ~= cos(2 degrees) ==> angle ABC ~= 178 degrees.
		tc_is_linear = (cosPBC > 0.9994) ? 1.0 : 0.0;

		// Record whether the curve has a somewhat sharpish turn to it.
		// cosPBC = 0.5 = cos(60 degrees) ==> angle ABC is 120 degrees.
		tc_is_sharp = (cosPBC < 0.5) ? 1.0 : 0.0;

		// Increase the density of high curvature curves.
		// cosPBC = 0.0 = cos(90 degrees) ==> angle ABC is 90 degrees.
		float density_amplifier = cosPBC < 0 ? 8 : (cosPBC < 0.5 ? 4 : 1);

		// Roughly approximate the arc length in NDC screen space. Start at a,
		// proceed to the midpoint of ab, then to the midpoint of bc, and then
		// to c.
		float l = 0.5*(length(ab_ndc) + length(bc_ndc)) + length(bc_mid_ndc - ab_mid_ndc);
		tc_is_small = (l < 0.1) ? 1.0 : 0.0;
		uint divs = uint(round(density * density_amplifier * l / ndc_height));

		gl_TessLevelOuter[0] = tessLevelOuter0;
		gl_TessLevelOuter[1] = max(min_divs, divs);
	}
}