#version 450

layout(vertices = 3) out;

in vec3 v_colour[];
in float v_thickness[];
out vec3 tc_colour[];
out float tc_thickness[];

void main() {
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tc_colour[gl_InvocationID] = v_colour[gl_InvocationID];
	tc_thickness[gl_InvocationID] = v_thickness[gl_InvocationID];

	if (0 == gl_InvocationID) {
		gl_TessLevelOuter[0] = 1;
		gl_TessLevelOuter[1] = 1;
	}
}