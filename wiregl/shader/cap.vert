#version 450

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_colour;
layout(location = 2) in float a_thickness;
layout(location = 3) in mat4x3 a_world_to_eye;

out vec3 v_colour;
out float v_thickness;

void main() {
	gl_Position = vec4(a_world_to_eye * vec4(a_position, 1.0), 1.0);
	
	// Reverse the gamma encoding of the sRGB colour components giving linear
	// components.
	//
	// https://www.w3.org/TR/css-color-4/#color-conversion-code
	v_colour.r = a_colour.r < 0.04045 ? a_colour.r / 12.92 : 
		pow((a_colour.r + 0.055) / 1.055, 2.4);
	v_colour.g = a_colour.g < 0.04045 ? a_colour.g / 12.92 : 
		pow((a_colour.g + 0.055) / 1.055, 2.4);
	v_colour.b = a_colour.b < 0.04045 ? a_colour.b / 12.92 : 
		pow((a_colour.b + 0.055) / 1.055, 2.4);

	v_thickness = clamp(a_thickness, 0, 1);
}
