package shader

import (
	_ "embed"
)

// Shaders to draw the quadratic Bezier curves.
var (
	BezVert = bezVert + "\x00"
	//go:embed bez.vert
	bezVert string

	BezTesc = bezTesc + "\x00"
	//go:embed bez.tesc
	bezTesc string

	BezTese = bezTese + "\x00"
	//go:embed bez.tese
	bezTese string

	BezGeom = bezGeom + "\x00"
	//go:embed bez.geom
	bezGeom string

	BezFrag = bezFrag + "\x00"
	//go:embed bez.frag
	bezFrag string
)

// Shaders to draw the quadratic Bezier curves' line caps.
var (
	CapVert = capVert + "\x00"
	//go:embed cap.vert
	capVert string

	CapTesc = capTesc + "\x00"
	//go:embed cap.tesc
	capTesc string

	CapTese = capTese + "\x00"
	//go:embed cap.tese
	capTese string

	CapGeom = capGeom + "\x00"
	//go:embed cap.geom
	capGeom string

	CapFrag = capFrag + "\x00"
	//go:embed cap.frag
	capFrag string
)

// Shaders to stitch images together.
var (
	StitchVert = stitchVert + "\x00"
	//go:embed stitch.vert
	stitchVert string

	StitchFrag = stitchFrag + "\x00"
	//go:embed stitch.frag
	stitchFrag string
)

// Shaders to convert images from linear to gamma form.
var (
	GammaVert = gammaVert + "\x00"
	//go:embed gamma.vert
	gammaVert string

	GammaFrag = gammaFrag + "\x00"
	//go:embed gamma.frag
	gammaFrag string
)
