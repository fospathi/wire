#version 450

layout(binding=0) uniform sampler2D linear;

in vec2 v_uv;

out vec4 f_colour;

void main() {
    vec4 lin = texture(linear, v_uv);

    // Go from linear encoding back to gamma encoding.
    //
    // https://www.w3.org/TR/css-color-4/#color-conversion-code
    f_colour = vec4(
        lin.r <= 0.0031308 ? lin.r * 12.92 : 1.055 * pow(lin.r,  1/2.4) - 0.055,
        lin.g <= 0.0031308 ? lin.g * 12.92 : 1.055 * pow(lin.g,  1/2.4) - 0.055,
        lin.b <= 0.0031308 ? lin.b * 12.92 : 1.055 * pow(lin.b,  1/2.4) - 0.055,
        lin.a
    );
}