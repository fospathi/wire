# GLSL shaders for wiregl

The GLSL shaders in this directory, which implement programmable stages of the OpenGL rendering pipeline, can be linked together into GLSL program objects.

There are various program objects for different tasks:

* bez: draws quadratic Bezier curves;
* cap: draws semicircles for the opening and closing end caps (line caps) of the quadratic Bezier curves that use them;
* stitch: blends images together;
* gamma: converts a linear encoded image to a gamma encoded image.

## Variable naming

In the shader source code some of the `in`/`out` variables use a naming convention such that the prefix indicates the origin of the variable:

* `a_ ` - app
* `v_ ` - vertex shader
* `tc_` - tessellation control shader
* `te_` - tessellation evaluation shader
* `g_ ` - geometry shader
* `f_ ` - fragment shader
