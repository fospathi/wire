#version 450

const vec3 negative_z = vec3(0.0, 0.0, -1.0);

uniform uint debug;
uniform mat4 eye_to_clip;

layout(isolines, equal_spacing) in;

in vec3 tc_colour[];
in float tc_thickness[];
patch in float tc_is_linear;
patch in float tc_is_sharp;
patch in float tc_is_small;
out vec4 te_colour;
out float te_thickness;
out float te_is_linear;
out float te_is_sharp;
out float te_is_small;
out vec2 cap_dir; // Screen-parallel direction of a line's line-cap at this tessCoord.
out vec3 position;

vec3 a = gl_in[0].gl_Position.xyz;
vec3 b = gl_in[1].gl_Position.xyz;
vec3 c = gl_in[2].gl_Position.xyz;
vec3 ab = b - a;
vec3 bc = c - b;

vec3 bezier_position(float t) {
	float wa = (1.0-t) * (1.0-t);
	float wb = 2.0 * (1.0-t) * t;
	float wc = t * t;
	return wa*a + wb*b + wc*c;
}

vec3 bezier_tangent(float t) {
	vec3 p1 = a + (ab * t);
	vec3 p2 = b + (bc * t);
	return normalize(p2 - p1);
}

void main() {
	te_is_linear = tc_is_linear;
	te_is_sharp = tc_is_sharp;
	te_is_small = tc_is_small;

	float t = gl_TessCoord.x;
	position = bezier_position(t);
	// Shorten the tangent to minimize direction distortion when going to NDC.
	vec3 tangent = bezier_tangent(t) * 0.0001;

	vec4 p_clip = eye_to_clip * vec4(position, 1.0);
	vec3 p_ndc = p_clip.xyz / p_clip.w;

	vec4 tan_clip = eye_to_clip * vec4(position + tangent, 1.0);
	vec3 tan_ndc = tan_clip.xyz / tan_clip.w;
	vec3 tan_dir = normalize(tan_ndc - p_ndc);

	cap_dir = normalize(cross(negative_z, tan_dir).xy);

	gl_Position = p_clip / p_clip.w; // Prematurely convert from clip space to NDC.
	te_colour = (0 == debug) ? vec4(mix(tc_colour[0], tc_colour[2], t), 1.0) : vec4(0.0, 1.0, 0.0, 1.0);
	te_thickness = mix(tc_thickness[0], tc_thickness[2], t);
}