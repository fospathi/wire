#version 450

out vec2 v_uv;

struct QuadCorner
{
  vec2 pos;
  vec2 uv;
};

// Cover the whole view with a quad (two triangles). The linear encoded image
// covers the quad.
const QuadCorner quad_corners[] = QuadCorner[]
(
  QuadCorner(vec2(-1.0,  1.0), vec2(0.0,  1.0)),
  QuadCorner(vec2(-1.0, -1.0), vec2(0.0,  0.0)),
  QuadCorner(vec2( 1.0,  1.0), vec2(1.0,  1.0)),
  QuadCorner(vec2( 1.0, -1.0), vec2(1.0,  0.0))
);
 
void main()
{
  gl_Position = vec4(quad_corners[gl_VertexID].pos, 0.0, 1.0);
  v_uv = quad_corners[gl_VertexID].uv;
}