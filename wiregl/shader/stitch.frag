#version 450

uniform float small_alpha;

layout(binding=0) uniform sampler2D full;
layout(binding=1) uniform sampler2D small;

in vec2 v_full_uv;
in vec2 v_small_uv;

out vec4 f_colour;

void main() {
    vec4 src = texture(small, v_small_uv);
    vec4 dst = texture(full, v_full_uv);

    // Reverse the gamma encoding of the sRGB colour components giving linear
	// components.
	//
	// https://www.w3.org/TR/css-color-4/#color-conversion-code
    src.r = src.r < 0.04045 ? src.r / 12.92 : pow((src.r + 0.055) / 1.055, 2.4);
	src.g = src.g < 0.04045 ? src.g / 12.92 : pow((src.g + 0.055) / 1.055, 2.4);
	src.b = src.b < 0.04045 ? src.b / 12.92 : pow((src.b + 0.055) / 1.055, 2.4);
	dst.r = dst.r < 0.04045 ? dst.r / 12.92 : pow((dst.r + 0.055) / 1.055, 2.4);
	dst.g = dst.g < 0.04045 ? dst.g / 12.92 : pow((dst.g + 0.055) / 1.055, 2.4);
	dst.b = dst.b < 0.04045 ? dst.b / 12.92 : pow((dst.b + 0.055) / 1.055, 2.4);


    // Note that this step of applying a unifrom alpha scaling to the source
    // image alpha channels is not part of the specified Porter-Duff compositing
    // method.
    src.a = src.a * small_alpha;

    // Porter-Duff compositing.
    // https://apoorvaj.io/alpha-compositing-opengl-blending-and-premultiplied-alpha/
    float final_alpha = src.a + dst.a * (1.0 - src.a);
    vec4 lin = vec4(
        (src.rgb * src.a + dst.rgb * dst.a * (1.0 - src.a)) / final_alpha,
        final_alpha
    );


    // Go from linear encoding back to gamma encoding.
    //
    // https://www.w3.org/TR/css-color-4/#color-conversion-code
    f_colour = vec4(
        lin.r <= 0.0031308 ? lin.r * 12.92 : 1.055 * pow(lin.r,  1/2.4) - 0.055,
        lin.g <= 0.0031308 ? lin.g * 12.92 : 1.055 * pow(lin.g,  1/2.4) - 0.055,
        lin.b <= 0.0031308 ? lin.b * 12.92 : 1.055 * pow(lin.b,  1/2.4) - 0.055,
        lin.a
    );
}