#version 450

in vec4 g_colour;
out vec4 f_colour;

void main() {
	f_colour = g_colour;
}