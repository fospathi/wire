#version 450

// point_strip_len controls the quality of circles used to display degenerate
// line segments. Shall be an even number.
//
// Shall be reasonably small: it is desirable that 
//
//   (2 * point_strip_len * point_circles) < 256
//
// due to the implementation dependent geometry shader's vertices output limit.
const int point_strip_len = 14;
// point_circles is the number of circles to draw in place of a degenerate case
// line segment and they are spread out over the length of the line segment.
const int point_circles = 5;
const vec3 negative_z = vec3(0.0, 0.0, -1.0);

uniform vec2 point_strip[point_strip_len];

uniform float aspect;
// A threshold value above which a line segment degenerates into circles
// (points). The metric for degeneracy against which the point_edge is compared
// is the cosine of the angle between the line segment's direction and (in the
// persepctive camera case) the direction of a ray from the eye to a vertex of
// the line segment.
uniform float point_edge; 
uniform uint debug;
uniform uint is_ortho;

layout(lines) in;
layout(triangle_strip, max_vertices = point_strip_len * 2 * point_circles) out;

in vec4 te_colour[];
in float te_thickness[];
in float te_is_linear[]; // Straightness was calculated in normal 3D eye space coords.
in float te_is_sharp[]; // Sharpness was calculated in normal 3D eye space coords.
in float te_is_small[]; // Smallness was calculated in NDC space coords.
in vec2 cap_dir[];
in vec3 position[];
out vec4 g_colour;

// Since bool types are not allowed to be passed along directly floats are used
// instead and then converted to bool.
bool is_linear = te_is_linear[0] != 0;
bool is_sharp = te_is_sharp[0] != 0;
bool is_small = te_is_small[0] != 0;
bool is_persp = is_ortho == 0;

// The input thickness (0..1) is a proportion of the screen height which 
// is 1-(-1) = 2 in NDC:
//      NDC thickness = input thickness * 2
// half NDC thickness = input thickness * 2 / 2
//                    = input thickness
float left_h = te_thickness[0];
float right_h = te_thickness[1];

void draw_circle(in vec3 c, in float r) {
	for (int i = 0; i < point_strip_len; i++) {
		vec2 v = point_strip[i] * r;
		// Account for the horizontal distortion of relative vectors made in NDC
		// space due to a non-square aspect ratio.
		// https://gist.github.com/fospathi/8227bb28e79d27379051cad82364c40c
		v.x = v.x / aspect;

		gl_Position = vec4(c + vec3(-v.x, v.y, 0.0), 1.0);
		g_colour = te_colour[0];
		EmitVertex();
		gl_Position = vec4(c + vec3( v.x, v.y, 0.0), 1.0);
		g_colour = te_colour[0];
		EmitVertex();
	}
	EndPrimitive();
}

void main() {
	// Clip those line segments that potentially intersect the near side of the
	// NDC cube to prevent visual glitches caused by those pieces.
	//
	// In eye coordinates any positive Z values are behind the eye.
	if (position[0].z > 0 || position[1].z > 0) {
		return;
	}

	// NDC space coordinates.
	vec3 left = gl_in[0].gl_Position.xyz;
	vec3 right = gl_in[1].gl_Position.xyz;
	vec3 delta = right - left;

	// Direction of the line in eye space coordinates.
	vec3 line_dir = normalize(position[1] - position[0]);
	vec3 pos_dir = normalize(position[0]);
	vec3 ray_dir = (0 == is_ortho) ? pos_dir : negative_z;
	float cos_line_ray = abs(dot(line_dir, ray_dir));

	// Line segments of significant face-on length should not degenerate into
	// circles.
	float actual_point_edge = (length(delta.xy) > 0.01) ? 1.0 :
		point_edge;

	// Linear Beziers don't contain corners and so are displayed with circles
	// only when almost head on.
 	actual_point_edge = is_linear ? 0.9995 : 
		actual_point_edge;

	// Sharp corners suffer more from blockiness and so are displayed with
	// circles more readily over a wider angle range.
	actual_point_edge = is_sharp ? (actual_point_edge - 7.0 * (1.0 - actual_point_edge)) : 
		actual_point_edge;

	// Simultaneously small and sharp corners suffer much more from blockiness
	// and so are displayed with circles much more readily.
	actual_point_edge = (is_sharp && is_small) ? 0.0 :
		actual_point_edge;

	if (cos_line_ray > actual_point_edge) {
		for (int i = 0; i < point_circles; i++) {
			float t = float(i) / float(point_circles - 1);
			vec3 c = mix(left, right, t);
			float r = mix(left_h, right_h, t);
			draw_circle(c, r);
		}
		return;
	}

	vec3 left_perp = vec3(left_h * cap_dir[0], 0.0);
	vec3 right_perp = vec3(right_h * cap_dir[1], 0.0);
	left_perp.x = left_perp.x / aspect;
	right_perp.x = right_perp.x / aspect;

	gl_Position = vec4(left - left_perp, 1.0);
	g_colour = te_colour[0];
	EmitVertex();

	gl_Position = vec4(left + left_perp, 1.0);
	g_colour = te_colour[0];
	g_colour = (0 == debug) ? te_colour[0] : vec4(1.0, 0.0, 0.0, 1.0);
	EmitVertex();

	gl_Position = vec4(right - right_perp, 1.0);
	g_colour = (0 == debug) ? te_colour[1] : vec4(1.0, 0.0, 0.0, 1.0);
	EmitVertex();

	gl_Position = vec4(right + right_perp, 1.0);
	g_colour = (0 == debug) ? te_colour[1] : vec4(1.0, 0.0, 0.0, 1.0);
	EmitVertex();

	EndPrimitive();
}
