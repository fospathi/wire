#version 450

const int point_strip_len = 14;
const vec3 positive_z = vec3(0.0, 0.0, 1.0);

uniform float aspect;
uniform int cap_index;
uniform vec2 point_strip[point_strip_len];

layout(lines) in;
layout(triangle_strip, max_vertices = point_strip_len * 2) out;

in vec4 te_colour[];
in float te_thickness[];
in vec2 cap_dir[];
out vec4 g_colour;

void main() {
	// A line-cap is a semicircle.
	// 
	// Consider how the cap direction is generated in the tess evaluation shader
	//     cap_dir = normalize(cross(negative_z, tan_dir).xy);
	// Thus adopt these conventions to draw the semicircle on the correct side:
	// right cap => anti-clockwise with cap direction acting as negative Y-axis.
	// left cap => anti-clockwise with cap direction acting as positive Y-axis.
	float right_flip = (cap_index == 0) ? 1 : -1;
	vec2 y_dir = cap_dir[cap_index] * right_flip;

	// Due to the projection transformation, in NDC space the Z-axis direction 
	// is inverted compared to eye space.
	vec2 x_dir = cross(vec3(y_dir, 0.0), positive_z).xy;
	
	float r = te_thickness[cap_index];
	vec3 c = gl_in[cap_index].gl_Position.xyz;
	for (int i = 0; i < point_strip_len; i++) {
		vec2 v = point_strip[i] * r;

		vec2 p = v.y * y_dir;
		// Account for the horizontal distortion of relative vectors made in NDC 
		// space due to a non-square aspect ratio.
		// https://gist.github.com/fospathi/8227bb28e79d27379051cad82364c40c
		p.x = p.x / aspect;
		gl_Position = vec4(c + vec3(p, 0.0), 1.0);
		g_colour = te_colour[cap_index];
		EmitVertex();

		p = v.x * x_dir + v.y * y_dir;
		p.x = p.x / aspect;
		gl_Position = vec4(c + vec3(p, 0.0), 1.0);
		g_colour = te_colour[cap_index];
		EmitVertex();
	}
	EndPrimitive();
}
