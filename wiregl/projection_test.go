package wiregl_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

func TestNewPerspectiveProjection(t *testing.T) {

	// The camera is at the origin looking down the negative Z-axis. The near
	// clipping plane is 1 unit away and the far clipping plane is 1000 units
	// away.

	n, f := 1.0, 1000.0
	vf := wiregl.ViewingFrustum{
		Aspect: mech.WToHPixelsRatio,
		FOV:    maf.HalfPi,
		N:      n,
		F:      f,
	}
	proj := wiregl.NewPerspectiveProjection(vf)

	// Calculate some corners of the viewing frustum.

	tan := math.Tan(maf.QuartPi)

	nearTopLeft := maf.Vec{X: -tan * mech.WToHPixelsRatio, Y: tan, Z: -n}
	nearTopRight := maf.Vec{X: tan * mech.WToHPixelsRatio, Y: tan, Z: -n}
	farTopLeft :=
		maf.Vec{X: -tan * f * mech.WToHPixelsRatio, Y: tan * f, Z: -f}
	farBottomRight :=
		maf.Vec{X: tan * f * mech.WToHPixelsRatio, Y: -tan * f, Z: -f}

	// Test that the corners of the viewing frustum are mapped to the corners of
	// the NDC box. Note that this is a reversed-Z projection where near plane
	// positions map to Z = 1 and far plane positions map to Z = 0.

	want := maf.Vec{X: -1, Y: 1, Z: 1}
	got := toVec3(proj.Matrix.Transform(toVec4(nearTopLeft)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: 1, Y: 1, Z: 1}
	got = toVec3(proj.Matrix.Transform(toVec4(nearTopRight)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: -1, Y: 1, Z: 0}
	got = toVec3(proj.Matrix.Transform(toVec4(farTopLeft)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: 1, Y: -1, Z: 0}
	got = toVec3(proj.Matrix.Transform(toVec4(farBottomRight)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the inverse.

	want = nearTopLeft
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: -1, Y: 1, Z: 1})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = nearTopRight
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: 1, Y: 1, Z: 1})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = farTopLeft
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: -1, Y: 1, Z: 0})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = farBottomRight
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: 1, Y: -1, Z: 0})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewOrthographicProjection(t *testing.T) {

	// The camera is at the origin looking down the negative Z-axis. The near
	// clipping plane is 1 unit away and the far clipping plane is 1000 units
	// away.

	h := 10.0
	w := mech.WToHPixelsRatio * h
	n, f := 1.0, 1000.0
	vb := wiregl.ViewingBox{
		H: h,
		W: w,
		N: n,
		F: f,
	}
	proj := wiregl.NewOrthographicProjection(vb)

	// Some corners of the viewing box.
	nearTopLeft := maf.Vec{X: -w / 2, Y: h / 2, Z: -n}
	nearTopRight := maf.Vec{X: w / 2, Y: h / 2, Z: -n}
	farTopLeft := maf.Vec{X: -w / 2, Y: h / 2, Z: -f}
	farBottomRight := maf.Vec{X: w / 2, Y: -h / 2, Z: -f}

	// Test that the corners of the viewing box are mapped to the corners of the
	// NDC box. Note that this is a reversed-Z projection where near plane
	// positions map to Z = 1 and far plane positions map to Z = 0.

	want := maf.Vec{X: -1, Y: 1, Z: 1}
	got := toVec3(proj.Matrix.Transform(toVec4(nearTopLeft)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: 1, Y: 1, Z: 1}
	got = toVec3(proj.Matrix.Transform(toVec4(nearTopRight)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: -1, Y: 1, Z: 0}
	got = toVec3(proj.Matrix.Transform(toVec4(farTopLeft)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = maf.Vec{X: 1, Y: -1, Z: 0}
	got = toVec3(proj.Matrix.Transform(toVec4(farBottomRight)))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the inverse.

	want = nearTopLeft
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: -1, Y: 1, Z: 1})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = nearTopRight
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: 1, Y: 1, Z: 1})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = farTopLeft
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: -1, Y: 1, Z: 0})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = farBottomRight
	got = toVec3(proj.Inverse.Transform(toVec4(maf.Vec{X: 1, Y: -1, Z: 0})))
	if !maf.CompareVec(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

// Do a perspective divide.
func toVec3(v maf.Vec4) maf.Vec {
	return maf.Vec{X: v.X / v.W, Y: v.Y / v.W, Z: v.Z / v.W}
}

// Add a W=1 component.
func toVec4(v maf.Vec) maf.Vec4 {
	return maf.Vec4{X: v.X, Y: v.Y, Z: v.Z, W: 1}
}
