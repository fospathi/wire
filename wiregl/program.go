package wiregl

import (
	"fmt"
	"strings"

	"github.com/go-gl/gl/v4.5-core/gl"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/wiregl/shader"
)

// quadraticsProgram is a GLSL program that draws the quadratic Bezier curves of
// a Scene.
var quadraticsProgram = program{
	vertexShader:         shader.BezVert,
	tessControlShader:    shader.BezTesc,
	tessEvaluationShader: shader.BezTese,
	geometryShader:       shader.BezGeom,
	fragmentShader:       shader.BezFrag,
}

// capsProgram is a GLSL program that draws the semicircular line-caps for a
// scene's quadratic Bezier curves that use them.
var capsProgram = program{
	vertexShader:         shader.CapVert,
	tessControlShader:    shader.CapTesc,
	tessEvaluationShader: shader.CapTese,
	geometryShader:       shader.CapGeom,
	fragmentShader:       shader.CapFrag,
}

// stitchProgram is a GLSL program that blends a smaller image into a larger
// image.
var stitchProgram = program{
	vertexShader:   shader.StitchVert,
	fragmentShader: shader.StitchFrag,
}

// gammaProgram is a GLSL program that converts a linear encoded image to a
// gamma encoded image.
var gammaProgram = program{
	vertexShader:   shader.GammaVert,
	fragmentShader: shader.GammaFrag,
}

// A program is made up of the source code of OpenGL GLSL shaders which can be
// compiled and linked together into a program object.
//
// The non-empty string shaders shall be terminated by a null byte.
type program struct {
	vertexShader         string
	tessControlShader    string
	tessEvaluationShader string
	geometryShader       string
	fragmentShader       string
}

func (p program) compile() (compiledProg, error) {
	var (
		err  error
		prog = gl.CreateProgram()

		vertexShader         compiledShader
		tessControlShader    compiledShader
		tessEvaluationShader compiledShader
		geometryShader       compiledShader
		fragmentShader       compiledShader
	)

	if len(p.vertexShader) > 0 {
		if vertexShader, err =
			compileShader(p.vertexShader, gl.VERTEX_SHADER); err != nil {
			return 0, err
		}
		gl.AttachShader(prog, uint32(vertexShader))
	}
	if len(p.tessControlShader) > 0 {
		if tessControlShader, err =
			compileShader(p.tessControlShader, gl.TESS_CONTROL_SHADER); err != nil {
			return 0, err
		}
		gl.AttachShader(prog, uint32(tessControlShader))
	}
	if len(p.tessEvaluationShader) > 0 {
		if tessEvaluationShader, err =
			compileShader(p.tessEvaluationShader, gl.TESS_EVALUATION_SHADER); err != nil {
			return 0, err
		}
		gl.AttachShader(prog, uint32(tessEvaluationShader))
	}
	if len(p.geometryShader) > 0 {
		if geometryShader, err =
			compileShader(p.geometryShader, gl.GEOMETRY_SHADER); err != nil {
			return 0, err
		}
		gl.AttachShader(prog, uint32(geometryShader))
	}
	if len(p.fragmentShader) > 0 {
		if fragmentShader, err =
			compileShader(p.fragmentShader, gl.FRAGMENT_SHADER); err != nil {
			return 0, err
		}
		gl.AttachShader(prog, uint32(fragmentShader))
	}
	gl.LinkProgram(prog)
	return compiledProg(prog), nil
}

// A compiledProg is a compiled OpenGL GLSL program made up of compiled shaders
// linked together.
type compiledProg uint32

// Store the uniform values in the receiver program object.
func (cmp compiledProg) setUniforms(mu map[string]interface{}) {
	for name, val := range mu {
		cName := gl.Str(name + nullByte)
		loc := gl.GetUniformLocation(uint32(cmp), cName)
		switch v := val.(type) {
		case int32:
			gl.ProgramUniform1i(uint32(cmp), loc, v)
		case float32:
			gl.ProgramUniform1f(uint32(cmp), loc, v)
		case uint32:
			gl.ProgramUniform1ui(uint32(cmp), loc, v)
		case maf.OpenGLMat4:
			gl.ProgramUniformMatrix4fv(uint32(cmp), loc, 1, false, &v[0])
		case circleStrip:
			gl.ProgramUniform2fv(uint32(cmp), loc, v.verticesCount(), &v[0])
		}
	}
}

type compiledProgs struct {
	// Scene rendering programs.
	quadratics compiledProg
	closing    compiledProg
	opening    compiledProg

	// Image stitching program.
	stitch compiledProg

	// Gamma encoding program.
	gamma compiledProg
}

func compilePrograms() (compiledProgs, error) {
	qp, err := quadraticsProgram.compile()
	if err != nil {
		return compiledProgs{}, err
	}
	cp, err := capsProgram.compile()
	if err != nil {
		return compiledProgs{}, err
	}
	sp, err := stitchProgram.compile()
	if err != nil {
		return compiledProgs{}, err
	}
	gp, err := gammaProgram.compile()
	if err != nil {
		return compiledProgs{}, err
	}
	return compiledProgs{
		closing:    cp,
		opening:    cp,
		quadratics: qp,
		stitch:     sp,
		gamma:      gp,
	}, nil
}

// A compiledShader is a compiled OpenGL GLSL shader.
type compiledShader uint32

func compileShader(source string, shaderType uint32) (compiledShader, error) {
	shader := gl.CreateShader(shaderType)

	cSources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, cSources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if gl.FALSE == status {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)
		log := strings.Repeat(nullByte, int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))
		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return compiledShader(shader), nil
}

const (
	nullByte = "\x00"
)
