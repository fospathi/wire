package wiregl

import (
	"fmt"
	"unsafe"

	"github.com/go-gl/gl/v4.5-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
)

func initOpenGL() error {
	if err := gl.Init(); err != nil {
		return err
	}
	gl.ClipControl(gl.LOWER_LEFT, gl.ZERO_TO_ONE)
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.GREATER)
	return nil
}

func initGlfw(spec ContextSpec) (*glfw.Window, error) {
	if err := glfw.Init(); err != nil {
		return nil, err
	}

	glfw.WindowHint(glfw.ContextVersionMajor, contextVersionMajor)
	glfw.WindowHint(glfw.ContextVersionMinor, contextVersionMinor)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.Visible, glfw.False)
	if spec.Debug {
		glfw.WindowHint(glfw.OpenGLDebugContext, glfw.True)
	}

	window, err := glfw.CreateWindow(spec.W, spec.H, "", nil, nil)
	if err != nil {
		glfw.Terminate()
		return nil, err
	}

	return window, nil
}

func getVertexArrayBinding() uint32 {
	var vao int32
	gl.GetIntegerv(gl.VERTEX_ARRAY_BINDING, &vao)
	return uint32(vao)
}

func getBufferSize(vbo uint32) int {
	var s int32
	gl.GetNamedBufferParameteriv(vbo, gl.BUFFER_SIZE, &s)
	return int(s)
}

func loadBuffer(buffer uint32, newSize int, p unsafe.Pointer) {
	if newSize > getBufferSize(buffer) {
		gl.NamedBufferData(buffer, newSize, p, gl.STREAM_DRAW)
	} else {
		gl.NamedBufferSubData(buffer, 0, newSize, p)
	}
}

func newFullSizeBuffer(ims ImageSizes) (colourBuffer, error) {
	return newColourBuffer(ims.w, ims.h, ims.internalFormat())
}

func newSmallSizeBuffer(ims ImageSizes) (colourBuffer, error) {
	return newColourBuffer(ims.sW, ims.sH, ims.internalFormat())
}

func newColourBuffer(w, h int32, internalFormat uint32) (colourBuffer, error) {
	var (
		colour uint32
		fbo    uint32
		status uint32
	)

	gl.CreateTextures(gl.TEXTURE_2D, 1, &colour)
	gl.TextureStorage2D(colour, 1, internalFormat, w, h)
	gl.CreateFramebuffers(1, &fbo)
	gl.NamedFramebufferTexture(fbo, gl.COLOR_ATTACHMENT0, colour, 0)
	status = gl.CheckNamedFramebufferStatus(fbo, gl.FRAMEBUFFER)
	if status != gl.FRAMEBUFFER_COMPLETE {
		return colourBuffer{},
			fmt.Errorf("framebuffer not complete: %v", status)
	}
	return colourBuffer{
		colour: colour,
		fbo:    fbo,
	}, nil
}

// An FBO and its colour texture attachment.
type colourBuffer struct {
	colour uint32
	fbo    uint32
}

func isAllZeros[T int | uint8 | uint16](vt []T) bool {
	for _, v := range vt {
		if v != 0 {
			return false
		}
	}
	return true
}
