package wiregl_test

import (
	"slices"
	"testing"
	"unsafe"

	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/wiregl"
)

func TestPixels(t *testing.T) {

	// Test a partial pixels with just a full size image.

	pix := wiregl.Pixels{
		Full: wiregl.PixelData{
			Height: 3,
			Width:  3,
			Data8: []uint8{
				1, 2, 3, 4,
				5, 6, 7, 8,
				9, 10, 11, 12,
				13, 14, 15, 16,
				17, 18, 19, 20,
				21, 22, 23, 24,
				25, 26, 27, 28,
				29, 30, 31, 32,
				33, 34, 35, 36,
			},
		},
	}

	bp := pix.ToBinaryPixels()
	l := len(bp)
	pix = bp.ToPixels()

	w, h := pix.Full.Width, pix.Full.Height
	{
		want1, want2, want3 := 3, 3, (3*3*4)+8
		got1, got2, got3 := w, h, l
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	// Test a pixels with both a full size image and a smaller size image.

	pix = wiregl.Pixels{
		Full: wiregl.PixelData{
			Height: 3,
			Width:  3,
			Data8: []uint8{
				1, 2, 3, 4,
				5, 6, 7, 8,
				9, 10, 11, 12,
				13, 14, 15, 16,
				17, 18, 19, 20,
				21, 22, 23, 24,
				25, 26, 27, 28,
				29, 30, 31, 32,
				33, 34, 35, 36,
			},
		},
		Small: wiregl.PixelData{
			Height: 1,
			Width:  1,
			Data8: []uint8{
				1, 2, 3, 4,
			},
		},
	}

	bp = pix.ToBinaryPixels()
	l = len(bp)
	pix = bp.ToPixels()
	{
		want := (3 * 3 * 4) + 8 + (1 * 4) + 8
		got := l
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		w, h = pix.Full.Width, pix.Full.Height

		want1, want2 := 3, 3
		got1, got2 := w, h
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}

		w, h = pix.Small.Width, pix.Small.Height

		want1, want2 = 1, 1
		got1, got2 = w, h
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}
}

func TestPixelData_AsBinary(t *testing.T) {

	// Test the width and height encoding.

	pix := wiregl.PixelData{
		Height: mech.HPixels,
		Width:  mech.WPixels,
		Data8:  []uint8{0, 1, 2, 3, 4, 5},
	}

	bp := pix.AsBinary()
	w, h := bp.WidthHeight()
	{
		want1, want2 := mech.WPixels, mech.HPixels
		got1, got2 := w, h
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%v\nGot1:\n%v\nWant2:\n%v\nGot2:\n%v\n",
				want1, got1, want2, got2)
		}
	}

	// Test encoded slice lengths.

	pix = wiregl.PixelData{
		Height: 1,
		Width:  6,
		Data8: []uint8{
			1, 2, 3, 4,
			5, 6, 7, 8,
			9, 10, 11, 12,
			13, 14, 15, 16,
			17, 18, 19, 20,
			21, 22, 23, 24,
		},
	}

	bp = pix.AsBinary()
	{
		want1, want2 := 24+8, true // 8 is the length of the width/height appendage.
		got1, got2 := len(bp), cap(bp) >= 24+8
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%v\nGot1:\n%v\nWant2:\n%v\nGot2:\n%v\n",
				want1, got1, want2, got2)
		}
	}

	pix2 := bp.ToPixelData()
	{
		want1, want2, want3 := 1, 6, true
		got1, got2, got3 := pix2.Height, pix2.Width,
			slices.Equal(pix.Data8, pix2.Data8)

		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	pix = wiregl.PixelData{
		Height: 1,
		Width:  6,
		Data16: []uint16{
			1, 2, 3, 4,
			5, 6, 7, 8,
			9, 10, 11, 12,
			13, 14, 15, 16,
			17, 18, 19, 20,
			21, 22, 23, 24,
		},
	}

	bp = pix.AsBinary()
	{
		want1, want2 := 48+8, true // 8 is the length of the width/height appendage.
		got1, got2 := len(bp), cap(bp) >= 48+8
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%v\nGot1:\n%v\nWant2:\n%v\nGot2:\n%v\n",
				want1, got1, want2, got2)
		}
	}

	// Test going back to pixels format.

	pix2 = bp.ToPixelData()
	{
		want1, want2, want3 := 1, 6, true
		got1, got2, got3 := pix2.Height, pix2.Width,
			slices.Equal(pix.Data16, pix2.Data16)

		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\n"+
				"Want2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}
}

func TestPixelData_ToUint8WithFlip(t *testing.T) {
	px := wiregl.PixelData{
		Data16: []uint16{
			1, 2, 3, 4, 5, 6, 7, 8,
			9, 10, 11, 12, 13, 14, 15, 16,
			259, 18, 19, 20, 21, 22, 23, 24,
		},
		Height: 3,
		Width:  2,
	}

	{
		want := make([]uint8, len(px.Data16)*2)
		for i, v := range []uint16{
			259, 18, 19, 20, 21, 22, 23, 24,
			9, 10, 11, 12, 13, 14, 15, 16,
			1, 2, 3, 4, 5, 6, 7, 8,
		} {
			// Has double the needed data-capacity to allow indexing of all
			// uint8 elements without giving an index bounds error.
			cpy := make([]uint16, 2)
			cpy[0] = v
			p := (*[]uint8)(unsafe.Pointer(&cpy))
			want[i*2] = (*p)[0]
			want[i*2+1] = (*p)[1]
		}
		got := px.ToUint8WithFlip()
		if !slices.Equal(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	px = wiregl.PixelData{
		Data16: []uint16{
			1, 2, 3, 4, 5, 6, 7, 8,
			9, 10, 11, 12, 13, 14, 15, 16,
			259, 18, 19, 20, 21, 22, 23, 24,
			25, 26, 27, 28, 29, 30, 31, 32,
		},
		Height: 4,
		Width:  2,
	}

	{
		want := make([]uint8, len(px.Data16)*2)
		for i, v := range []uint16{
			25, 26, 27, 28, 29, 30, 31, 32,
			259, 18, 19, 20, 21, 22, 23, 24,
			9, 10, 11, 12, 13, 14, 15, 16,
			1, 2, 3, 4, 5, 6, 7, 8,
		} {
			// Has double the needed data-capacity to allow indexing of all
			// uint8 elements without giving an index bounds error.
			cpy := make([]uint16, 2)
			cpy[0] = v
			p := (*[]uint8)(unsafe.Pointer(&cpy))
			want[i*2] = (*p)[0]
			want[i*2+1] = (*p)[1]
		}
		got := px.ToUint8WithFlip()
		if !slices.Equal(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
