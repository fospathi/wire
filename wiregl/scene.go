package wiregl

import (
	"fmt"
	"sync"

	"github.com/go-gl/gl/v4.5-core/gl"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
)

// A Scene is a collection of styled quadratic Beziers and any other values
// necessary for the GLSL program to rasterise the scene.
type Scene struct {
	mech.Depiction
	Veneer mech.Depiction // The veneer appears above the main view onto the world.

	// The projection matrix which projects a position given in eye coordinates
	// to clip coordinates.
	EyeToClip       maf.OpenGLMat4
	VeneerEyeToClip maf.OpenGLMat4

	// Controls the number of divisions each Bezier curve is divided into
	// depending on how much screen space it occupies.
	//
	// The density is approximately how many divisions a single Bezier curve
	// would be divided into if it was a straight line that occupied the
	// screen's full height. Thus the 'units' of this density value might be
	// thought of as 'divisions per full-vertical-screen'.
	//
	// A higher value will result in possibly higher quality at the cost of
	// worse performance.
	Density uint32

	// ClearAlpha is the buffer clear colour opacity.
	ClearAlpha float64
	// ClearColour is the buffer clear colour.
	ClearColour mech.Colour

	// Small is true if the small size version of the scene should be generated
	// in addition to the full size version.
	Small bool

	// A collection of optional values which effect the quality of the
	// rasterisation that most users can safely ignore.
	Minutiae SceneMinutiae
}

func (s Scene) isOrthographic() bool {
	return IsOrthographic(s.EyeToClip)
}

func (s Scene) isOrthographicVeneer() bool {
	return IsOrthographic(s.VeneerEyeToClip)
}

func setDefaultsAndPrivates(s *Scene) {
	if s.Density < 1 {
		s.Density = DefaultDensity
	}
	if s.Minutiae.MinDivs < 1 {
		s.Minutiae.MinDivs = minutiaeDefaults.MinDivs
	}
	if s.isOrthographic() {
		if s.Minutiae.OrthoPointEdge == 0 {
			s.Minutiae.pointEdge = minutiaeDefaults.OrthoPointEdge
		} else {
			s.Minutiae.pointEdge = s.Minutiae.OrthoPointEdge
		}
	} else {
		if s.Minutiae.PerspPointEdge == 0 {
			s.Minutiae.pointEdge = minutiaeDefaults.PerspPointEdge
		} else {
			s.Minutiae.pointEdge = s.Minutiae.PerspPointEdge
		}
	}
	if s.isOrthographicVeneer() {
		if s.Minutiae.VeneerOrthoPointEdge == 0 {
			s.Minutiae.veneerPointEdge = minutiaeDefaults.OrthoPointEdge
		} else {
			s.Minutiae.veneerPointEdge = s.Minutiae.OrthoPointEdge
		}
	} else {
		if s.Minutiae.VeneerPerspPointEdge == 0 {
			s.Minutiae.veneerPointEdge = minutiaeDefaults.PerspPointEdge
		} else {
			s.Minutiae.veneerPointEdge = s.Minutiae.PerspPointEdge
		}
	}
}

// A SceneMinutiae specifies the values of other configurable GLSL shader
// variables which effect the visual quality of the scene rasterisation.
//
// These values are optional and most users will not need to change any
// minutiae.
type SceneMinutiae struct {
	// Set as true/non-zero to enable alternate colours that enable the
	// individual triangles that make up a curve/line to be seen.
	Debug uint32

	// The minimum number of divisions each quadratic Bezier curve will be
	// divided into regardless of the density value.
	MinDivs uint32

	// The pointEdge threshold determines when a line segment has degenerated
	// into a point.
	//
	// A line is drawn with a circle(s) if it degenerates into a point.
	pointEdge      float32
	OrthoPointEdge float32
	PerspPointEdge float32

	veneerPointEdge      float32
	VeneerOrthoPointEdge float32
	VeneerPerspPointEdge float32
}

const (
	// DefaultDensity is a sensible default value for a scene's density
	// variable.
	DefaultDensity uint32 = 125
)

var minutiaeDefaults = SceneMinutiae{
	MinDivs: 5,

	OrthoPointEdge: 0.995,
	PerspPointEdge: 0.97,
}

type scenePart struct {
	// Uniforms.
	mcu map[string]interface{}
	mou map[string]interface{}
	mqu map[string]interface{}

	// Indices (not in the literal Go sense!) of vertices/(Bezier control
	// points) in the vertex attributes array.
	vci []uint32
	voi []uint32
	vqi []uint32

	// Vertex attributes.
	vva []float32
}

type scene struct {
	clearAlpha  float32
	clearColour mech.Colour
	small       bool
	sceneBuffers

	world  scenePart
	veneer scenePart
}

type sceneBuffers struct {
	ciVBO uint32 // Closing indices.
	oiVBO uint32 // Opening indices.
	qiVBO uint32 // Quadratic Bezier indices.
	vaVBO uint32
	vaVAO uint32

	multisampledFBO uint32

	// Non-multisampled linear encoded buffers.
	fullLinearColBuf  colourBuffer
	smallLinearColBuf colourBuffer
	// Non-multisampled gamma encoded buffers.
	fullColBuf  colourBuffer
	smallColBuf colourBuffer

	ImageSizes
}

// aspect is the width to height aspect ratio.
func newScene(s Scene, sb sceneBuffers, aspect float32) scene {
	setDefaultsAndPrivates(&s)
	var (
		isOrtho       uint32
		isOrthoVeneer uint32
	)
	if s.isOrthographic() {
		isOrtho = 1
	}
	if s.isOrthographicVeneer() {
		isOrthoVeneer = 1
	}
	return scene{
		clearAlpha:   float32(s.ClearAlpha),
		clearColour:  s.ClearColour,
		small:        s.Small,
		sceneBuffers: sb,

		world: scenePart{
			mcu: map[string]interface{}{
				"aspect":      aspect,
				"cap_index":   int32(1),
				"debug":       s.Minutiae.Debug,
				"eye_to_clip": s.EyeToClip,
				"point_strip": circStrip,
			},
			mou: map[string]interface{}{
				"aspect":      aspect,
				"cap_index":   int32(0),
				"debug":       s.Minutiae.Debug,
				"eye_to_clip": s.EyeToClip,
				"point_strip": circStrip,
			},
			mqu: map[string]interface{}{
				"aspect":      aspect,
				"debug":       s.Minutiae.Debug,
				"density":     float32(s.Density),
				"eye_to_clip": s.EyeToClip,
				"is_ortho":    isOrtho,
				"min_divs":    s.Minutiae.MinDivs,
				"point_edge":  s.Minutiae.pointEdge,
				"point_strip": circStrip,
			},
			vci: s.Closing,
			voi: s.Opening,
			vqi: s.Quadratics,
			vva: s.VertexAttributes,
		},

		veneer: scenePart{
			mcu: map[string]interface{}{
				"aspect":      aspect,
				"cap_index":   int32(1),
				"debug":       s.Minutiae.Debug,
				"eye_to_clip": s.VeneerEyeToClip,
				"point_strip": circStrip,
			},
			mou: map[string]interface{}{
				"aspect":      aspect,
				"cap_index":   int32(0),
				"debug":       s.Minutiae.Debug,
				"eye_to_clip": s.VeneerEyeToClip,
				"point_strip": circStrip,
			},
			mqu: map[string]interface{}{
				"aspect":      aspect,
				"debug":       s.Minutiae.Debug,
				"density":     float32(s.Density),
				"eye_to_clip": s.VeneerEyeToClip,
				"is_ortho":    isOrthoVeneer,
				"min_divs":    s.Minutiae.MinDivs,
				"point_edge":  s.Minutiae.pointEdge,
				"point_strip": circStrip,
			},
			vci: s.Veneer.Closing,
			voi: s.Veneer.Opening,
			vqi: s.Veneer.Quadratics,
			vva: s.Veneer.VertexAttributes,
		},
	}
}

func rasteriseScene(s scene, cp compiledProgs) Pixels {

	// A thing to be done during scene rasterisation.
	type drawTask struct {
		count int32                  // The count of vertex indices.
		mu    map[string]interface{} // Uniforms.
		prog  compiledProg
		vbo   uint32 // The buffer containing the vertex indices.
	}

	// Make a list of world draw tasks and send the indices to the GPU.
	var worldTasks = []drawTask{}
	for _, task := range []struct {
		p   compiledProg
		mu  map[string]interface{}
		vi  []uint32
		vbo uint32
	}{
		{cp.closing, s.world.mcu, s.world.vci, s.ciVBO},
		{cp.opening, s.world.mou, s.world.voi, s.oiVBO},
		{cp.quadratics, s.world.mqu, s.world.vqi, s.qiVBO},
	} {
		if count := int32(len(task.vi)); count > 0 {
			loadBuffer(task.vbo, bytesPerUInt32*int(count), gl.Ptr(task.vi))
			worldTasks = append(worldTasks, drawTask{
				count: count,
				mu:    task.mu,
				prog:  task.p,
				vbo:   task.vbo,
			})
		}
	}

	// Send the world vertex array to the GPU.
	loadBuffer(s.vaVBO, bytesPerFloat32*len(s.world.vva), gl.Ptr(s.world.vva))

	// Rasterise the world scene onto a clear buffer by executing the world draw
	// tasks.

	gl.BindFramebuffer(gl.FRAMEBUFFER, s.multisampledFBO)

	gl.ClearColor(
		s.clearColour[0], s.clearColour[1], s.clearColour[2], s.clearAlpha)
	gl.ClearDepth(0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	if s.vaVAO != getVertexArrayBinding() {
		gl.BindVertexArray(s.vaVAO)
	}
	gl.PatchParameteri(gl.PATCH_VERTICES, 3) // 3 vertices in a quadratic Bezier curve.

	for _, dt := range worldTasks {
		gl.VertexArrayElementBuffer(s.vaVAO, dt.vbo)
		dt.prog.setUniforms(dt.mu)
		gl.UseProgram(uint32(dt.prog))
		gl.DrawElements(gl.PATCHES, dt.count, gl.UNSIGNED_INT, gl.PtrOffset(0))
	}

	// Make a list of veneer draw tasks and send the indices to the GPU.
	var veneerTasks = []drawTask{}
	for _, task := range []struct {
		p   compiledProg
		mu  map[string]interface{}
		vi  []uint32
		vbo uint32
	}{
		{cp.closing, s.veneer.mcu, s.veneer.vci, s.ciVBO},
		{cp.opening, s.veneer.mou, s.veneer.voi, s.oiVBO},
		{cp.quadratics, s.veneer.mqu, s.veneer.vqi, s.qiVBO},
	} {
		if count := int32(len(task.vi)); count > 0 {
			loadBuffer(task.vbo, bytesPerUInt32*int(count), gl.Ptr(task.vi))
			veneerTasks = append(veneerTasks, drawTask{
				count: count,
				mu:    task.mu,
				prog:  task.p,
				vbo:   task.vbo,
			})
		}
	}

	// Send the veneer vertex array to the GPU.
	loadBuffer(s.vaVBO, bytesPerFloat32*len(s.veneer.vva), gl.Ptr(s.veneer.vva))

	// Rasterise the veneer scene over the world scene by executing the veneer
	// draw tasks.

	gl.ClearDepth(0)
	gl.Clear(gl.DEPTH_BUFFER_BIT)

	for _, dt := range veneerTasks {
		gl.VertexArrayElementBuffer(s.vaVAO, dt.vbo)
		dt.prog.setUniforms(dt.mu)
		gl.UseProgram(uint32(dt.prog))
		gl.DrawElements(gl.PATCHES, dt.count, gl.UNSIGNED_INT, gl.PtrOffset(0))
	}

	// Complete the anti-aliasing with a blit operation to a normal buffer.
	// Also, glReadPixels cant't read directly from a multisampled type buffer.

	gl.BlitNamedFramebuffer(s.multisampledFBO, s.fullLinearColBuf.fbo,
		0, 0, s.w, s.h, 0, 0, s.w, s.h, gl.COLOR_BUFFER_BIT, gl.NEAREST)
	if s.small {
		gl.BlitNamedFramebuffer(s.fullLinearColBuf.fbo, s.smallLinearColBuf.fbo,
			0, 0, s.w, s.h, 0, 0, s.sW, s.sH, gl.COLOR_BUFFER_BIT, gl.NEAREST)
	}

	// Convert from linear back to gamma encoded form now that the pixel
	// manipulations have been done in linear space.

	gl.BindTextureUnit(0, s.fullLinearColBuf.colour)
	gl.BindFramebuffer(gl.FRAMEBUFFER, s.fullColBuf.fbo)
	gl.UseProgram(uint32(cp.gamma))
	gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)
	if s.small {
		gl.BindTextureUnit(0, s.smallLinearColBuf.colour)
		gl.BindFramebuffer(gl.FRAMEBUFFER, s.smallColBuf.fbo)
		gl.Viewport(0, 0, s.sW, s.sH)
		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)
		gl.Viewport(0, 0, s.w, s.h)
	}

	// Read the rasterised scene's pixels.

	var (
		pix8   []uint8
		pix16  []uint16
		sPix8  []uint8
		sPix16 []uint16
	)

	gl.BindFramebuffer(gl.FRAMEBUFFER, s.fullColBuf.fbo)
	if s.DeepColour {
		pix16 = make([]uint16, 4*s.w*s.h)
		gl.ReadPixels(
			0, 0, s.w, s.h, gl.RGBA, gl.UNSIGNED_SHORT, gl.Ptr(&pix16[0]))
	} else {
		pix8 = make([]uint8, 4*s.w*s.h)
		gl.ReadPixels(
			0, 0, s.w, s.h, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&pix8[0]))
	}

	if s.small {
		gl.BindFramebuffer(gl.FRAMEBUFFER, s.smallColBuf.fbo)
		if s.DeepColour {
			sPix16 = make([]uint16, 4*s.sW*s.sH)
			gl.ReadPixels(
				0, 0, s.sW, s.sH, gl.RGBA, gl.UNSIGNED_SHORT, gl.Ptr(&sPix16[0]))
		} else {
			sPix8 = make([]uint8, 4*s.sW*s.sH)
			gl.ReadPixels(
				0, 0, s.sW, s.sH, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&sPix8[0]))
		}
	}

	return Pixels{
		Full: PixelData{
			Height: int(s.h),
			Width:  int(s.w),
			Data16: pix16,
			Data8:  pix8,
		},
		Small: PixelData{
			Height: int(s.sH),
			Width:  int(s.sW),
			Data16: sPix16,
			Data8:  sPix8,
		},
	}
}

func newSceneBuffers(spec ContextSpec) (sceneBuffers, error) {
	var (
		ciVBO uint32
		oiVBO uint32
		qiVBO uint32
		vaVBO uint32
		vaVAO uint32
	)
	gl.CreateBuffers(1, &ciVBO)
	gl.CreateBuffers(1, &oiVBO)
	gl.CreateBuffers(1, &qiVBO)
	gl.CreateBuffers(1, &vaVBO)
	gl.CreateVertexArrays(1, &vaVAO)
	gl.VertexArrayVertexBuffer(vaVAO, 0, vaVBO, 0, stride)
	enableAttributes(vaVAO)

	var (
		// Multisampled buffer.
		msColour uint32
		msDepth  uint32
		msFBO    uint32

		samples      = int32(spec.Samples)
		fullW, fullH = int32(spec.W), int32(spec.H)
	)

	gl.CreateTextures(gl.TEXTURE_2D_MULTISAMPLE, 1, &msColour)
	gl.TextureStorage2DMultisample(
		msColour, samples, spec.internalFormat(), fullW, fullH, true)
	gl.CreateTextures(gl.TEXTURE_2D_MULTISAMPLE, 1, &msDepth)
	gl.TextureStorage2DMultisample(
		msDepth, samples, gl.DEPTH_COMPONENT32F, fullW, fullH, true)
	gl.CreateFramebuffers(1, &msFBO)
	gl.NamedFramebufferTexture(msFBO, gl.COLOR_ATTACHMENT0, msColour, 0)
	gl.NamedFramebufferTexture(msFBO, gl.DEPTH_ATTACHMENT, msDepth, 0)
	status := gl.CheckNamedFramebufferStatus(msFBO, gl.FRAMEBUFFER)
	if status != gl.FRAMEBUFFER_COMPLETE {
		return sceneBuffers{},
			fmt.Errorf("multisampled framebuffer not complete: %v", status)
	}

	fullLinearColBuf, err := newFullSizeBuffer(spec.ImageSizes)
	if err != nil {
		return sceneBuffers{}, err
	}
	smallLinearColBuf, err := newSmallSizeBuffer(spec.ImageSizes)
	if err != nil {
		return sceneBuffers{}, err
	}

	fullColBuf, err := newFullSizeBuffer(spec.ImageSizes)
	if err != nil {
		return sceneBuffers{}, err
	}
	smallColBuf, err := newSmallSizeBuffer(spec.ImageSizes)
	if err != nil {
		return sceneBuffers{}, err
	}

	return sceneBuffers{
		ciVBO: ciVBO,
		oiVBO: oiVBO,
		qiVBO: qiVBO,
		vaVBO: vaVBO,
		vaVAO: vaVAO,

		fullLinearColBuf:  fullLinearColBuf,
		smallLinearColBuf: smallLinearColBuf,
		fullColBuf:        fullColBuf,
		smallColBuf:       smallColBuf,
		multisampledFBO:   msFBO,

		ImageSizes: spec.ImageSizes,
	}, nil
}

// NewDepictionBuilder constructs a new DepictionBuilder.
func NewDepictionBuilder() *DepictionBuilder {
	return &DepictionBuilder{}
}

// A DepictionBuilder helps build the depiction part of a complete scene from
// smaller depictions which are local to a frame.
type DepictionBuilder struct {
	pic      mech.Depiction
	complete bool
	m        sync.Mutex
}

// Add the given depiction to the depiction but with the given toEye
// transformation added to the vertices' attributes.
//
// The vertices in the given depiction shall not already include a toEye
// transformation attribute.
//
// Once the depiction builder is completed Add() is a no-op.
func (db *DepictionBuilder) Add(pic mech.Depiction, toEye maf.OpenGLMat4x3) {
	db.m.Lock()
	defer db.m.Unlock()

	if db.complete {
		return
	}

	const (
		destSpan = mech.Float32sNonEye + mech.Float32sEye
		srcSpan  = mech.Float32sNonEye
	)

	nDest := uint32(len(db.pic.VertexAttributes) / destSpan)
	nSrc := len(pic.VertexAttributes) / srcSpan

	// Shift vertex indices from their index in the depiction to their index in
	// the full scene.
	for i := 0; i < len(pic.Closing); i++ {
		db.pic.Closing = append(db.pic.Closing, pic.Closing[i]+nDest)
	}
	for i := 0; i < len(pic.Opening); i++ {
		db.pic.Opening = append(db.pic.Opening, pic.Opening[i]+nDest)
	}
	for i := 0; i < len(pic.Quadratics); i++ {
		db.pic.Quadratics = append(db.pic.Quadratics, pic.Quadratics[i]+nDest)
	}

	buf := make([]float32, destSpan)
	// Copy the vertex attributes over but include the toEye as well.
	for i := 0; i < nSrc; i++ {
		copy(buf, pic.VertexAttributes[i*srcSpan:(i+1)*srcSpan])
		for j := 0; j < len(toEye); j++ {
			buf[srcSpan+j] = toEye[j]
		}
		db.pic.VertexAttributes = append(db.pic.VertexAttributes, buf...)
	}
}

// Complete the depiction and return the depiction with the toEye
// transformations now part of each vertices' attributes.
func (db *DepictionBuilder) Complete() mech.Depiction {
	db.m.Lock()
	defer db.m.Unlock()

	db.complete = true
	return db.pic
}
