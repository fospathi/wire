package wiregl

import (
	"math"
	"sync"

	"gitlab.com/fospathi/wire/colour"
)

// CPUStitcher stitches a small size image into a full size image using multiple
// CPU cores.
type CPUStitcher struct {
	cores int
	m     sync.Mutex
	vc    []chan cpuStitch
	wg    *sync.WaitGroup
}

type cpuStitch struct {
	alpha       float64
	bottom      int
	left        int
	full, small PixelData
	wg          *sync.WaitGroup
}

// Stitch the small size image into the full size image.
func (s *CPUStitcher) Stitch(
	full, small PixelData, smallAlpha float64, bottom int, left int,
) {

	s.m.Lock()
	defer s.m.Unlock()

	const c = 4 // 4 channels in RGBA.

	var (
		cores = int(math.Min(float64(s.cores), float64(small.Height)))
	)
	s.wg.Add(cores)

	// Partition the small image into evenly sized clumps of rows to process
	// concurrently.

	var (
		// Number of image rows per core (except for the last core).
		n = int(float64(small.Height) / float64(cores))
		// The last core possibly gets more rows.
		r = small.Height % cores
	)

	for i := 0; i < cores; i++ {
		h := n
		if i == cores-1 {
			h += r
		}

		var (
			data8  []uint8
			data16 []uint16
		)

		start := (small.Width * i * n) * c
		l := small.Width * h * c
		if small.Data8 != nil {
			data8 = small.Data8[start : start+l]
		} else {
			data16 = small.Data16[start : start+l]
		}

		s.vc[i] <- cpuStitch{
			alpha:  smallAlpha,
			bottom: bottom + i*n,
			left:   left,
			full:   full,
			small: PixelData{
				Height: h,
				Width:  small.Width,
				Data8:  data8,
				Data16: data16,
			},
			wg: s.wg,
		}

	}

	s.wg.Wait()
}

// NewCPUStitcher constructs a new CPUStitcher.
func NewCPUStitcher(cores int) *CPUStitcher {
	if cores < 2 {
		cores = 2
	}

	s := &CPUStitcher{
		cores: cores,
		m:     sync.Mutex{},
		wg:    &sync.WaitGroup{},
	}

	for i := 0; i < cores; i++ {
		ch := make(chan cpuStitch, 1)
		s.vc = append(s.vc, ch)
		func(ch chan cpuStitch) {
			go func() {
				for {
					s := <-ch
					s.full.Stitch(s.small, s.alpha, s.bottom, s.left)
					s.wg.Done()
				}
			}()
		}(ch)
	}

	return s
}

func drawOver[T uint8 | uint16](
	dstWidth int, dstHeight int, dstData []T,
	srcWidth int, srcHeight int, srcData []T,
	maxT T, sourceAlpha float64, bottom, left int,
	gammafied bool,
) {
	const (
		c          = 4 // RGBA has 4 channels.
		r, g, b, a = 0, 1, 2, 3
	)

	max := float64(maxT)

	for j := 0; j < srcHeight; j++ {
		for i := 0; i < srcWidth; i++ {
			var (
				si = (j*srcWidth + i) * c
				di = ((j+bottom)*dstWidth + (i + left)) * c

				sR = float64(srcData[si+r]) / max
				sG = float64(srcData[si+g]) / max
				sB = float64(srcData[si+b]) / max
				sA = (float64(srcData[si+a]) / max) * sourceAlpha

				dR = float64(dstData[di+r]) / max
				dG = float64(dstData[di+g]) / max
				dB = float64(dstData[di+b]) / max
				dA = float64(dstData[di+a]) / max
			)

			// Blending must be done in linear space.
			if gammafied {
				sR = colour.GammaToLin(sR)
				sG = colour.GammaToLin(sG)
				sB = colour.GammaToLin(sB)
				dR = colour.GammaToLin(dR)
				dG = colour.GammaToLin(dG)
				dB = colour.GammaToLin(dB)
			}

			var (
				// https://apoorvaj.io/alpha-compositing-opengl-blending-and-premultiplied-alpha/
				finalAlpha = sA + dA*(1-sA)
				col        = [c]float64{
					(sR*sA + dR*dA*(1-sA)) / finalAlpha,
					(sG*sA + dG*dA*(1-sA)) / finalAlpha,
					(sB*sA + dB*dA*(1-sA)) / finalAlpha,
					finalAlpha,
				}
			)

			if gammafied {
				col[r] = colour.LinToGamma(col[r])
				col[g] = colour.LinToGamma(col[g])
				col[b] = colour.LinToGamma(col[b])
			}

			dstData[di+r] = T(col[r] * max)
			dstData[di+g] = T(col[g] * max)
			dstData[di+b] = T(col[b] * max)
			dstData[di+a] = T(col[a] * max)
		}
	}
}
