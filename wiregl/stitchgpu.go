package wiregl

import (
	"unsafe"

	"github.com/go-gl/gl/v4.5-core/gl"
)

// GPUStitch the small image into the full size image using the GPU.
type GPUStitch struct {
	// Bottom and left are used to position the bottom left corner of the source
	// image. They are axis aligned distances in units of pixels relative to the
	// bottom left corner of the full size destination image.
	Bottom, Left int
	// Full size destination image onto which the source image is blended.
	Full8  []uint8
	Full16 []uint16
	// Small size source image which is blended into the full size image.
	Small8  []uint8
	Small16 []uint16

	// SmallAlpha is effectively pre-multiplied with the alpha values of the
	// small image's pixels before the blend operation.
	SmallAlpha float64

	stitchStorage
}

func (s GPUStitch) fullData() unsafe.Pointer {
	if s.DeepColour {
		return gl.Ptr(&s.Full16[0])
	} else {
		return gl.Ptr(&s.Full8[0])
	}
}

func (s GPUStitch) smallData() unsafe.Pointer {
	if s.DeepColour {
		return gl.Ptr(&s.Small16[0])
	} else {
		return gl.Ptr(&s.Small8[0])
	}
}

type stitchStorage struct {
	full     imageTexture
	small    imageTexture
	stitched uint32

	ImageSizes
}

func stitchPixels(s GPUStitch, prog compiledProg) PixelData {

	// Load the image data into the image textures and bind them to texture
	// units.

	gl.TextureSubImage2D(
		s.full.tex,
		0, 0, 0, s.w, s.h,
		gl.RGBA, s.dataType(), s.fullData(),
	)
	gl.TextureSubImage2D(
		s.small.tex,
		0, 0, 0, s.sW, s.sH,
		gl.RGBA, s.dataType(), s.smallData(),
	)

	gl.BindTextureUnit(0, s.full.tex)
	gl.BindTextureUnit(1, s.small.tex)

	// Stitch the smaller image into the full size image and draw the result to
	// the framebuffer.

	gl.BindFramebuffer(gl.FRAMEBUFFER, s.stitched)

	prog.setUniforms(map[string]interface{}{
		"full_h":       float32(s.h),
		"full_w":       float32(s.w),
		"small_h":      float32(s.sH),
		"small_w":      float32(s.sW),
		"small_alpha":  float32(s.SmallAlpha),
		"small_bottom": float32(s.Bottom),
		"small_left":   float32(s.Left),
	})
	gl.UseProgram(uint32(prog))
	gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)

	// Read the stitched image's pixels.

	var (
		pix8  []uint8
		pix16 []uint16
	)
	if s.DeepColour {
		pix16 = make([]uint16, 4*s.w*s.h)
		gl.ReadPixels(
			0, 0, s.w, s.h, gl.RGBA, gl.UNSIGNED_SHORT, gl.Ptr(&pix16[0]))
	} else {
		pix8 = make([]uint8, 4*s.w*s.h)
		gl.ReadPixels(
			0, 0, s.w, s.h, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&pix8[0]))
	}

	return PixelData{
		Height: int(s.h),
		Width:  int(s.w),
		Data16: pix16,
		Data8:  pix8,
	}
}

type imageTexture struct {
	tex uint32
}

func newFullImageTexture(ims ImageSizes) (imageTexture, error) {
	return newImageTexture(
		int32(ims.W), int32(ims.H), ims.internalFormat())
}

func newSmallImageTexture(ims ImageSizes) (imageTexture, error) {
	return newImageTexture(
		int32(ims.SmallW), int32(ims.SmallH), ims.internalFormat())
}

func newImageTexture(w, h int32, internalFormat uint32) (imageTexture, error) {
	var (
		transparentBlack = [4]float32{0.0, 0.0, 0.0, 0.0}
		tex              uint32
	)

	gl.CreateTextures(gl.TEXTURE_2D, 1, &tex)

	gl.TextureParameteri(tex, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_BORDER)
	gl.TextureParameteri(tex, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_BORDER)
	gl.TextureParameterfv(tex, gl.TEXTURE_BORDER_COLOR, &transparentBlack[0])

	gl.TextureParameteri(tex, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TextureParameteri(tex, gl.TEXTURE_MAG_FILTER, gl.NEAREST)

	gl.TextureStorage2D(tex, 1, internalFormat, w, h)

	return imageTexture{
		tex: tex,
	}, nil
}

func newStitchStorage(ims ImageSizes) (stitchStorage, error) {
	fullTex, err := newFullImageTexture(ims)
	if err != nil {
		return stitchStorage{}, err
	}

	smallTex, err := newSmallImageTexture(ims)
	if err != nil {
		return stitchStorage{}, err
	}

	stitchedColBuf, err := newFullSizeBuffer(ims)
	if err != nil {
		return stitchStorage{}, err
	}

	return stitchStorage{
		full:     fullTex,
		small:    smallTex,
		stitched: stitchedColBuf.fbo,

		ImageSizes: ims,
	}, err
}
