package wiregl

import (
	"math"

	"gitlab.com/fospathi/maf"
)

const (
	circleStripLen = 14
)

var (
	circStrip circleStrip = newCircleStrip(circleStripLen / 2)
)

// The 2D coordinates of vertices on the perimeter of the right side (x > 0) of
// a unit circle in an anti-clockwise direction from lowest to highest.
//
// Adjacent vertices are equally spaced (by angle spacing).
//
// Convenient for drawing a circle with an OpenGL triangle strip. The missing
// vertices are simply a reflection in the Y-axis of the existing vertices.
type circleStrip []float32

func (c circleStrip) verticesCount() int32 {
	return int32(len(c) / 2) // 2D: two values per point.
}

// Constructs a new circle strip.
//
// The argument is the number of points in the top right quadrant of the circle.
// The returned circle contains twice that many points.
func newCircleStrip(n int) circleStrip {
	if n < 1 {
		n = 1
	}
	cs := func() *circleStrip {
		angle := maf.HalfPi / float64(n)
		startAngle := angle / 2
		var res *circleStrip // Use a pointer so defers can change it.
		{
			cs := circleStrip([]float32{})
			res = &cs
		}
		for i := n - 1; i >= 0; i-- {
			a := startAngle + float64(i)*angle
			x, y := float32(math.Cos(a)), float32(math.Sin(a))
			*res = append(*res, x, -y)
			defer func() {
				*res = append(*res, x, y)
			}()
		}
		return res
	}()
	return *cs
}
