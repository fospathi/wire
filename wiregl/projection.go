package wiregl

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Projection matrices to transform between camera/eye space and clip space.
type Projection struct {
	// Go from camera/eye space to clip space.
	Matrix maf.Mat4
	// Go from clip space to camera/eye space.
	Inverse maf.Mat4
}

// ViewingFrustum specifies a viewing volume for a perspective projection.
//
// Basically this defines a truncated pyramidal shape with the implied camera at
// the implied tip of the pyramid pointing towards the pyramid base.
type ViewingFrustum struct {
	// Aspect is the ratio of the FOV in the x direction to the FOV in the y
	// direction, that is, width divided by height.
	Aspect float64
	// FOV is the field of view in the y direction given in radians.
	FOV float64
	// Magnitude of the distance from the camera to the far clipping plane.
	F float64
	// Magnitude of the distance from the camera to the near clipping plane.
	N float64
}

// NewPerspectiveProjection constructs a new perspective projection matrix that
// projects points in the given pyramidal viewing frustum to 4D clip space
// coordinates.
//
// In terms of eye space coordinates the viewing volume is centered around and
// occupies a portion of the negative Z-axis so that the camera appears to be
// looking down the negative Z-axis from the origin.
//
// Applying a perspective division to the 4D clip space coordinates produces
// normalised device coordinates (NDC). The usual Z mapping is reversed so that
// the near clipping plane is mapped to Z = 1 and the far clipping plane is
// mapped to Z = 0. Note that the camera space is right-handed whereas the
// projected-to NDC space is left-handed.
func NewPerspectiveProjection(vf ViewingFrustum) Projection {
	// A quintessential perspective projection matrix with a symmetric viewing
	// volume and without a reverse-Z mapping is given by
	//
	//   |  C/A     0      0        0       |          where
	//   |                                  |          C = cot(FOV/2)
	//   |   0      C      0        0       |          A = aspect
	//   |                                  |          F = positive far z
	//   |   0      0   F/(N-F)  N*F/(N-F)  |          N = positive near z
	//   |                                  |
	//   |   0      0     -1        0       |
	//
	// For a derivation of this matrix see the explanation for equation (2.23)
	// in the book "3D Graphics for Game Programming":
	//
	//   Han, JungHyun
	//   2011
	//   3D Graphics for Game Programming
	//   Chapman and Hall/CRC
	//
	// An explanation of the reverse-Z concept can be found at:
	//   https://dev.theomader.com/depth-precision/
	// So, apply a reverse-Z mapping to the above matrix giving
	//
	//   |  C/A     0        0             0       |
	//   |                                         |
	//   |   0      C        0             0       |
	//   |                                         |
	//   |   0      0    -1-F/(N-F)    -N*F/(N-F)  |
	//   |                                         |
	//   |   0      0       -1             0       |
	c, a := 1/math.Tan(vf.FOV/2), vf.Aspect
	n, f := vf.N, vf.F
	m := maf.Mat4{
		c / a, 0, 0, 0,
		0, c, 0, 0,
		0, 0, -1 - f/(n-f), -1,
		0, 0, -n * f / (n - f), 0,
	}

	// The inverse of
	//                                             Array indices:
	//  a  0  0  0                                 ( 0  4  8  12 )
	//  0  b  0  0                                 ( 1  5  9  13 )
	//  0  0  c  d                                 ( 2  6  10 14 )
	//  0  0  e  0                                 ( 3  7  11 15 )
	//
	// is
	//
	//  1/a  0    0    0
	//  0    1/b  0    0
	//  0    0    0    1/e
	//  0    0    1/d  -c/(de)
	inv := maf.Mat4{
		1 / m[0], 0, 0, 0,
		0, 1 / m[5], 0, 0,
		0, 0, 0, 1 / m[14],
		0, 0, 1 / m[11], -m[10] / (m[14] * m[11]),
	}

	return Projection{Matrix: m, Inverse: inv}
}

// ViewingBox specifies a viewing volume for an orthographic projection.
//
// Basically this defines a box shape with the implied camera outside the box
// and level with the near face centre.
type ViewingBox struct {
	// Magnitude of the distance from the camera to the far clipping plane.
	F float64
	// Magnitude of the distance from the camera to the near clipping plane.
	N float64
	// Width of the box.
	W float64
	// Height of the box.
	H float64
}

// NewOrthographicProjection constructs a new orthographic projection matrix
// that projects points in the given viewing box to 4D clip space coordinates.
//
// In terms of eye space coordinates the viewing volume is centered around and
// occupies a portion of the negative Z-axis so that the camera appears to be
// looking down the negative Z-axis from the origin.
//
// Assuming W=1 for a given eye space vector then the corresponding 4D clip
// space coordinates are already the normalised device coordinates (NDC) and no
// perspective divide is necessary, this follows since an orthographic
// projection is just a linear scaling from a box to a box. The usual Z mapping
// is reversed so that the near clipping plane is mapped to Z = 1 and the far
// clipping plane is mapped to Z = 0. Note that the camera space is right-handed
// whereas the projected-to NDC space is left-handed.
func NewOrthographicProjection(vb ViewingBox) Projection {
	// A quintessential orthographic projection matrix with a symmetric viewing
	// volume and without a reverse-Z mapping is given by
	//
	//   |  2/W     0      0          0       |      where
	//   |                                    |      W = width
	//   |   0     2/H     0          0       |      H = height
	//   |                                    |      F = positive far z
	//   |   0      0   1/(N-F)    N/(N-F)    |      N = positive near z
	//   |                                    |
	//   |   0      0      0          1       |
	//
	// For an idea of how to derive this matrix see:
	//   https://www.songho.ca/opengl/gl_projectionmatrix.html#ortho
	// Note that since in this case we require a Z in the range [0, 1] as
	// opposed to the usual OpenGL range of [-1, 1] the third row in the above
	// matrix is slightly different to that referenced derivation.
	//
	// An explanation of the reverse-Z concept can be found at:
	//   https://dev.theomader.com/depth-precision/
	// So, apply a reverse-Z mapping to the above matrix giving
	//
	//   |  2/W     0        0             0       |
	//   |                                         |
	//   |   0     2/H       0             0       |
	//   |                                         |
	//   |   0      0      -1/(N-F)     1-N/(N-F)  |
	//   |                                         |
	//   |   0      0        0             1       |
	w, h := vb.W, vb.H
	n, f := vb.N, vb.F
	m := maf.Mat4{
		2 / w, 0, 0, 0,
		0, 2 / h, 0, 0,
		0, 0, -1 / (n - f), 0,
		0, 0, 1 - n/(n-f), 1,
	}

	// The inverse of
	//                                             Array indices:
	//  a  0  0  0                                 ( 0  4  8  12 )
	//  0  b  0  0                                 ( 1  5  9  13 )
	//  0  0  c  d                                 ( 2  6  10 14 )
	//  0  0  0  e                                 ( 3  7  11 15 )
	//
	// is
	//
	//  1/a  0    0     0
	//  0    1/b  0     0
	//  0    0    1/c  -d/(ce)
	//  0    0    0     1/e
	//
	// where e = 1
	inv := maf.Mat4{
		1 / m[0], 0, 0, 0,
		0, 1 / m[5], 0, 0,
		0, 0, 1 / m[10], 0,
		0, 0, -m[14] / m[10], 1,
	}

	return Projection{Matrix: m, Inverse: inv}
}

// IsOrthographic distinguishes between orthographic and perspective projections
// created by the functions in this file.
func IsOrthographic(m maf.OpenGLMat4) bool {
	return m[11] == 0
}
