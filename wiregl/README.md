# Package wiregl

```go
import "gitlab.com/fospathi/wire/wiregl"
```

Go package wiregl rasterises 3D scenes composed of quadratic Bezier curves.

It uses an OpenGL [version 4.5](https://en.wikipedia.org/wiki/OpenGL#OpenGL_4.5) context created with Go's [GLFW](https://github.com/go-gl/glfw) and [OpenGL](https://github.com/go-gl/gl) bindings.

## Dependencies

Package wiregl depends on [GLFW](https://www.glfw.org/). GLFW has its own dependencies; on Ubuntu for example you need the `libgl1-mesa-dev` and `xorg-dev` packages:

```sh
sudo apt install libgl1-mesa-dev xorg-dev
```
