package wiregl

import (
	"fmt"
	"log"
	"strings"
	"unsafe"

	"github.com/go-gl/gl/v4.5-core/gl"
)

// Check whether the OpenGL context has debugging enabled.
func isOpenGLDebugContext() bool {
	var flag int32
	gl.GetIntegerv(gl.CONTEXT_FLAGS, &flag)
	return flag&gl.CONTEXT_FLAG_DEBUG_BIT != 0
}

// The OpenGL callback is called when OpenGl has debugging enabled and an OpenGL
// error occurs.
func openGLDebugCallback(
	glSource uint32,
	glType uint32,
	id uint32,
	glSeverity uint32,
	length int32,
	message string,
	userParam unsafe.Pointer) {
	src, t, svy := "", "", ""

	switch glSource {
	case gl.DEBUG_SOURCE_API:
		src = "API"
	case gl.DEBUG_SOURCE_APPLICATION:
		src = "application"
	case gl.DEBUG_SOURCE_OTHER:
		src = "other"
	case gl.DEBUG_SOURCE_SHADER_COMPILER:
		src = "shader compiler"
	case gl.DEBUG_SOURCE_THIRD_PARTY:
		src = "third party"
	case gl.DEBUG_SOURCE_WINDOW_SYSTEM:
		src = "window system"
	}

	switch glType {
	case gl.DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		t = "deprecated behavior"
	case gl.DEBUG_TYPE_ERROR:
		t = "error"
	case gl.DEBUG_TYPE_MARKER:
		t = "marker"
	case gl.DEBUG_TYPE_OTHER:
		t = "other"
	case gl.DEBUG_TYPE_PERFORMANCE:
		t = "performance"
	case gl.DEBUG_TYPE_POP_GROUP:
		t = "pop group"
	case gl.DEBUG_TYPE_PORTABILITY:
		t = "portability"
	case gl.DEBUG_TYPE_PUSH_GROUP:
		t = "push group"
	case gl.DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		t = "undefined behavior"
	}

	switch glSeverity {
	case gl.DEBUG_SEVERITY_HIGH:
		svy = "high"
	case gl.DEBUG_SEVERITY_LOW:
		svy = "low"
	case gl.DEBUG_SEVERITY_MEDIUM:
		svy = "medium"
	case gl.DEBUG_SEVERITY_NOTIFICATION:
		svy = "notification"
	}

	var b strings.Builder
	b.WriteString("openGL debug\n")
	b.WriteString(fmt.Sprintf(" message: %v\n", message))
	b.WriteString(fmt.Sprintf("  source: %v\n", src))
	b.WriteString(fmt.Sprintf("    type: %v\n", t))
	b.WriteString(fmt.Sprintf("severity: %v\n", svy))
	b.WriteString(fmt.Sprintf("      id: %v\n", id))
	log.Print(b.String())
}

func registerOpenGLDebugCallback() {
	gl.Enable(gl.DEBUG_OUTPUT)
	gl.Enable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
	gl.DebugMessageCallback(openGLDebugCallback, nil)
	gl.DebugMessageControl(gl.DONT_CARE, gl.DONT_CARE, gl.DONT_CARE, 0, nil, true)
}
