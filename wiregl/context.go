package wiregl

import (
	"errors"
	"runtime"
	"sync"
	"time"

	"github.com/go-gl/gl/v4.5-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"gitlab.com/fospathi/universal/mech"
)

// ContextSpec specifies the properties of a new context.
type ContextSpec struct {
	Debug   bool // Set as true to enable OpenGL debugging.
	Samples int  // Enable MSAA by specifying a non-zero number: usually 2, 4, or 8.
	ImageSizes
}

type ImageSizes struct {
	DeepColour     bool // Set as true to enable 16 bit RGB framebuffer/image/screen components as opposed to the usual 8 bits.
	W, H           int  // The dimensions of the framebuffer in pixels.
	SmallW, SmallH int  // The dimensions of the smaller framebuffer in pixels.

	w, h   int32
	sW, sH int32
}

// The sized internal colour format: suitable for glTextureStorage2D() and
// glTextureStorage2DMultisample()...
func (ims ImageSizes) internalFormat() uint32 {
	if ims.DeepColour {
		return gl.RGBA16
	}
	return gl.RGBA8
}

func (ims ImageSizes) dataType() uint32 {
	if ims.DeepColour {
		return gl.UNSIGNED_SHORT
	}
	return gl.UNSIGNED_BYTE
}

// Context is an OpenGL rendering context.
type Context struct {
	pixels    chan Pixels
	rasterise chan Scene
	stitch    chan GPUStitch
	stitched  chan PixelData
	main      chan func()

	aspect        float32
	done          chan struct{}
	deep          bool
	m             sync.RWMutex
	progs         compiledProgs
	sceneBuffers  sceneBuffers
	stitchBuffers stitchStorage
	w, h          int32
	sW, sH        int32
	window        *glfw.Window
}

// InitResult is either a rasterising context or an initialisation error.
type InitResult struct {
	Context *Context
	Err     error
}

// Close terminates GLFW and then closes the main-thread-only channel.
//
// To successfully close, the main-thread-only channel shall be executing
// received functions concurrently.
//
// If the context was previously closed nothing happens.
func (cx *Context) Close() {
	cx.m.Lock()
	defer cx.m.Unlock()
	select {
	case <-cx.done:
		return
	default:
	}

	fin := make(chan struct{})
	cx.main <- func() {
		glfw.Terminate()
		close(fin)
	}
	<-fin
	close(cx.main)
	close(cx.done)
}

// Rasterise the argument scene and return the pixels.
//
// If the given scene has no vertex attribute data a standard empty scene is
// drawn in its place.
//
// If the context was previously closed nothing happens.
func (cx *Context) Rasterise(s Scene) Pixels {
	cx.m.RLock()
	defer cx.m.RUnlock()
	select {
	case <-cx.done:
		return Pixels{}
	default:
	}

	{
		// Prevent a fatal error being encountered for a scene part with no
		// vertex attribute data.
		if len(s.VertexAttributes) == 0 {
			s.VertexAttributes = []float32{0}
		}
		if len(s.Veneer.VertexAttributes) == 0 {
			s.Veneer.VertexAttributes = []float32{0}
		}
	}

	cx.rasterise <- s
	return <-cx.pixels
}

// Stitch the small image into the full image using the GPU.
func (cx *Context) Stitch(s GPUStitch) PixelData {
	cx.m.RLock()
	defer cx.m.RUnlock()
	select {
	case <-cx.done:
		return PixelData{}
	default:
	}

	cx.stitch <- s
	return <-cx.stitched
}

// A MainThread type channel shall execute the functions it receives in the main
// thread only.
type MainThread <-chan func()

// NewContext creates and initialises a new context according to the argument
// spec.
//
// For initialisation to succeed the context's main-thread-only-channel needs to
// execute any received functions in the main thread concurrently.
//
// The returned channel returns an error if initialisation fails in which case
// the context is nil. If initialisation fails glfw has already been terminated.
func NewContext(spec ContextSpec) (MainThread, <-chan InitResult) {
	if spec.H == 0 {
		spec.H = mech.HPixels
	}
	if spec.W == 0 {
		spec.W = mech.WPixels
	}
	if spec.SmallH == 0 {
		spec.SmallH = mech.SmallHPixels
	}
	if spec.SmallW == 0 {
		spec.SmallW = mech.SmallWPixels
	}
	if spec.Samples < 1 {
		spec.Samples = 1
	}
	spec.h = int32(spec.H)
	spec.w = int32(spec.W)
	spec.sH = int32(spec.SmallH)
	spec.sW = int32(spec.SmallW)

	main := make(chan func())
	cx := &Context{
		pixels:    make(chan Pixels),
		rasterise: make(chan Scene),
		stitch:    make(chan GPUStitch),
		stitched:  make(chan PixelData),
		main:      main,

		aspect: float32(spec.W) / float32(spec.H),
		deep:   spec.DeepColour,
		done:   make(chan struct{}),
		m:      sync.RWMutex{},
		w:      int32(spec.W),
		h:      int32(spec.H),
		sW:     int32(spec.SmallW),
		sH:     int32(spec.SmallH),
	}

	ec := make(chan error, 1)

	// initialise shall be executed once in the main thread.
	//
	// If it fails, as indicated by an error being sent on the channel, then
	// glfw.Terminate() has already been called.
	initialise := func() {
		runtime.LockOSThread()

		var err error
		if cx.window, err = initGlfw(spec); err != nil {
			ec <- err
			return
		}

		glOK := make(chan bool)
		go func() { // Process OpenGL commands in a separate goroutine.
			runtime.LockOSThread()
			cx.window.MakeContextCurrent()

			if err = initOpenGL(); err != nil {
				glfw.Terminate()
				ec <- err
				glOK <- false
				return
			}

			if spec.Debug {
				if !isOpenGLDebugContext() {
					glfw.Terminate()
					ec <- errors.New("failed to create an OpenGL debug context")
					glOK <- false
					return
				}
				registerOpenGLDebugCallback()
			}

			if cx.progs, err = compilePrograms(); err != nil {
				glfw.Terminate()
				ec <- err
				glOK <- false
				return
			}

			if cx.sceneBuffers, err = newSceneBuffers(spec); err != nil {
				glfw.Terminate()
				ec <- err
				glOK <- false
				return
			}

			if cx.stitchBuffers, err = newStitchStorage(spec.ImageSizes); err != nil {
				glfw.Terminate()
				ec <- err
				glOK <- false
				return
			}

			go func() { // Still process GLFW events even though they are not used.
				t := time.NewTicker(1 * time.Second)
				defer t.Stop()
				for {
					select {
					case <-t.C:
						cx.main <- glfw.PollEvents
					case <-cx.done:
						return
					}
				}
			}()

			glOK <- true

			for {
				select {
				case s := <-cx.rasterise:
					sc := newScene(s, cx.sceneBuffers, cx.aspect)
					pix := rasteriseScene(sc, cx.progs)
					cx.pixels <- pix
				case s := <-cx.stitch:
					s.stitchStorage = cx.stitchBuffers
					pix := stitchPixels(s, cx.progs.stitch)
					cx.stitched <- pix
				case <-cx.done:
					return
				}
			}
		}()
		if !<-glOK {
			return
		}

		ec <- nil
	}

	rc := make(chan InitResult)
	go func() {
		cx.main <- initialise
		if err := <-ec; err != nil {
			rc <- InitResult{Err: err}
			return
		}
		rc <- InitResult{Context: cx}
	}()
	return main, rc
}
