package wiregl

import (
	"image"
	"math"
	"slices"

	"github.com/bcmills/unsafeslice"
	"gitlab.com/fospathi/wire/colour"
)

// Pixels is the data from the GPU's framebuffer after a scene is rasterised.
//
// The data is in either 16 or 8 bits per component format depending on the bits
// of the framebuffer. The small size data is optional but, if present, has the
// same bits per component as the full size data.
type Pixels struct {
	Full  PixelData
	Small PixelData
}

func (px Pixels) binaryPixelsLen() int {
	l := 8
	if nil != px.Full.Data16 {
		l += len(px.Full.Data16)
	} else {
		l += len(px.Full.Data8)
	}
	if nil != px.Small.Data16 || nil != px.Small.Data8 {
		l += 8
		if nil != px.Small.Data16 {
			l += len(px.Small.Data16)
		} else {
			l += len(px.Small.Data8)
		}
	}
	return l
}

func (px Pixels) ContainsSmall() bool {
	return px.Small.Data16 != nil || px.Small.Data8 != nil
}

func (px Pixels) ToBinaryPixels() BinaryPixels {
	var (
		bin  = make([]byte, px.binaryPixelsLen())
		data []byte
		i    int
	)

	if nil != px.Small.Data16 || nil != px.Small.Data8 {
		if nil != px.Small.Data16 {
			unsafeslice.ConvertAt(&data, px.Small.Data16)
			i += copy(bin[i:], data)
		} else {
			unsafeslice.ConvertAt(&data, px.Small.Data8)
			i += copy(bin[i:], data)
		}
		i += copy(bin[i:], whToBinary(px.Small.Width, px.Small.Height))
	}

	if nil != px.Full.Data16 {
		unsafeslice.ConvertAt(&data, px.Full.Data16)
		i += copy(bin[i:], data)
	} else {
		unsafeslice.ConvertAt(&data, px.Full.Data8)
		i += copy(bin[i:], data)
	}
	i += copy(bin[i:], whToBinary(px.Full.Width, px.Full.Height))

	return bin
}

type PixelData struct {
	// The dimensions of the framebuffer in units of pixels.
	Height, Width int
	// Data16, if not empty, contains the 16 bit RGBA values from each pixel
	// starting at the bottom left of the screen in row order from the lowest to
	// the highest row, left to right in each row.
	Data16 []uint16
	// Data8, if not empty, contains the 8 bit RGBA values from each pixel
	// starting at the bottom left of the screen in row order from the lowest to
	// the highest row, left to right in each row.
	Data8 []uint8
}

// AsBinary returns the data in binary pixels format which will possibly be
// using the same backing slice that the original data uses.
func (px PixelData) AsBinary() BinaryPixelData {
	var bin []byte
	if nil != px.Data16 {
		unsafeslice.ConvertAt(&bin, px.Data16)
	} else {
		unsafeslice.ConvertAt(&bin, px.Data8)
	}
	bin = append(bin, whToBinary(px.Width, px.Height)...)
	return bin
}

// BlankCopy of the receiver with a new backing slice and black transparent
// pixels.
func (px PixelData) BlankCopy() PixelData {
	var (
		data16 []uint16
		data8  []uint8
	)
	if px.Data16 != nil {
		data16 = make([]uint16, len(px.Data16))
	} else {
		data8 = make([]uint8, len(px.Data8))
	}
	return PixelData{
		Height: px.Height,
		Width:  px.Width,
		Data16: data16,
		Data8:  data8,
	}
}

// Copy the receiver using a new backing slice.
func (px PixelData) Copy() PixelData {
	var (
		data16 []uint16
		data8  []uint8
	)
	if px.Data16 != nil {
		data16 = make([]uint16, len(px.Data16))
		copy(data16, px.Data16)
	} else {
		data8 = make([]uint8, len(px.Data8))
		copy(data8, px.Data8)
	}
	return PixelData{
		Height: px.Height,
		Width:  px.Width,
		Data16: data16,
		Data8:  data8,
	}
}

// Stitch the given source image onto the receiver image using the CPU.
//
// Before applying the Porter-Duff compositing algorithm the given alpha (in the
// range 0..1) is multiplied with the alpha of the src image.
// https://apoorvaj.io/alpha-compositing-opengl-blending-and-premultiplied-alpha/
//
// The bottom left corner of the source image is offset from the bottom left
// corner of the receiver image by the given offsets.
func (px PixelData) Stitch(src PixelData, alpha float64, bottom, left int) {
	alpha = math.Min(1, math.Max(0, alpha))
	if src.Data16 != nil {
		drawOver(
			px.Width, px.Height, px.Data16,
			src.Width, src.Height, src.Data16,
			math.MaxUint16, alpha, bottom, left, true)
	} else {
		drawOver(
			px.Width, px.Height, px.Data8,
			src.Width, src.Height, src.Data8,
			math.MaxUint8, alpha, bottom, left, true)
	}
}

func toGam8(i uint8) uint8 {
	f := float64(i) / float64(math.MaxUint8)
	return uint8(math.Round(colour.LinToGamma(f) * math.MaxUint8))
}

func toGam16(i uint16) uint16 {
	f := float64(i) / float64(math.MaxUint16)
	return uint16(math.Round(colour.LinToGamma(f) * math.MaxUint16))
}

// ToGamma encoded form, assuming the receiver pixel data is linear encoded.
func (px PixelData) ToGamma() PixelData {
	gam := px.Copy()
	if px.Data16 != nil {
		for i, l := 0, len(px.Data16); i < l; i++ {
			px.Data16[i] = toGam16(px.Data16[i])
		}
	} else {
		for i, l := 0, len(px.Data8); i < l; i++ {
			px.Data8[i] = toGam8(px.Data8[i])
		}
	}
	return gam
}

// Stride is the number uint8s per row when the data is interpreted as a uint8
// slice.
func (px PixelData) Stride() int {
	if nil != px.Data16 {
		return px.Width * 4 * 2
	}
	return px.Width * 4
}

// ToImage returns a new image from the data.
func (px PixelData) ToImage() image.Image {
	if nil != px.Data16 {
		return &image.NRGBA64{
			Pix:    px.ToUint8WithFlip(),
			Rect:   image.Rect(0, 0, px.Width, px.Height),
			Stride: px.Stride(),
		}
	}
	return &image.NRGBA{
		Pix:    px.ToUint8WithFlip(),
		Rect:   image.Rect(0, 0, px.Width, px.Height),
		Stride: px.Stride(),
	}
}

// ToUint8 returns a new copy of the data but interpreted as a slice of uint8.
func (px PixelData) ToUint8() []uint8 {
	var data []uint8
	if nil != px.Data16 {
		unsafeslice.ConvertAt(&data, slices.Clone(px.Data16))
	} else {
		data = make([]uint8, len(px.Data8))
		copy(data, px.Data8)
	}
	return data
}

// ToUint8WithFlip returns a new copy of the data but interpreted as a slice of
// uint8 and with the rows reflected from top to bottom around the central row.
func (px PixelData) ToUint8WithFlip() []uint8 {
	data := px.ToUint8()
	stride := px.Stride()
	tmp := make([]uint8, stride)
	for r := 0; r < px.Height/2; r++ {
		i1, i2 := r*stride, (px.Height-1-r)*stride
		copy(tmp, data[i1:])
		copy(data[i1:], data[i2:i2+stride])
		copy(data[i2:], tmp)
	}
	return data
}

// BinaryPixels is one or two pixel data binary encodings. When two encodings
// are present, the encoding of the smaller size image is first.
//
// Each pixel data binary encoding is made of:
//
//	data-1, data-2, ..., data-(4*w*h), w-1, w-2, w-3, w-4, h-1, h-2, h-3, h-4
//	|--------------------------------|<-------------appendage-------------->|
//
// * Raw RGBA data, either one or two bytes per channel: data-n are one or two
// byte data elements for 8 or 16 bit colour channels respectively.
//
// * Followed by two four-byte-numbers which are the width and height of the
// image in pixels: w-n and h-n are the bytes, msb first, of four-byte integers
// giving the image width and height.
type BinaryPixels []byte

// ToPixels with new backing slice(s).
func (pix BinaryPixels) ToPixels() Pixels {
	var (
		pixLen = len(pix)
		apd    = 8 // Appendage length in bytes.

		// Full size image dimensions.
		w, h = whFromBinary(pix)
		l8   = int(w) * int(h) * 4 // 4 for the number of channels in RGBA.
		l16  = l8 * 2

		full, small PixelData
	)

	if pixLen-apd == l16 || pixLen-apd == l8 {
		full = BinaryPixelData(slices.Clone(pix)).ToPixelData()
	} else {
		var i int
		if pixLen-apd == l16 {
			i = pixLen - apd - l16
		} else {
			i = pixLen - apd - l8
		}
		full = BinaryPixelData(slices.Clone(pix[i:])).ToPixelData()
		small = BinaryPixelData(slices.Clone(pix[:i])).ToPixelData()
	}

	return Pixels{Full: full, Small: small}
}

// BinaryPixelData is RGBA data and a width/height suffix.
type BinaryPixelData []byte

// ToPixelData with a newly allocated backing slice.
func (pix BinaryPixelData) ToPixelData() PixelData {
	var (
		w, h = pix.WidthHeight()

		apd = 8              // Appendage length in bytes.
		l   = len(pix) - apd // Actual pixel data length in bytes.

		n = int(w) * int(h) * 4 // 4 for the number of channels in RGBA.
	)

	data := make([]byte, l)
	copy(data, pix)

	if l == 2*n {
		var data16 []uint16
		unsafeslice.ConvertAt(&data16, data)
		return PixelData{
			Height: int(h),
			Width:  int(w),
			Data16: data16,
		}
	}

	var data8 []uint8
	unsafeslice.ConvertAt(&data8, data)
	return PixelData{
		Height: int(h),
		Width:  int(w),
		Data8:  data8,
	}
}

// WidthHeight of the image in pixels as given by the binary pixels' appendage.
func (pix BinaryPixelData) WidthHeight() (w int, h int) {
	return whFromBinary(pix)
}

func whFromBinary(pix []byte) (w int, h int) {
	l := len(pix)
	var (
		w1 = uint32(pix[l-8]) * (1 << 24)
		w2 = uint32(pix[l-7]) * (1 << 16)
		w3 = uint32(pix[l-6]) * (1 << 8)
		w4 = uint32(pix[l-5])
		h1 = uint32(pix[l-4]) * (1 << 24)
		h2 = uint32(pix[l-3]) * (1 << 16)
		h3 = uint32(pix[l-2]) * (1 << 8)
		h4 = uint32(pix[l-1])
	)
	w = int(w1 + w2 + w3 + w4)
	h = int(h1 + h2 + h3 + h4)
	return
}

func whToBinary(width, height int) (wh []byte) {
	const mask8 = ^uint32(0) >> (32 - 8)
	wu32, hu32 := uint32(width), uint32(height)
	wh = make([]byte, 8)
	wh[0] = byte((wu32 >> (32 - 8)) & mask8)
	wh[1] = byte((wu32 >> (32 - 16)) & mask8)
	wh[2] = byte((wu32 >> (32 - 24)) & mask8)
	wh[3] = byte((wu32 >> (32 - (32))) & mask8)
	wh[4] = byte((hu32 >> (32 - 8)) & mask8)
	wh[5] = byte((hu32 >> (32 - 16)) & mask8)
	wh[6] = byte((hu32 >> (32 - 24)) & mask8)
	wh[7] = byte((hu32 >> (32 - (32))) & mask8)
	return wh
}
