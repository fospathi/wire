package wiregl

import (
	"github.com/go-gl/gl/v4.5-core/gl"
)

const (
	// float32sPerVertex is how many float32s each Bezier curve vertex uses in
	// the vertex attribute array of a Scene.
	float32sPerVertex = 19

	bytesPerFloat32 = 4
	bytesPerUInt32  = 4

	stride = int32(bytesPerFloat32 * float32sPerVertex)
)

var attributes = []vertexAttribute{
	positionAttrib,
	colourAttrib,
	thicknessAttrib,
	toEyeAttrib,
}

var positionAttrib = vertexAttribute{
	Index:          0,
	Indices:        1,
	RelativeOffset: 0,
	Elements:       3,
}

var colourAttrib = vertexAttribute{
	Index:          positionAttrib.nextAttributeIndex(),
	Indices:        1,
	RelativeOffset: positionAttrib.nextRelativeOffset(),
	Elements:       3,
}

var thicknessAttrib = vertexAttribute{
	Index:          colourAttrib.nextAttributeIndex(),
	Indices:        1,
	RelativeOffset: colourAttrib.nextRelativeOffset(),
	Elements:       1,
}

var toEyeAttrib = vertexAttribute{
	Index:          thicknessAttrib.nextAttributeIndex(),
	Indices:        4,
	RelativeOffset: thicknessAttrib.nextRelativeOffset(),
	Elements:       12,
}

// Specifies the properties of a vertex attribute in a vertex attribute array.
//
// A vertex can have multiple vertex attributes.
type vertexAttribute struct {
	// The number of elements needed to store an instance of this vertex
	// attribute in the vertex attribute array.
	//
	// So for for a vertex attribute array of type []float32 it is the number of
	// float32s it uses.
	Elements int32
	// The order/index of this vertex attribute in the attributes of the vertex.
	Index uint32
	// The number of attribute indices taken by this vertex attribute.
	Indices uint32
	// The start byte index of this vertex attribute relative to the first byte
	// of the vertex. Measured in bytes.
	RelativeOffset int
}

func (va vertexAttribute) glEnableVertexArrayAttrib(vao uint32) {
	for i := uint32(0); i < va.Indices; i++ {
		gl.EnableVertexArrayAttrib(vao, va.Index+i)
	}
}

func (va vertexAttribute) glVertexArrayAttribFormat(vao uint32) {
	size := va.size()
	for i := uint32(0); i < va.Indices; i++ {
		gl.VertexArrayAttribFormat(
			vao, va.Index+i, size, gl.FLOAT, false, va.relativeOffset(i))
	}
}

func (va vertexAttribute) glVertexArrayAttribBinding(
	vao uint32,
	bindingIndex uint32,
) {

	for i := uint32(0); i < va.Indices; i++ {
		gl.VertexArrayAttribBinding(vao, va.Index+i, bindingIndex)
	}
}

func (va vertexAttribute) nextAttributeIndex() uint32 {
	return va.Index + va.Indices
}

func (va vertexAttribute) nextRelativeOffset() int {
	return va.RelativeOffset + int(va.Elements)*bytesPerFloat32
}

// The relative offset for the relative argument attribute index, both of which
// shall be relative to the start of this attribute.
func (va vertexAttribute) relativeOffset(i uint32) uint32 {
	return uint32(va.RelativeOffset) + i*uint32(va.size())*bytesPerFloat32
}

// The size​ parameter of the gl.VertexArrayAttribFormat function.
//
// Gives the number of elements of the vertex attributes array used per
// attribute index for this attribute.
//
// It is a number in the range 1-4.
func (va vertexAttribute) size() int32 {
	return va.Elements / int32(va.Indices)
}

func enableAttributes(vao uint32) {
	for _, a := range attributes {
		a.glEnableVertexArrayAttrib(vao)
	}
	for _, a := range attributes {
		a.glVertexArrayAttribFormat(vao)
	}
	for _, a := range attributes {
		a.glVertexArrayAttribBinding(vao, 0)
	}
}
