package wire

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/universal/mech"
	"gitlab.com/fospathi/wire/depictioner"
)

// NewSin constructs a new Sin which is initially oriented with a standard
// frame.
func NewSin() *Sin {
	return &Sin{depictioner.Figure{Orientation: maf.StdFrame}}
}

// Sin is a wireframe sin curve figure.
//
// Relative to its orientation frame the curve is in the Z=0 plane.
type Sin struct {
	depictioner.Figure
}

// Depict the given portions/(X-axis intervals) on the sin curve, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians" and
// when left unset a sensible default is used.
func (fig *Sin) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictSin(fig.Orientation, vp)
}

// DepictSin depicts the given portions/(parameter intervals) on a sin curve.
func DepictSin(
	fr maf.Frame,
	vp []mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	// Concavity of sin
	//
	// 2nd derivative of sin(θ) == -sin(θ) == concavity at θ. Compare the
	// concavity sign at two angles:
	//   different sign => change of concavity direction => inflection
	f := func(x float64) (float64, float64, float64) {
		s, c := math.Sincos(x)
		return s, c, -s
	}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYCartesian(p, fr, f))
	}
	return pic
}

// NewCos constructs a new Cos which is initially oriented with a standard
// frame.
func NewCos() *Cos {
	return &Cos{depictioner.Figure{Orientation: maf.StdFrame}}
}

// Cos is a wireframe cos curve figure.
//
// Relative to its orientation frame the curve is in the Z=0 plane.
type Cos struct {
	depictioner.Figure
}

// Depict the given portions/(X-axis intervals) on the cos curve, overwriting
// the previous depiction(s) if any.
//
// The given portions' intervals shall not overlap with each other and their
// units are radians.
//
// The portions' quality has units of "quadratic Beziers per 2π radians" and
// when left unset a sensible default is used.
func (fig *Cos) Depict(vp ...mech.Portion) {
	fig.Lock()
	defer fig.Unlock()

	fig.Fig = DepictCos(fig.Orientation, vp...)
}

// DepictCos depicts the given portions/(parameter intervals) on a cos curve.
func DepictCos(
	fr maf.Frame,
	vp ...mech.Portion,
) mech.Depiction {

	pic := mech.Depiction{}
	// Concavity of cos
	//
	// 2nd derivative of cos(θ) == -cos(θ) == concavity at θ. Compare the
	// concavity sign at two angles:
	//   different sign => change of concavity direction => inflection
	f := func(x float64) (float64, float64, float64) {
		s, c := math.Sincos(x)
		return c, -s, -c
	}
	for _, p := range vp {
		if p.Qual < 1 {
			p.Qual = TrigQual
		}
		p.Qual = int(math.Ceil(float64(p.Qual) * p.AbsLen() / maf.TwoPi))
		pic = pic.Append(depictioner.DepictXYCartesian(p, fr, f))
	}
	return pic
}
