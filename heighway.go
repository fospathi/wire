package wire

import (
	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/wire/compgeom/heighway"
)

// NewHeighwayDragon constructs a new HeighwayDragon curve which is initially
// oriented with a standard frame.
func NewHeighwayDragon(spec heighway.PlotSpec) HeighwayDragon {
	return HeighwayDragon{NewPolyLine(heighway.Vertices(spec))}
}

// A HeighwayDragon is a wireframe figure that depicts a Heighway dragon curve.
type HeighwayDragon struct {
	*PolyLine
}

// NewHeighwayTwinDragon constructs a new HeighwayTwinDragon curve which is
// initially oriented with a standard frame.
func NewHeighwayTwinDragon(spec heighway.PlotSpec) *HeighwayTwinDragon {
	d1 := heighway.Vertices(spec)
	spec.ReflectX = !spec.ReflectX
	spec.ReflectY = !spec.ReflectY
	d2 := heighway.Vertices(spec)
	return &HeighwayTwinDragon{NewMultiPartPolyLine([][]maf.Vec{d1, d2})}
}

// HeighwayTwinDragon is a wireframe figure that depicts a Heighway twin dragon
// curve.
type HeighwayTwinDragon struct {
	*MultipartPolyLine
}

// NewTerdragon constructs a new Terdragon curve which is initially oriented
// with a standard frame.
func NewTerdragon(spec heighway.TerdragonPlotSpec) Terdragon {
	return Terdragon{NewPolyLine(heighway.TerdragonVertices(spec))}
}

// A Terdragon is a wireframe figure that depicts a Heighway terdragon curve.
type Terdragon struct {
	*PolyLine
}

// NewHexTerdragon constructs a new HexTerdragon curve which is initially
// oriented with a standard frame.
func NewHexTerdragon(spec heighway.TerdragonPlotSpec) HexTerdragon {
	const n = 6 // Six copies fit around a point.
	dragons := [6][]maf.Vec{}
	dragons[0] = heighway.TerdragonVertices(spec)
	for i, l := 1, len(dragons[0]); i < n; i++ {
		rot := maf.NewZRot(float64(i) * maf.TwoPi / n)
		vp := make([]maf.Vec, l)
		for j := 0; j < l; j++ {
			vp[j] = rot.Transform(dragons[0][j])
		}
		dragons[i] = vp
	}
	return HexTerdragon{NewMultiPartPolyLine(dragons[:])}
}

// A HexTerdragon is a wireframe figure that depicts six copies of a Heighway
// terdragon curve which surround a point.
type HexTerdragon struct {
	*MultipartPolyLine
}

// NewHeighwayFudgeflake constructs a new HeighwayFudgeflake curve which is
// initially oriented with a standard frame.
func NewHeighwayFudgeflake(
	spec heighway.FudgeflakePlotSpec,
) HeighwayFudgeflake {

	return HeighwayFudgeflake{NewPolyLine(heighway.FudgeflakeVertices(spec))}
}

// A HeighwayFudgeflake is a wireframe figure that depicts a Heighway fudgeflake
// curve.
type HeighwayFudgeflake struct {
	*PolyLine
}

// NewGoldenDragon constructs a new GoldenDragon curve which is initially
// oriented with a standard frame.
func NewGoldenDragon(spec heighway.TerdragonPlotSpec) GoldenDragon {
	return GoldenDragon{NewPolyLine(heighway.GoldenVertices(spec))}
}

// A GoldenDragon is a wireframe figure that depicts a Heighway golden dragon
// curve.
type GoldenDragon struct {
	*PolyLine
}
